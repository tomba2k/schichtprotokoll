<?php

	require 'vendor/autoload.php';
	require 'core/bootstrap.php';
	use App\Core\Router;
	use App\Core\Request;

	$action = 'schichtprotokoll/pdf';
	$method = "GET";

	Router::load('app/routes.php')
		->direct($action, $method);
	//Router::load('app/routes.php')->direct(Request::uri(), Request::method());
	