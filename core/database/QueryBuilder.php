<?php

namespace App\Core\Database;

use PDO;
use Exception;

class QueryBuilder {

    protected $pdo;

    public function __construct(PDO $pdo) {
        $this->pdo = $pdo;
    }

    public function SelectAll($table, $intoClass, $orderBy = '') {
        if ($orderBy <> '') {
            $orderBy = " ORDER BY $orderBy";
        }
        $stmt = $this->pdo->prepare("SELECT * FROM {$table} {$orderBy}");
        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_CLASS, $intoClass);
        $stmt->closeCursor();
        return $result;
    }

    public function SelectWhere($table, $columns = '', $parameters = '', $orderBy = '', $intoClass) {
        $where = $parameters;
        $what = $columns;

        if ($columns != '') {
            $what = $columns['COLUMNS'] . " ";
        }
        //die(var_dump($what));

        if ($where != '') {
            $where = '';
            foreach ($parameters as $parameter => $value) {
                $where .= $parameter . "=" . $value . " AND ";
            };
            $where = 'WHERE ' . preg_replace('/\W\w+\s*(\W*)$/', '$1', $where);
            //die(var_dump($where));
        } else {
            $where = '';
        }

        if ($orderBy != '') {
            $order = ' ORDER BY ' . $orderBy;
        } else {
            $order = '';
        }

        $sql = sprintf(
                'SELECT %s FROM %s %s %s', $what, $table, $where, $order
        );

        try {
            $stmt = $this->pdo->prepare($sql);
            //die(var_dump($sql));
            $stmt->execute();

            $result = $stmt->fetchAll(PDO::FETCH_CLASS, $intoClass);
            $stmt->closeCursor();

            return $result;
        } catch (Exception $e) {
            die(var_dump($stmt) . '<BR><BR>' . $e->getMessage());
        }
    }

    // same as SelectWhere but where must be set manually
    public function SelectWhere2($table, $columns = '', $parameters = '', $orderBy = '', $intoClass) {
        $where = $parameters;
        $what = $columns;

        if ($columns != '') {
            $what = $columns['COLUMNS'] . " ";
        }
        //die(var_dump($what));

        if ($where != '') {
            $where = $parameters;
        } else {
            $where = '';
        }

        if ($orderBy != '') {
            $order = ' ORDER BY ' . $orderBy;
        } else {
            $order = '';
        }

        $sql = sprintf(
                'SELECT %s FROM %s %s %s', $what, $table, $where, $order
        );

        try {
            $stmt = $this->pdo->prepare($sql);
            //die(var_dump($sql));
            $stmt->execute();

            $result = $stmt->fetchAll(PDO::FETCH_CLASS, $intoClass);
            $stmt->closeCursor();

            return $result;
        } catch (Exception $e) {
            die(var_dump($stmt) . '<BR><BR>' . $e->getMessage());
        }
    }

    // same as SelectWhere but where must be set manually
    public function SelectWhere3($table, $columns = '', $parameters = '', $orderBy = '', $intoClass) {
        $where = $parameters;
        $what = $columns;

        if ($columns != '') {
            $what = $columns['COLUMNS'] . " ";
        }
        //die(var_dump($what));

        if ($where != '') {
            $where = $parameters;
        } else {
            $where = '';
        }

        if ($orderBy != '') {
            $order = ' ORDER BY ' . $orderBy;
        } else {
            $order = '';
        }

        $sql = sprintf(
                'SELECT %s FROM %s %s %s', $what, $table, $where, $order
        );

        try {
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute();


            $result = $stmt->fetchAll(PDO::FETCH_CLASS);
            $stmt->closeCursor();

            return $result;
        } catch (Exception $e) {
            die(var_dump($stmt) . '<BR><BR>' . $e->getMessage());
        }
    }

    public function SelectGroupBy($whats, $tables, $wheres, $groupBys, $orderBy, $intoClass) {

        $wht = '';
        $tbl = '';
        $whr = '';
        $grpBy = '';

        foreach ($whats as $what) {
            $wht .= $what . ', ';
        }
        $wht = trim($wht, ', ');

        foreach ($tables as $table) {
            $tbl .= $table . ', ';
        }
        $tbl = trim($tbl, ', ');

        foreach ($wheres as $where) {
            $whr .= $where . ' AND ';
        }
        $whr = trim($whr, ' AND ');

        foreach ($groupBys as $groupBy) {
            $grpBy .= $groupBy . ', ';
        }
        $grpBy = trim($grpBy, ', ');




        $sql = sprintf(
                'SELECT %s FROM %s WHERE %s GROUP BY %s ORDER BY %s', $wht, $tbl, $whr, $grpBy, $orderBy
        );

        try {
            $stmt = $this->pdo->prepare($sql);
            $stmt->execute();

            $result = $stmt->fetchAll(PDO::FETCH_CLASS, $intoClass);
            $stmt->closeCursor();

            return $result;
        } catch (Exception $e) {
            die(var_dump($stmt) . '<BR><BR>' . $e->getMessage());
        }
    }

    public function insert($table, $parameters) {
        $sql = sprintf(
                'INSERT INTO %s (%s) VALUES (%s)', $table, implode(', ', array_keys($parameters)), implode(', ', array_values($parameters))
        );

        try {
            $stmt = $this->pdo->prepare($sql);
            //die(var_dump($stmt));
            $stmt->execute();
            $stmt->closeCursor();
        } catch (Exception $e) {
            die(var_dump($stmt) . '<BR><BR>' . $e->getMessage());
        }
    }

    public function update($table, $setValues, $whereValues) {
        $where = '';
        $set = '';

        foreach ($setValues as $setKey => $setValue) {
            $set .= $setKey . '=' . $setValue . ', ';
        }
        $set = trim($set, ', ');

        foreach ($whereValues as $whereKey => $whereValue) {
            $where .= $whereKey . '=' . $whereValue . ' AND ';
        }
        $where = trim($where, ' AND ');

        $sql = sprintf(
                'UPDATE %s SET %s WHERE %s', $table, $set, $where
        );

        try {
            $stmt = $this->pdo->prepare($sql);
            //die(var_dump($stmt));
            $stmt->execute();
            $stmt->closeCursor();
        } catch (Exception $e) {
            die(var_dump($stmt) . '<BR><BR>' . $e->getMessage());
        }
    }

    public function delete($table, $whereValues) {
        $where = '';
        foreach ($whereValues as $whereKey => $whereValue) {
            $where .= $whereKey . "=" . $whereValue . " AND ";
        };
        $where = preg_replace('/\W\w+\s*(\W*)$/', '$1', $where);

        $sql = sprintf(
                'DELETE FROM %s WHERE %s', $table, $where
        );
        //die(var_dump($sql));

        try {
            $stmt = $this->pdo->prepare($sql);
            //die(var_dump($stmt));
            $stmt->execute();
            $stmt->closeCursor();
        } catch (Exception $e) {
            die(var_dump($stmt) . '<BR><BR>' . $e->getMessage());
        }
    }

}
