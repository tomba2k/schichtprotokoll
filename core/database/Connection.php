<?php

namespace App\Core\Database;

use PDO;

class Connection
{
	public static function make($config)
	{
		try{
	        return new PDO(
	        	'oci:dbname='.
	        	$config['tns'].
	        	';'.$config['charset'],
	        	$config['username'],
	        	$config['password'],
	        	$config['options']
	        	);
	    }catch(PDOException $e){
	        die('ERROR: ' . $e->getMessage());
	    }
	}
}