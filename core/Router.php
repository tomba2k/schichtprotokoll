<?php

namespace App\Core;

use Exception;

class Router {

    protected $routes = [
        'GET' => [],
        'POST' => [],
    ];

    public static function load($file) {
        $router = new static;

        require $file;
        return $router;
    }

    public function get($uri, $controller) {
        $this->routes['GET'][$uri] = $controller;
    }

    public function post($uri, $controller) {
        $this->routes['POST'][$uri] = $controller;
    }

    public function direct($uri, $requestType) {
        if (array_key_exists($uri, $this->routes[$requestType])) {
            $arguments = explode('@', $this->routes[$requestType][$uri]);
            return $this->callAction($arguments[0], $arguments[1]);
        }

        return $this->callAction('PagesController', 'home');
        //throw new Exception('No routes defined for this URI'.$this->routes[$requestType][$uri]);
    }

    protected function callAction($controller, $action) {
        $controller = "App\\Controllers\\{$controller}";
        $controller = new $controller;

        if (!method_exists($controller, $action)) {
            throw new Exception(var_dump($controller) . "does not respond to the {$action} action");
        }

        return $controller->$action();
    }

}
