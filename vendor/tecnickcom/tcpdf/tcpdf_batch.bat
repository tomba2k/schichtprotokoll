@echo off
Title Start and Kill Internet Explorer
Mode con cols=75 lines=5 & color 0B
set url="http://localhost/schichtprotokoll/pdf"

echo(
echo                     Launching Internet Explorer ...

Start "" "%ProgramFiles%\Internet Explorer\iexplore.exe" %url%

:: Sleep for 10 seconds, you can change the SleepTime variable
set SleepTime=10
Timeout /T %SleepTime% /NoBreak>NUL
Cls & Color 0C
echo(
echo              Killing Internet Explorer Please wait for a while ...
Taskkill /IM "iexplore.exe" /FI "WINDOWTITLE eq TCPDF*"
