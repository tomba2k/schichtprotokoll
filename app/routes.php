<?php

$router->get('schichtprotokoll', 'PagesController@home');
$router->get('schichtprotokoll/admin', 'PagesController@admin');
$router->get('schichtprotokoll/pdf', 'PagesController@pdf');

// GET Schichtprotokoll
$router->get('schichtprotokoll/schichtprotokoll', 'SchichtProtokollController@schichtprotokoll');
$router->get('schichtprotokoll/ruckweisungrohbau' , 'RuckweisungRohbauController@ruckweisungrohbau');
$router->get('schichtprotokoll/qualitatsschwerpunkte' , 'QualitatsschwerpunkteController@qualitatsschwerpunkte');

// GET Admin area
$router->get('schichtprotokoll/schichten', 'SchichtenController@schichten');
$router->get('schichtprotokoll/storungen', 'StorungenController@storungen');
$router->get('schichtprotokoll/users', 'UsersController@users');
$router->get('schichtprotokoll/art', 'ArtController@art');
$router->get('schichtprotokoll/typ', 'TypController@typ');

// GET Reports
$router->get('schichtprotokoll/reports.alle_storungen', 'ReportsController@alle_storungen');
$router->get('schichtprotokoll/reports.schichtprotokoll', 'ReportsController@schichtprotokoll');
$router->get('schichtprotokoll/reports.advancedreport', 'ReportsController@advancedreport');
$router->get('schichtprotokoll/reports.listreport', 'ReportsController@listreport');
$router->get('schichtprotokoll/reports.URNreport', 'ReportsController@Urnreport');
$router->get('schichtprotokoll/reports.ALDreport', 'ReportsController@Aldreport');
// POST Schichtprotokoll
$router->post('schichtprotokoll/schichtprotokoll', 'SchichtProtokollController@schichtprotokoll');
$router->post('schichtprotokoll/schichtprotokoll.store', 'SchichtProtokollController@store');
$router->post('schichtprotokoll/schichtprotokoll.edit', 'SchichtProtokollController@edit');
$router->post('schichtprotokoll/schichtprotokoll.delete', 'SchichtProtokollController@delete');

// POST RuckweisungRohbau
$router->post('schichtprotokoll/ruckweisungrohbau', 'RuckweisungRohbauController@ruckweisungrohbau');
$router->post('schichtprotokoll/ruckweisungrohbau.store', 'RuckweisungRohbauController@store');
$router->post('schichtprotokoll/ruckweisungrohbau.edit', 'RuckweisungRohbauController@edit');
$router->post('schichtprotokoll/ruckweisungrohbau.delete', 'RuckweisungRohbauController@delete');

// POST Qualitatsschwerpunkte
$router->post('schichtprotokoll/qualitatsschwerpunkte', 'QualitatsschwerpunkteController@qualitatsschwerpunkte');
$router->post('schichtprotokoll/qualitatsschwerpunktetest', 'QualitatsschwerpunkteTestController@qualitatsschwerpunkte');
$router->post('schichtprotokoll/qualitatsschwerpunkte.store', 'QualitatsschwerpunkteController@store');
$router->post('schichtprotokoll/qualitatsschwerpunkte.edit', 'QualitatsschwerpunkteController@edit');
$router->post('schichtprotokoll/qualitatsschwerpunkte.delete', 'QualitatsschwerpunkteController@delete');

// POST Reports
$router->post('schichtprotokoll/reports.schichtprotokoll', 'ReportsController@schichtprotokoll');
$router->post('schichtprotokoll/reports.advancedreport', 'ReportsController@advancedreport');
$router->post('schichtprotokoll/reports.listreport', 'ReportsController@listreport');
$router->post('schichtprotokoll/reports.ALDreport', 'ReportsController@ALDreport');
$router->post('schichtprotokoll/reports.schichtprotokoll.exportCSV', 'ReportsController@exportCSV');
$router->post('schichtprotokoll/reports.URNreport', 'ReportsController@Urnreport');

// POST Admin area
$router->post('schichtprotokoll/schichten', 'SchichtenController@store');
$router->post('schichtprotokoll/schichten.delete', 'SchichtenController@delete');

$router->post('schichtprotokoll/storungen', 'StorungenController@storungen');
$router->post('schichtprotokoll/storungen.active', 'StorungenController@active');
$router->post('schichtprotokoll/storungen.new_storung', 'StorungenController@new_storung');
$router->post('schichtprotokoll/storungen.new_unterkategorie', 'StorungenController@new_unterkategorie');

$router->post('schichtprotokoll/users', 'UsersController@store');
$router->post('schichtprotokoll/users.edit', 'UsersController@edit');

$router->post('schichtprotokoll/art', 'ArtController@store');
$router->post('schichtprotokoll/art.edit', 'ArtController@edit');

$router->post('schichtprotokoll/typ', 'TypController@store');
$router->post('schichtprotokoll/typ.edit', 'TypController@edit');