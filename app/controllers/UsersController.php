<?php

namespace App\Controllers;

use App\Core\App;

class UsersController{

	public function __construct(){
	}

	public function users(){
		$users = App::get('database')->SelectWhere('STR_USERS',['COLUMNS' => 'ID, NAME, SURNAME, USERNAME, TEAM, USERRIGHTS, ACTIVE, PASSWORD'],'','','Users');

		return view ('users',['users' => $users]);
	}

	public function store(){

		if(isset($_POST['name']) && isset($_POST['surname']) && isset($_POST['username']) && isset($_POST['password'])){
			$name = $_POST['name'];
			$surname = $_POST['surname'];
			$username = $_POST['username'];
			$password = $_POST['password'];
			$userrights = $_POST['userrights'];
			$team = $_POST['team'];
			$active = $_POST['active'];

			if ($name != '' && $surname != '' && $username != '' && $password != '') {
				App::get('database') -> insert('STR_USERS', [
					'USERNAME'	=> "'".$username."'",
					'PASSWORD'	=> "'".$password."'",
					'NAME'		=> "'".$name."'",
					'SURNAME'	=> "'".$surname."'",
					'TEAM'		=> "'".$team."'",
					'USERRIGHTS'=> "'".$userrights."'",
					'ACTIVE'	=> "'".$active."'"
				]);
			}
		}

		return redirect ('users');
	}

	public function edit(){

		$id = $_POST['modalid'];
		$password = $_POST['modalpassword'];
		$userrights = $_POST['modaluserrights'];
		$team = $_POST['modalteam'];
		$active = $_POST['modalactive'];

		App::get('database') -> update('STR_USERS', [
			'PASSWORD'	=> "'".$password."'",
			'TEAM'		=> "'".$team."'",
			'USERRIGHTS'=> "'".$userrights."'",
			'ACTIVE'	=> "'".$active."'" ],[
			'ID'		=> $id
			]);
		
		return redirect ('users');	
	}
}

