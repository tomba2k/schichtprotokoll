<?php

namespace App\Controllers;

use App\Core\App;

class RuckweisungRohbauController {
    public $time;
    public $hours;
    public $schicht;
    public function __construct() {
        
        $hour = new \Time();
        $hour->SCHICHT = 1;
        $hour->STR_TIME = "06:00";
        $hour->STR_TIME_END = "13:59";
        
        $TMP[] = $hour;
        $hour = new \Time();
        $hour->SCHICHT = 2;
        $hour->STR_TIME = "14:00";
        $hour->STR_TIME_END = "21:59";
        
        $TMP[] = $hour;
        $hour = new \Time();
        $hour->SCHICHT = 3;
        $hour->STR_TIME = "22:00";
        $hour->STR_TIME_END = "05:59";
        
        $TMP[] = $hour;
        $this->hours = $TMP;
        $this->autorefresh = 'on';
        $this->autorefreshtime = '10';
        if (isset($_POST['autorefresh_checked'])) {
            $this->autorefresh = $_POST['autorefresh_checked'];
        }

        if (isset($_POST['autorefreshtime'])) {
            $this->autorefreshtime = $_POST['autorefreshtime'];
        }
		
        $this->datum_tage = date('d.m.Y H:i');
        $this->phpdate = new \DateTime($this->datum_tage);  
        $this->yesterday = new \DateTime($this->datum_tage);
        $this->yesterday = $this->yesterday->sub(new \DateInterval('P1D'))->format('d.m.Y');

        $this->time = $this->phpdate->format('H');
        $this->week = $this->phpdate->format('W');
        $this->year = $this->phpdate->format('Y');
        
        $this->halle_id = 3;
        $this->schicht = '';
        $this->ds = '';
        $this->phpdate = new \DateTime($this->datum_tage);

        $this->yesterday = new \DateTime($this->datum_tage);
        $this->yesterday = $this->yesterday->sub(new \DateInterval('P1D'))->format('d.m.Y');

        $this->time = $this->phpdate->format('H');
        $this->week = $this->phpdate->format('W');
        $this->year = $this->phpdate->format('Y');
        $this->datum_tage_sql = "TO_DATE('" . date('d.m.Y') . "','DD.MM.YYYY')";
        $this->datum_tage_sql_yesterday = "TO_DATE('" . date('d.m.Y', strtotime($this->yesterday)) . "','DD.MM.YYYY')";

        // get Schicht and Team data filtered by selected date for that week
        $this->schichten = App::get('database')->SelectWhere('STR_CALENDAR', ['COLUMNS' => 'TEAM, SCHICHT'], ['ACTIVE' => "'1'",
            'WEEK' => $this->week,
            "TO_CHAR(STR_DATE,'YYYY')" => "'" . $this->year . "'"
                ], '', 'Schichten');
        
        $this->setSchicht($this->hours, $this->schichten);

        if (isset($_POST['halle_id']) && $_POST['halle_id'] != '') {

            if ($this->autorefresh == 'off') {
                $this->datum_tage = $_POST['datum_tage'];
            }

            $this->halle_id = $_POST['halle_id'];
            $this->schicht = $_POST['schicht'];
            $this->phpdate = new DateTime($this->datum_tage);
            $this->week = $this->phpdate->format('W');
            $this->year = $this->phpdate->format('Y');
            $this->datum_tage_sql = strtotime($this->datum_tage);
            $this->datum_tage_sql = "TO_DATE('" . date('d.m.Y', $this->datum_tage_sql) . "','DD.MM.YYYY')";
        }


        if (isset($_POST['datum_tage_modal']) && $_POST['datum_tage_modal'] != '') {

            $this->autorefresh = $_POST['autorefresh_checked_modal'];
            $this->autorefreshtime = $_POST['autorefreshtime_modal'];

            $this->halle_id = $_POST['halle_id_modal'];
            $this->schicht = $_POST['schicht_modal'];
            $this->datum_tage = $_POST['datum_tage_modal'];
            $this->phpdate = new \DateTime($this->datum_tage);
            $this->week = $this->phpdate->format('W');
            $this->year = $this->phpdate->format('Y');
            $this->datum_tage_sql = strtotime($this->datum_tage);
            $this->datum_tage_sql = "TO_DATE('" . date('d.m.Y', $this->datum_tage_sql) . "','DD.MM.YYYY')";

        }
    }

    public function ruckweisungrohbau() {
        $ruckweisungrohbau = App::get('database')->SelectWhere('STR_RUECKWEISUNG', ['COLUMNS' => 'ID, to_char(DATUM, \'DD.MM.YYYY\') as DATUM, TYPE, GRUND, SCHICHT, BEMERKUNG'], '', '', 'RuckweisungRohbau');
        $ruckweisungrohbau = $this->utf8_encode_deep($ruckweisungrohbau);
        return view('ruckweisungrohbau', ['ruckweisungrohbau' => $ruckweisungrohbau,'autorefresh' => $this->autorefresh,
            'autorefreshtime' => $this->autorefreshtime,
            'datum_tage' => $this->datum_tage,
            'yesterday' => $this->yesterday,
            'time' => $this->time,
            'hours' => $this->hours,
            'schicht' => $this->schicht
		]);
    }

    public function store() {

        if (isset($_POST['datum_tage']) && isset($_POST['schicht']) && isset($_POST['typen']) && isset($_POST['grund']) /*&& isset($_POST['bemerkung'])*/) {
            $datum = $_POST['datum_tage'];
            $schicht = $_POST['schicht'];
            $typen = $_POST['typen'];
            $grund = $_POST['grund'];
            $bemerkung = $_POST['bemerkung'];

            if ($datum != '' && $schicht != '' && $typen != '' && $grund != ''/* && $bemerkung != ''*/) {
                App::get('database')->insert('STR_RUECKWEISUNG', [
                    'DATUM' => "TO_DATE('" . $datum . "', 'dd.mm.yyyy')",
                    'SCHICHT' => "'" . $schicht . "'",
                    'TYPE' => "'" . $typen . "'",
                    'GRUND' => "'" . $grund . "'",
                    'BEMERKUNG' => "'" . $bemerkung . "'"
                ]);
            }
        }

        return redirect('ruckweisungrohbau');
    }

    public function edit() {

        $id = $_POST['modalid'];
        //$datum = $_POST['mdatum_tage'];
        $datumFull = $_POST['mdatum_tage'];
        $datumArray = explode(" ", $datumFull);
        $datum = $datumArray[0];
        $schicht = $_POST['mschicht'];
        $typen = $_POST['mtypen'];
        $grund = $_POST['mgrund'];
        $bemerkung = $_POST['mbemerkung'];

        App::get('database')->update('STR_RUECKWEISUNG', [
            'DATUM' => "TO_DATE('" . $datum . "', 'dd.mm.yyyy')",
            'SCHICHT' => "'" . $schicht	 . "'",
            'TYPE' => "'" . $typen . "'",
            'GRUND' => "'" . $grund . "'",
            'BEMERKUNG' => "'" . $bemerkung . "'"], [
            'ID' => $id
        ]);

        return redirect('ruckweisungrohbau');
    }
		    
	public function delete() {
        if (isset($_POST['delete']) && $_POST['delete'] != '') {
            $id = $_POST['delete'];
            App::get('database')->delete('STR_RUECKWEISUNG', ['ID' => $id]);
        }
    }
    // set current Schicht and DS function
    private function setSchicht($hours, $schichten) {
        if ($hours != '') {
            $ds = '';
            $schicht = '';
            foreach ($hours as $key) {
                if ($this->time >= substr($key->STR_TIME, 0, 2)) {
                    $schicht = $key->SCHICHT;
                }
                //die(var_dump($this->time));
                if ($schicht == '') {
                    $schicht = 3;
                }
            }
            $this->schicht = $schicht;
            foreach ($schichten as $key) {
                for ($i = 0; $i <= 4; $i++) {
                    if (substr($key->SCHICHT, $i, 1) == $schicht) {
                        $ds = substr($key->TEAM, $i, 1);
                    }
                }
            }
            if ($ds != '') {
                $this->ds = $ds;
            }
        } else {
            foreach ($schichten as $key) {
                for ($i = 0; $i <= 4; $i++) {
                    if (substr($key->SCHICHT, $i, 1) == $this->schicht) {
                        $ds = substr($key->TEAM, $i, 1);
                    }
                }
            }
            $this->ds = $ds;
        }
    }

    private function utf8_encode_deep(&$input) {
        if (is_string($input)) {
            $input = utf8_encode($input);
        } else if (is_array($input)) {
            foreach ($input as &$value) {
                if (is_array($value)) {
                    foreach ($value as &$key) {
                        $this->utf8_encode_deep($key);
                    }
                    unset($key);
                } else {
                    $this->utf8_encode_deep($value);
                }
            }
            unset($value);
        } else if (is_object($input)) {
            $vars = array_keys(get_object_vars($input));
            foreach ($vars as $var) {
                $this->utf8_encode_deep($input->$var);
            }
        }
        return $input;
    }
}
