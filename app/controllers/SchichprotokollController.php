<?php

namespace App\Controllers;

use App\Core\App;
use DateTime;
use DateInterval;

class SchichtProtokollController {

    public function __construct() {

        $this->autorefresh = 'on';
        $this->autorefreshtime = '10';

        if (isset($_POST['autorefresh_checked'])) {
            $this->autorefresh = $_POST['autorefresh_checked'];
        }

        if (isset($_POST['autorefreshtime'])) {
            $this->autorefreshtime = $_POST['autorefreshtime'];
        }

        $this->halle_id = 3;
        $this->schicht = '';
        $this->ds = '';
        $this->datum_tage = date('d.m.Y H:i');
        $this->phpdate = new DateTime($this->datum_tage);

        $this->yesterday = new DateTime($this->datum_tage);
        $this->yesterday = $this->yesterday->sub(new DateInterval('P1D'))->format('d.m.Y');

        $this->time = $this->phpdate->format('H');
        $this->week = $this->phpdate->format('W');
        $this->year = $this->phpdate->format('Y');
        $this->datum_tage_sql = "TO_DATE('" . date('d.m.Y') . "','DD.MM.YYYY')";
        $this->datum_tage_sql_yesterday = "TO_DATE('" . date('d.m.Y', strtotime($this->yesterday)) . "','DD.MM.YYYY')";


        //$this->datum_tage_sql	= "TO_DATE('".date('Y-m-d')."','YYYY-MM-DD')";

        if (isset($_POST['halle_id']) && $_POST['halle_id'] != '') {

            if ($this->autorefresh == 'off') {
                $this->datum_tage = $_POST['datum_tage'];
            }

            $this->halle_id = $_POST['halle_id'];
            $this->schicht = $_POST['schicht'];
            //$this->ds				= $_POST['ds'];
            $this->phpdate = new DateTime($this->datum_tage);
            $this->week = $this->phpdate->format('W');
            $this->year = $this->phpdate->format('Y');
            $this->datum_tage_sql = strtotime($this->datum_tage);
            $this->datum_tage_sql = "TO_DATE('" . date('d.m.Y', $this->datum_tage_sql) . "','DD.MM.YYYY')";
        }


        if (isset($_POST['datum_tage_modal']) && $_POST['datum_tage_modal'] != '') {

            $this->autorefresh = $_POST['autorefresh_checked_modal'];
            $this->autorefreshtime = $_POST['autorefreshtime_modal'];

            $this->halle_id = $_POST['halle_id_modal'];
            $this->schicht = $_POST['schicht_modal'];
            $this->datum_tage = $_POST['datum_tage_modal'];
            $this->phpdate = new DateTime($this->datum_tage);
            $this->week = $this->phpdate->format('W');
            $this->year = $this->phpdate->format('Y');
            $this->datum_tage_sql = strtotime($this->datum_tage);
            $this->datum_tage_sql = "TO_DATE('" . date('d.m.Y', $this->datum_tage_sql) . "','DD.MM.YYYY')";
        }
    }

    private $art = '';
    private $typ = '';
    private $unterkategorien = [];
    private $filtered_storungen = [];
    private $filtered_errors = [];
    private $dauer_sum = [
        'l0_t' => 0,
        'l0_o' => 0,
        'l0_q' => 0,
        'l1_t' => 0,
        'l1_o' => 0,
        'l1_q' => 0,
        'l2_t' => 0,
        'l2_o' => 0,
        'l2_q' => 0
    ];

    private function utf8_encode_deep(&$input) {
        if (is_string($input)) {
            $input = utf8_encode($input);
        } else if (is_array($input)) {
            foreach ($input as &$value) {
                if (is_array($value)) {
                    foreach ($value as &$key) {
                        $this->utf8_encode_deep($key);
                    }
                    unset($key);
                } else {
                    $this->utf8_encode_deep($value);
                }
            }
            unset($value);
        } else if (is_object($input)) {
            $vars = array_keys(get_object_vars($input));
            foreach ($vars as $var) {
                $this->utf8_encode_deep($input->$var);
            }
        }
        return $input;
    }

    // set current Schicht and DS function
    private function setSchicht($hours, $schichten) {
        if ($hours != '') {
            $ds = '';
            $schicht = '';
            foreach ($hours as $key) {
                if ($this->time >= substr($key->STR_TIME, 0, 2)) {
                    $schicht = $key->SCHICHT;
                }
                //die(var_dump($this->time));
                if ($schicht == '') {
                    $schicht = 3;
                }
            }
            $this->schicht = $schicht;
            foreach ($schichten as $key) {
                for ($i = 0; $i <= 4; $i++) {
                    if (substr($key->SCHICHT, $i, 1) == $schicht) {
                        $ds = substr($key->TEAM, $i, 1);
                    }
                }
            }
            if ($ds != '') {
                $this->ds = $ds;
            }
        } else {
            foreach ($schichten as $key) {
                for ($i = 0; $i <= 4; $i++) {
                    if (substr($key->SCHICHT, $i, 1) == $this->schicht) {
                        $ds = substr($key->TEAM, $i, 1);
                    }
                }
            }
            $this->ds = $ds;
        }
    }

    // test if number is between two numbers
    private function isBetween($int, $min, $max) {
        return ($min <= $int && $int <= $max);
    }

    // set variables for insert or update record
    private function setInsertUpdateVariables() {
        for ($i = 1; $i <= 4; $i++) {
            if (isset($_POST['unterkategorie' . $i])) {
                $this->{'u' . $i} = $_POST['unterkategorie' . $i];
            } else {
                $this->{'u' . $i} = '';
            }
        }

        $this->id_user = 23;
        $this->von = $_POST['startzeit_h'] . ':' . $_POST['startzeit_m'];
        $this->von = date('H:i', strtotime($this->von));
        $this->bis = $_POST['endzeit_h'] . ':' . $_POST['endzeit_m'];
        $this->bis = date('H:i', strtotime($this->bis));
        $this->date = $this->datum_tage_sql;
        $this->komentar = $_POST['user_text'];
        $this->art = $_POST['arts'];
        $this->typ = (isset($_POST['typs'])) ? $_POST['typs'] : '';
        $this->dauer = $_POST['dauer'];
        $this->storung = $_POST['storungen'];
        $this->linie = $_POST['linien'];
        $this->schicht = $_POST['schicht_modal'];
        $this->halle = $this->halle_id;
    }

    // set all data for schichtprotokoll
    private function schichtprotokoll_data($my_id = '') {
        $halle_id = ($my_id != '') ? $my_id : $this->halle_id;
        // get Art data
        $this->art = App::get('database')->SelectWhere('STR_TYPART', ['COLUMNS' => 'ID, TEXT, ACTIVE'], ['TYPE' => "'A'", 'ACTIVE' => "'1'"], '', 'Art');
        // get Typ data
        $this->typ = App::get('database')->SelectWhere('STR_TYPART', ['COLUMNS' => 'ID, TEXT, ACTIVE'], ['TYPE' => "'T'", 'ACTIVE' => "'1'"], '', 'Typ');
        // get Schicht and Team data filtered by selected date for that week
        $this->schichten = App::get('database')->SelectWhere('STR_CALENDAR', ['COLUMNS' => 'TEAM, SCHICHT'], ['ACTIVE' => "'1'",
            'WEEK' => $this->week,
            "TO_CHAR(STR_DATE,'YYYY')" => "'" . $this->year . "'"
                ], '', 'Schichten');
        // get starting hours of all Schichts
        $this->hours = App::get('database')->SelectAll('STR_CALENDAR_TIME', 'Time', '');

        // set current Schicht and DS if autorefresh id on
        if ($this->autorefresh == 'on') {
            $this->setSchicht($this->hours, $this->schichten);
        } else {
            $this->setSchicht('', $this->schichten);
        }

        if ($this->isBetween($this->time, 0, 5) && $this->schicht == 3 && $this->autorefresh == 'on') {
            $this->datum_tage = $this->yesterday;
            $this->datum_tage_sql = $this->datum_tage_sql_yesterday;
        }

        // get errors for selected date schicht and halle
        $this->filtered_errors = App::get('database')->SelectWhere('VIEW_ERRORS', ['COLUMNS' => '*'], ['DATUM' => $this->datum_tage_sql,
            'SCHICHT' => $this->schicht,
            'HALLE' => $halle_id], '', 'Errors');

        $yesterday = "(DATUM = " . $this->datum_tage_sql . "-1 AND TO_TIMESTAMP(VON,'HH24:MI') BETWEEN TO_TIMESTAMP('22:00','HH24:MI') AND TO_TIMESTAMP('23:59','HH24:MI')) ";
        $where = "WHERE " . $yesterday . " AND SCHICHT='" . $this->schicht . "' AND HALLE ='" . $halle_id . "'";
        $temps = App::get('database')->SelectWhere2('VIEW_ERRORS', ['COLUMNS' => '*'], $where, '', 'Errors');


        // fill array for organisatorische and technische erros for Linie 1 and Line 2 and change date format
        foreach ($this->filtered_errors as $filtered_error) {
            $my_date = $filtered_error->DATUM;
            if (strpos($my_date, '-') !== false) {
                $year = substr($my_date, 0, 4);
                $month = substr($my_date, 5, 2);
                $day = substr($my_date, 8, 2);
                $corected_date = $day . '.' . $month . '.' . $year;
                $filtered_error->DATUM = $corected_date;
            }

            if ($filtered_error->HALLE == '5') {
                $filtered_error->HALLE = '4A';
            }
            if ($filtered_error->LINIE == 0 && $filtered_error->ART == 'O') {
                $this->dauer_sum['l0_o'] += $filtered_error->DAUER;
            }
            if ($filtered_error->LINIE == 0 && $filtered_error->ART == 'T') {
                $this->dauer_sum['l0_t'] += $filtered_error->DAUER;
            }
            if ($filtered_error->LINIE == 0 && $filtered_error->ART == 'Q') {
                $this->dauer_sum['l0_q'] += $filtered_error->DAUER;
            }
            if ($filtered_error->LINIE == 1 && $filtered_error->ART == 'O') {
                $this->dauer_sum['l1_o'] += $filtered_error->DAUER;
            }
            if ($filtered_error->LINIE == 1 && $filtered_error->ART == 'T') {
                $this->dauer_sum['l1_t'] += $filtered_error->DAUER;
            }
            if ($filtered_error->LINIE == 1 && $filtered_error->ART == 'Q') {
                $this->dauer_sum['l1_q'] += $filtered_error->DAUER;
            }
            if ($filtered_error->LINIE == 2 && $filtered_error->ART == 'O') {
                $this->dauer_sum['l2_o'] += $filtered_error->DAUER;
            }
            if ($filtered_error->LINIE == 2 && $filtered_error->ART == 'T') {
                $this->dauer_sum['l2_t'] += $filtered_error->DAUER;
            }
            if ($filtered_error->LINIE == 2 && $filtered_error->ART == 'Q') {
                $this->dauer_sum['l2_q'] += $filtered_error->DAUER;
            }
        }
        //$this->filtered_errors = $this->utf8_encode_deep($this->filtered_errors);
        // get Errotypes data
        $this->filtered_storungen = App::get('database')->SelectWhere('STR_STORUNG', ['COLUMNS' => '*'], ['ACTIVE' => '1'], 'NR ASC', 'Storungen');
        //$this->filtered_storungen = $this->utf8_encode_deep($this->filtered_storungen);
        // get list of all unterkategorien
        $this->unterkategorien = App::get('database')->SelectWhere('STR_UNTERKATEGORIE', ['COLUMNS' => '*'], ['ACTIVE' => '1'], 'TEXT ASC', 'Unterkategorien');
        $this->unterkategorien = $this->utf8_encode_deep($this->unterkategorien);
        // get Errotypes filtered by Halle
        foreach ($this->filtered_storungen as $key => $value) {
            if ($value->{'H' . $this->halle_id . '_A'} == 0) {
                unset($this->filtered_storungen[$key]);
            }
        }



        // set eglible times for selection of error duration
        $this->time_for_schicht = App::get('database')->SelectWhere('STR_CALENDAR_TIME', ['COLUMNS' => '*'], ['SCHICHT' => $this->schicht], '', 'Time');
    }

    public function schichtprotokoll($my_id = '') {

        $halle_id = ($my_id != '') ? $my_id : $this->halle_id;
        $this->schichtprotokoll_data($my_id);
        //die(var_dump($this->datum_tage_sql_yesterday));

        return view('schichtprotokoll', ['autorefresh' => $this->autorefresh,
            'autorefreshtime' => $this->autorefreshtime,
            'filtered_errors' => $this->filtered_errors,
            'filtered_storungen' => $this->filtered_storungen,
            'unterkategorien' => $this->unterkategorien,
            'halle_id' => $halle_id,
            'datum_tage' => $this->datum_tage,
            'yesterday' => $this->yesterday,
            'hours' => $this->hours,
            'time' => $this->time,
            'time_for_schicht' => substr($this->time_for_schicht[0]->STR_TIME, 0, 2),
            'schicht' => $this->schicht,
            'schichten' => $this->schichten,
            'ds' => $this->ds,
            'arts' => $this->art,
            'typs' => $this->typ,
            'dauer_sum' => $this->dauer_sum
        ]);
    }

    public function store() {

        $this->setInsertUpdateVariables();

        App::get('database')->insert('STR_ERRORS', [
            'VON' => "'" . $this->von . "'",
            'BIS' => "'" . $this->bis . "'",
            'DATUM' => $this->date,
            'KOMENTAR' => "'" . $this->komentar . "'",
            'ART' => "'" . $this->art . "'",
            'TYP' => "'" . $this->typ . "'",
            'ID_USER' => "'" . $this->id_user . "'",
            'DAUER' => "'" . $this->dauer . "'",
            'STORUNG' => "'" . $this->storung . "'",
            'U1' => "'" . $this->u1 . "'",
            'U2' => "'" . $this->u2 . "'",
            'U3' => "'" . $this->u3 . "'",
            'U4' => "'" . $this->u4 . "'",
            'LINIE' => "'" . $this->linie . "'",
            'SCHICHT' => "'" . $this->schicht . "'",
            'HALLE' => "'" . $this->halle . "'"
        ]);

        $this->schichtprotokoll();
    }

    public function edit() {
        $this->setInsertUpdateVariables();

        $error_id = $_POST['error_id'];


        App::get('database')->update('STR_ERRORS', [
            'VON' => "'" . $this->von . "'",
            'BIS' => "'" . $this->bis . "'",
            'DATUM' => $this->date,
            'KOMENTAR' => "'" . $this->komentar . "'",
            'ART' => "'" . $this->art . "'",
            'TYP' => "'" . $this->typ . "'",
            'ID_USER' => "'" . $this->id_user . "'",
            'DAUER' => "'" . $this->dauer . "'",
            'STORUNG' => "'" . $this->storung . "'",
            'U1' => "'" . $this->u1 . "'",
            'U2' => "'" . $this->u2 . "'",
            'U3' => "'" . $this->u3 . "'",
            'U4' => "'" . $this->u4 . "'",
            'LINIE' => "'" . $this->linie . "'",
            'SCHICHT' => "'" . $this->schicht . "'",
            'HALLE' => "'" . $this->halle . "'"
                ], [
            'ID' => $error_id
        ]);

        $this->schichtprotokoll($this->halle);
    }

    public function delete() {
        if (isset($_POST['delete']) && $_POST['delete'] != '') {
            $id = $_POST['delete'];
            $halle_id_delete = $_POST['halle_id_delete'];
            App::get('database')->delete('STR_ERRORS', ['ID' => $id]);
        }
        $this->schichtprotokoll($halle_id_delete);
    }

}
