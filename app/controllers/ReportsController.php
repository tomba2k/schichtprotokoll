<?php

namespace App\Controllers;

use App\Core\App;
use DateTime;

class ReportsController {

    public $datum_tage_sql;
    private $HALLE;

    public function __construct() {
        $this->halle_id = 3;
        $this->datum_tage = date('d.m.Y H:i');
        $this->phpdate = new DateTime($this->datum_tage);
        $this->order = 'SCHICHT, LINIE, to_number(DAUER) DESC';

        //$this->datum_tage_sql = "TO_DATE('" . date('d.m.Y') . "','DD.MM.YYYY')";
        $this->datum_tage_sql = "TO_DATE('" . date('Y-m-d') . "','YYYY-MM-DD')";

        if (isset($_POST['halle']) || isset($_POST['datum_tage'])) {

            $this->halle_id = $_POST['halle'];
            $this->datum_tage = $_POST['datum_tage'];
            $this->phpdate = new DateTime($this->datum_tage);

            $this->datum_tage_sql = strtotime($this->datum_tage);
            // $this->datum_tage_sql = "TO_DATE('" . date('d.m.Y', $this->datum_tage_sql) . "','DD.MM.YYYY')";
            $this->datum_tage_sql = "TO_DATE('" . date('Y-m-d', $this->datum_tage_sql) . "','YYYY-MM-DD')";
        }

        if (isset($_POST['datum_tage_von']) || isset($_POST['datum_tage_bis'])) {
            if ($_POST['datum_tage_von'] != '') {
                $this->datum_tage_von = $_POST['datum_tage_von'];
                $this->datum_tage_sql_von = strtotime($this->datum_tage_von);
                $this->datum_tage_sql_von = "TO_DATE('" . date('d.m.Y', $this->datum_tage_sql_von) . "','DD.MM.YYYY')";
            } else {
                $this->datum_tage_sql_von = '';
            }
            if ($_POST['datum_tage_bis'] != '') {
                $this->datum_tage_bis = $_POST['datum_tage_bis'];
                $this->datum_tage_sql_bis = strtotime($this->datum_tage_bis);
                $this->datum_tage_sql_bis = "TO_DATE('" . date('d.m.Y', $this->datum_tage_sql_bis) . "','DD.MM.YYYY')";
            } else {
                $this->datum_tage_sql_bis = '';
            }
        }
    }

    private function fixDateAndHalle($array) {
        foreach ($array as $alle_storunge) {
            if ($alle_storunge->HALLE == '5') {
                $alle_storunge->HALLE = '4A';
            }

            $my_date = $alle_storunge->DATUM;
            if (strpos($my_date, '-') !== false) {
                $year = substr($my_date, 0, 4);
                $month = substr($my_date, 5, 2);
                $day = substr($my_date, 8, 2);
                $corected_date = $day . '.' . $month . '.' . $year;
                $alle_storunge->DATUM = $corected_date;
            }
        }
        return $array;
    }

    private function utf8_encode_deep(&$input) {
        if (is_string($input)) {
            $input = utf8_encode($input);
        } else if (is_array($input)) {
            foreach ($input as &$value) {
                if (is_array($value)) {
                    foreach ($value as &$key) {
                        $this->utf8_encode_deep($key);
                    }
                    unset($key);
                } else {
                    $this->utf8_encode_deep($value);
                }
            }
            unset($value);
        } else if (is_object($input)) {
            $vars = array_keys(get_object_vars($input));
            foreach ($vars as $var) {
                $this->utf8_encode_deep($input->$var);
            }
        }
        return $input;
    }

    public function exportCSV() {
        if (isset($_POST['report'])) {

            $report = $_POST['report'];

            switch ($report) {
                case 'schichtprotokoll':
                    $today = "(DATUM = " . $this->datum_tage_sql . " AND TO_TIMESTAMP(SCHICHT_BEGIN,'HH24:MI') BETWEEN TO_TIMESTAMP('06:00','HH24:MI') AND TO_TIMESTAMP('22:00','HH24:MI'))";
                    //$today = "(DATUM = ".$this->datum_tage_sql." AND TO_TIMESTAMP(VON,'HH24:MI')>=TO_TIMESTAMP('06:00','HH24:MI'))";
                    $tomorrow = "(DATUM = " . $this->datum_tage_sql . "+1 AND TO_TIMESTAMP(BIS,'HH24:MI')<=TO_TIMESTAMP('05:59','HH24:MI'))";
                    $columns = 'STORUNG_TEXT,U1_TEXT,U2_TEXT,U3_TEXT,U4_TEXT,ART,TYP,KOMENTAR,LINIE,SCHICHT,SCHICHT_BEGIN,HALLE,VON,DAUER,date_format_yy (datum) as DATUM';

                    if ($this->halle_id == 0) {
                        $halle = '';
                    } else {
                        $halle = "AND HALLE='" . $this->halle_id . "'";
                    }
                    $where = "WHERE " . $today . $halle;


                    $alle_storungen = App::get('database')->SelectWhere2('VIEW_ERRORS', ['COLUMNS' => $columns], $where, $this->order, 'Errors');


                    if (!empty($alle_storungen)) {
                        $delimiter = ';';
                        $schicht_txt = '';
                        $filename = 'schichtprotokoll_halle' . $this->halle_id . '_' . $this->datum_tage . '.csv';

                        //create a file pointer
                        $f = fopen('php://memory', 'w');

                        //set column headers
                        $fields = array('Datum', 'Art', 'Schicht', 'Linie', 'Anfang', 'Dauer (Min)', 'St�rung', 'Ebene 1', 'Ebene 2', 'Ebene 3', 'Ebene 4', 'Bemerkung manuell', 'Model');
                        fputcsv($f, $fields, $delimiter);

                        //output each row of the data, format line as csv and write to file pointer
                        foreach ($alle_storungen as $row) {
                            if ($row->SCHICHT == 1) {
                                $schicht_txt = 'Fr�hschicht';
                            };
                            if ($row->SCHICHT == 2) {
                                $schicht_txt = 'Sp�tschicht';
                            };
                            if ($row->SCHICHT == 3) {
                                $schicht_txt = 'Nachtschicht';
                            };
                            $lineData = array($row->DATUM, $row->ART, $schicht_txt, $row->LINIE, $row->VON, $row->DAUER, $row->STORUNG_TEXT, $row->U1_TEXT, $row->U2_TEXT, $row->U3_TEXT, $row->U4_TEXT, $row->KOMENTAR, $row->TYP);
                            fputcsv($f, $lineData, $delimiter);
                        }

                        //move back to beginning of file
                        fseek($f, 0);

                        //set headers to download file rather than displayed
                        header('Content-Type: text/csv');
                        header('charset=ISO-8859-15');
                        header('Content-Disposition: attachment; filename="' . $filename . '";');

                        //output all remaining data on a file pointer
                        fpassthru($f);
                    }
                    exit;
                    break;

                case 'alle_storungen':
                    if ($this->datum_tage_sql_von != '' && $this->datum_tage_sql_bis != '') {
                        $where = " WHERE DATUM BETWEEN " . $this->datum_tage_sql_von . " AND " . $this->datum_tage_sql_bis;
                    }
                    if ($this->datum_tage_sql_von != '' && $this->datum_tage_sql_bis == '') {
                        $where = " WHERE DATUM >= " . $this->datum_tage_sql_von;
                    }

                    if ($this->datum_tage_sql_von == '' && $this->datum_tage_sql_bis != '') {
                        $where = " WHERE DATUM <= " . $this->datum_tage_sql_bis;
                    }
                    if ($this->datum_tage_sql_von == '' && $this->datum_tage_sql_bis == '') {
                        $where = '';
                    }

                    $columns = 'date_format_yyyy (datum) as DATUM,SCHICHT_BEGIN,VON,DAUER,ART,STORUNG_TEXT,HALLE,LINIE,U1_TEXT,U2_TEXT,U3_TEXT,U4_TEXT,KOMENTAR,TYP';
                    $alle_storungen = App::get('database')->SelectWhere2('VIEW_ERRORS', ['COLUMNS' => $columns], $where, 'DATUM DESC', 'Errors');
                    $alle_storungen = $this->fixDateAndHalle($alle_storungen);

                    if (!empty($alle_storungen)) {
                        $delimiter = ';';
                        $schicht_txt = '';
                        $filename = 'alle_storungen_' . $this->datum_tage . '.csv';

                        //create a file pointer
                        $f = fopen('php://memory', 'w');

                        //set column headers
                        $fields = array('Datum', 'Schichtbeginn', 'Anfangszeit', 'Dauer (Min)', 'Art', 'St�rung', 'Halle', 'Linie', 'Ebene 1', 'Ebene 2', 'Ebene 3', 'Ebene 4', 'Bemerkung manuell', 'Model');
                        fputcsv($f, $fields, $delimiter);

                        //output each row of the data, format line as csv and write to file pointer
                        foreach ($alle_storungen as $row) {
                            $lineData = array($row->DATUM, $row->SCHICHT_BEGIN, $row->VON, $row->DAUER, $row->ART, $row->STORUNG_TEXT, $row->HALLE, $row->LINIE, $row->U1_TEXT, $row->U2_TEXT, $row->U3_TEXT, $row->U4_TEXT, $row->KOMENTAR, $row->TYP);
                            fputcsv($f, $lineData, $delimiter);
                        }

                        //move back to beginning of file
                        fseek($f, 0);

                        //set headers to download file rather than displayed
                        header('Content-Type: text/csv');
                        header('charset=ISO-8859-15');
                        header('Content-Disposition: attachment; filename="' . $filename . '";');

                        //output all remaining data on a file pointer
                        fpassthru($f);
                    }
                    exit;
                    break;
            }
        }
    }

    public function alle_storungen() {
        $columns = 'ID_ERROR,STORUNG_ID,STORUNG_TEXT,U1_ID,U1_TEXT,U2_ID,U2_TEXT,U3_ID,U3_TEXT,U4_ID,U4_TEXT,ART_ID,ART,TYP_ID,TYP,KOMENTAR,LINIE,SCHICHT,SCHICHT_BEGIN,HALLE,VON,BIS,DAUER,date_format_yyyy (datum) as DATUM,STANDARDSTORUNGSART,USER_ID,USERNAME';
        $alle_storungen = App::get('database')->SelectWhere('VIEW_ERRORS', ['COLUMNS' => $columns], '', 'DATUM DESC', 'Errors');
        $alle_storungen = $this->fixDateAndHalle($alle_storungen);


        return view('reports.alle_storungen', ['alle_storungen' => $alle_storungen]);
    }

    public function schichtprotokoll() {

        $arts = App::get('database')->SelectWhere('STR_TYPART', ['COLUMNS' => 'TEXT'], ['TYPE' => "'A'", 'ACTIVE' => "'1'"], '', 'Art');

        foreach ($arts as $key) {
            $art[] = $key->TEXT;
        }
        //$art = ['T','O','Q'];

        $today = "(DATUM = " . $this->datum_tage_sql . " AND TO_TIMESTAMP(SCHICHT_BEGIN,'HH24:MI') BETWEEN TO_TIMESTAMP('06:00','HH24:MI') AND TO_TIMESTAMP('22:00','HH24:MI'))";
        //$today = "(DATUM = ".$this->datum_tage_sql." AND TO_TIMESTAMP(VON,'HH24:MI')>=TO_TIMESTAMP('06:00','HH24:MI'))";
        //$tomorrow = "(DATUM = ".$this->datum_tage_sql."+1 AND TO_TIMESTAMP(BIS,'HH24:MI')<=TO_TIMESTAMP('05:59','HH24:MI'))";
        $columns = 'ID_ERROR, STORUNG_ID, STORUNG_TEXT,	U1_ID, U1_TEXT, U2_ID, U2_TEXT,	U3_ID, U3_TEXT,	U4_ID, U4_TEXT, ART_ID, ART, TYP_ID, TYP, KOMENTAR,	LINIE, SCHICHT, SCHICHT_BEGIN, HALLE, VON, BIS, DAUER, date_format_yy (datum) as DATUM,	STANDARDSTORUNGSART, USER_ID, USERNAME';

        foreach ($art as $key => $value) {
            if ($this->halle_id == 0) {
                $halle = '';
            } else {
                $halle = "AND HALLE='" . $this->halle_id . "'";
            }
            $where = "WHERE " . $today . " AND ART='" . $value . "'" . $halle;

            $alle_storungen[$key] = App::get('database')->SelectWhere2('VIEW_ERRORS', ['COLUMNS' => $columns], $where, $this->order, 'Errors');
            /*
              $where = "WHERE ".$tomorrow. " AND ART='".$value."' AND HALLE='".$this->halle_id."'";
              $temps = App::get('database')->SelectWhere2('VIEW_ERRORS',
              ['COLUMNS'	=> $columns],
              $where,
              'DATUM DESC',
              'Errors');
              foreach ($temps as $temp) {
              if (!empty($temp)){
              $alle_storungen[$key][] = $temp;
              }
              }
             */
        }

        return view('reports.schichtprotokoll', ['halle_id' => $this->halle_id,
            'datum_tage' => $this->datum_tage,
            'alle_storungen' => $alle_storungen
        ]);
    }

    public function advancedreport() {
//        $today = "(DATUM = " . $this->datum_tage_sql . " AND TO_TIMESTAMP(SCHICHT_BEGIN,'HH24:MI') BETWEEN TO_TIMESTAMP('06:00','HH24:MI') AND TO_TIMESTAMP('22:00','HH24:MI'))";
//        $where = "WHERE " . $today . " AND HALLE='" . $this->halle_id . "'";
//        $alle_storungen = App::get('database')->SelectWhere2('VIEW_ERRORS', ['COLUMNS' => '*'], $where, $this->order, 'Errors');

$rep = null;
        $std = new \stdClass();
        $std->DATUM = $this->datum_tage_sql;
        $std->HALLE = $this->halle_id;
        $report = new ComplexReport($std);
        if ($_POST) {
            $rep = new ComplexReportDynamic($this->datum_tage, $this->halle_id);
        }
//        foreach ($alle_storungen as $value) {
//            $report->addErrorRow($value);
//        }

        $report->calculateSum();

        return view('reports.advanced', ['halle_id' => $this->halle_id,
            'datum_tage' => $this->datum_tage,
            'report' => $report,
            'rep' => $rep
        ]);
    }

    public function listreport() {
        $today = "(DATUM = " . $this->datum_tage_sql . " AND TO_TIMESTAMP(SCHICHT_BEGIN,'HH24:MI') BETWEEN TO_TIMESTAMP('06:00','HH24:MI') AND TO_TIMESTAMP('22:00','HH24:MI'))";
        $where = "WHERE " . $today . " AND HALLE='" . $this->halle_id . "'";
        $alle_storungen = App::get('database')->SelectWhere2('VIEW_ERRORS', ['COLUMNS' => '*'], $where, $this->order, 'Errors');



        $std = new \stdClass();
        $std->DATUM = $this->datum_tage_sql;
		$std->HALLE = $this->halle_id;
        $report = new ComplexReport($std);

//        foreach ($alle_storungen as $value) {
//            $report->addListReportRow($value);
//        }

        return view('reports.list', ['halle_id' => $this->halle_id,
            'datum_tage' => $this->datum_tage,
            'report' => $report
        ]);
    }

    public function URNreport() {
        $report = NULL;
        $RueckweisungReport = NULL;
        $qualite = NULL;
        if ($_POST && $_POST['datum_tage'] != NULL && $_POST['halle'] != NULL) {
            $date = $_POST['datum_tage']; //"02.02.2018";
            $halle = $_POST['halle'];
            $report = new UrnReport($date, $halle);
            $report->date = $date;
            $report->halle = $halle;
            $report->getAllRows();
            if ($_POST['halle'] == 3) {
                $RueckweisungReport = new RueckweisungReport($date, $halle);
            }
            $qualite = new QualitatsschwerpunkteReport($date, $halle);
        }


        return view('reports.urn', ['halle_id' => $this->halle_id,
            'datum_tage' => $this->datum_tage,
            'report' => $report,
            'qa' => $qualite,
            'ru' => $RueckweisungReport
        ]);
    }

    public function ALDreport() {
        $rep = NULL;
        if ($_POST) {
            $rep = new ComplexReportDynamic($this->datum_tage, $this->halle_id);
        }
        return view('reports.ald', ['halle_id' => $this->halle_id,
            'datum_tage' => $this->datum_tage,
            'rep' => $rep
        ]);
    }

}

class ComplexReport {

    public $Sch = array();
    public $Sum;
    protected $Datum;
    protected $Halle;
    public $ReportErrors;
    public $ReportSum;

    public function __construct($std) {
        $this->Halle = $std->HALLE;
        $this->Datum = $std->DATUM;


        if ($this->Datum != null && $this->Halle != null) {
            $this->_getReportErrors();
            $this->_createSchichts($std->DATUM);
            $this->_getReportData();
        }


        $this->Sum = new ErrorRow();
    }

    public function addErrorRow($param) {
        if (!array_key_exists($param->SCHICHT, $this->Sch)) {
            //if($this->Sch[$param->SCHICHT] == null){
            $this->Sch[$param->SCHICHT] = new Schihte($param, $this);
        }
        $this->Sch[$param->SCHICHT]->addRow($param);
    }

    public function addListReportRow($param) {
        if (!array_key_exists($param->SCHICHT, $this->Sch)) {
            $this->Sch[$param->SCHICHT] = new Schihte($param, $this);
        }
        $this->Sch[$param->SCHICHT]->addListReportRow($param);
    }

    public function calculateSum() {
        foreach ($this->Sch as $value) {
            $this->Sum->FAD += intval($value->sumRow->FAD);
            $this->Sum->GAD += intval($value->sumRow->GAD);
            $this->Sum->KTL += intval($value->sumRow->KTL);
            $this->Sum->Nacharbeit += intval($value->sumRow->Nacharbeit);
            $this->Sum->Orga += intval($value->sumRow->Orga);
            $this->Sum->PVCVersor += intval($value->sumRow->FAD);
            $this->Sum->RuckfuhrungEHBLackskid += intval($value->sumRow->RuckfuhrungEHBLackskid);
            $this->Sum->RuckfuhrungLackskid += intval($value->sumRow->RuckfuhrungLackskid);
            $this->Sum->RuckfuhrungRohSkid += intval($value->sumRow->RuckfuhrungRohSkid);
            $this->Sum->SDM += intval($value->sumRow->SDM);
            $this->Sum->Schweller += intval($value->sumRow->Schweller);
            $this->Sum->Sonst += intval($value->sumRow->Sonst);
            $this->Sum->SpeicherFuller += intval($value->sumRow->SpeicherFuller);
            $this->Sum->SpeicherH2 += intval($value->sumRow->SpeicherH2);
            $this->Sum->SpeicherKTL += intval($value->sumRow->SpeicherKTL);
            $this->Sum->TrocknerKTL += intval($value->sumRow->TrocknerKTL);
            $this->Sum->TrocknerUBS += intval($value->sumRow->TrocknerUBS);
            $this->Sum->UBS += intval($value->sumRow->UBS);
            $this->Sum->VBH += intval($value->sumRow->VBH);
        }
    }

    public function AddError($param) {
        if (!array_key_exists($param->SCHICHT, $this->Sch)) {
            $this->Sch[$param->SCHICHT] = new Schihte($param, $this);
        }
        $this->Sch[$param->SCHICHT]->addRow($param);
    }

    private function _createSchichts($datum) {
        for ($s = 1; $s <= 3; $s++) {
            $param = new \stdClass();
            $param->DATUM = $datum;
            $param->SCHICHT = $s;
            $this->Sch[$s] = new Schihte($param, $this);
        }
    }

    private function _getReportData() {
        $order = 'SCHICHT asc';
        $today = "(DATUM = " . $this->Datum . " AND TO_TIMESTAMP(SCHICHT_BEGIN,'HH24:MI') BETWEEN TO_TIMESTAMP('06:00','HH24:MI') AND TO_TIMESTAMP('22:00','HH24:MI'))";
        $where = "WHERE " . $today . " AND HALLE='" . $this->Halle . "'";
        $alle_storungen = App::get('database')->SelectWhere2('VIEW_ERRORS', ['COLUMNS' => '*'], $where, $order, 'Errors');
        $this->_loadReportData($alle_storungen);
    }

    private function _getReportErrors() {
        $order = '';
        switch ($this->Halle) {
            case 3:
                $where = " where H3_A = '1'";


                break;
            case 4:
                $where = " where H4_A = '1'";


                break;

            default:
                $where = " where H5_A = '1'";
                break;
        }
        $tmp = App::get('database')->SelectWhere2('STR_STORUNG', ['COLUMNS' => '*'], $where, $order, 'Storungen');

        $errors = array();
        $sum = array();
        foreach ($tmp as $value) {
            $errors[$value->ID] = $value->TEXT;
            $sum[$value->ID] = 0;
        }
        $this->ReportErrors = $errors;
        $this->ReportSum = $sum;
    }

    private function _loadReportData($param) {
        if (count($param) > 0) {
            foreach ($param as $value) {
                $this->AddError($value);
                $this->ReportSum[$value->STORUNG_ID] += $value->DAUER;
            }
        }
    }

    public function generateHtmlTable() {
        $table = '<style>
            ' /* td {
                  border-collapse:collapse;
                  border: 1px black solid;
                  }
                  tr:nth-of-type(5) td:nth-of-type(1) {
                  visibility: hidden;
                  }
                  .rotates {
                  -moz-transform: rotate(-90.0deg);
                  -o-transform: rotate(-90.0deg);
                  -webkit-transform: rotate(-90.0deg);
                  filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);
                  -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)";
                  }
                 * 
                 * */
                . ''
                . 'td {
    border: 1px black solid;
    padding: 5px;
}
.rotateo {
  text-align: center;
  white-space: nowrap;
  vertical-align: middle;
  width: 1.5em;
  height: 120px;
}
.rotateo div {
     -moz-transform: rotate(-90.0deg);  /* FF3.5+ */
       -o-transform: rotate(-90.0deg);  /* Opera 10.5 */
  -webkit-transform: rotate(-90.0deg);  /* Saf3.1+, Chrome */
             filter:  progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083);  /* IE6,IE7 */
         -ms-filter: "progid:DXImageTransform.Microsoft.BasicImage(rotation=0.083)"; /* IE8 */

    position: relative;
    top: 30%;
}'
                . '</style>'
                . '<table border=1>';
        $table .= $this->generateHtmlTableHeader();
        $table .= $this->generateHtmlTableBody();
        $table .= '</table>'
                . '<script>'
                . ' $(".fr").each(function(index) {
                    var $ob = $($(this).children());
                    $ob.each(function(ind, val){
                        $v = $(val);
                        
                       // $($v.children()[0]).css("margin-top", "70px");
                        $($v.children()[0]).css("width", "20px");
                    console.log($v.children()[0]);
                    });
                    $a = $($ob[0]); //.css("background","red");
                    //$($a.children()[0]);
                 //   console.log($ob);
                   // $ob.css("background","red");
});'
                . '</script>';

        return $table;
    }

    public function generateHtmlTableBody() {
        $tbody = "<tbody>";

        foreach ($this->Sch as $s) {

            $tbody .= $this->generateSchichtBlock($s);
        }

        $tbody .= $this->generateSchichtSun($this->ReportSum);
        $tbody .= "</tbody>";


        return $tbody;
    }

    public function generateHtmlTableHeader() {
        $thead = "<thead><tr class = 'fr'>"
                . "<th class='rotateo'><div>Schicht</div></th>"
                . "<th class='rotateo'><div>Art</div></th>";
        foreach ($this->ReportErrors as $key => $value) {
            $thead .= "<th class='rotateo'><div>" . $value . "</div></th>";
        }
        $thead .= "<th class='rotateo'><div>Linie</div></th>"
                . "<th class='rotateo'><div>Uhrzeit</div></th>"
                . "<th class='rotateo'><div>Modell</div></th><th>Störungsbereich</th><th>Bemerkung</th></tr></thead>";
        return $thead;
    }

    public function generateSchichtBlock($s) {
        $out = "";
        $cnt = 0;
        foreach ($s->errors as $index => $r) {
            $out .= $this->generateSchichtRow($r, $s, $index);
        }
        $out .= $this->generateSchichtSun($s->schichtSum);
        return $out;
    }

    public function generateSchichtRow($r, $s, $index) {

        $rows = '<tr style="background:white;">';
        $rows .= '<td></td>';
        $rows .= '<td>' . $r->originalError->ART . '</td>';

        foreach ($this->ReportErrors as $key => $value) {
            $rows .= "<td>";
            if ($r->storungId == $key) {
                $rows .= $r->dauer;
            }
            $rows .= "</td>";
        }

        $rows .= '<td>' . $r->Linie . '</td>'
                . '<td>' . $r->Uhrzeit . '</td>'
                . '<td>' . $r->type . '</td>'
                . '<td>' . $r->Bemerkung . '</td>'
                . '<td>' . $r->Storungsbereich . '</td>'
                . '</tr>';
        return $rows;
    }

    public function generateSchichtSun($s, $color = null) {
        $rows = '<tr style="background:red;">'
                . '<td></td>'
                . '<td></td>';
        foreach ($s as $value) {
            $rows .= "<td>";
            if ($value != null && $value != 0) {
                $rows .= $value;
            }
            $rows .= "</td>";
        }
        $rows .= '<td></td>'
                . '<td></td>'
                . '<td></td>'
                . '<td></td>'
                . '<td></td>'
                . '</tr>';
        return $rows;
    }

}

class Schihte {

    public $errors = array();
    public $sumRow;
    public $team;
    public $schicht;
    private $report;
    public $schichtSum = array();

    public function __construct($param, $parent) {
        $this->report = $parent;
        foreach ($this->report->ReportErrors as $key => $value) {
            $this->schichtSum[$key] = 0;
        }
        $this->sumRow = new ErrorRow();

        $weekNum = intval(date("W", strtotime($param->DATUM)));
        $year = date("Y", strtotime($param->DATUM));



        $where = " WHERE trunc(STR_DATE) = (Select TRUNC (Trunc(to_date ('$year','YYYY'),'yyyy')+($weekNum-1)*7,'IW') from dual)";
        $Cal = App::get('database')->SelectWhere2('STR_CALENDAR', ['COLUMNS' => "*"], $where, "", 'StrCalendar');

        if ($Cal != NULL && isset($Cal[0])) {
            $sc = explode("|", $Cal[0]->TEAM);
            $this->team = $sc[$param->SCHICHT - 1];
        }
        $this->schicht = $param->SCHICHT;
    }

    public function GetLabelRowspan() {
        return(count($this->errors) - 2);
    }

    public function addRow($param) {
        if ($this->errors != null) {
            foreach ($this->errors as $key => $val) {
                if ($val->storungId == $param->STORUNG_ID && $val->parrentId == $param->U1_ID && $val->Bemerkung == $param->KOMENTAR && $val->Uhrzeit == $param->VON) {
                    switch ($param) {
                        case $param->STORUNG_ID == 49:
                            $this->value = $this->VBH += $param->DAUER;
                            $this->sumRow->VBH += $param->DAUER;
                            return;
                            break;
                        case $param->STORUNG_ID == 50:
                            $this->value = $this->KTL += $param->DAUER;
                            $this->sumRow->KTL += $param->DAUER;
                            return;
                            break;
                        case $param->STORUNG_ID == 51:
                            $this->value = $this->GAD += $param->DAUER;
                            $this->sumRow->GAD += $param->DAUER;
                            return;
                            break;
                        case $param->STORUNG_ID == 52:
                            $this->value = $val->UBS += $param->DAUER;
                            $this->sumRow->UBS += $param->DAUER;
                            return;
                            break;
                        case $param->STORUNG_ID == 53:
                            $this->value = $this->Schweller += $param->DAUER;
                            $this->sumRow->Schweller += $param->DAUER;
                            return;
                            break;
                        case $param->STORUNG_ID == 54:
                            $this->value = $this->SDM += $param->DAUER;
                            $this->sumRow->SDM += $param->DAUER;
                            return;
                            break;
                        case $param->STORUNG_ID == 55:
                            $this->value = $this->FAD += $param->DAUER;
                            $this->sumRow->FAD += $param->DAUER;
                            return;
                            break;
                        case $param->STORUNG_ID == 85:
                            $this->value = $this->PVCVersor += $param->DAUER;
                            $this->sumRow->PVCVersor += $param->DAUER;
                            break;
                        case $param->STORUNG_ID == 81:
                            $this->value = $this->Nacharbeit += $param->DAUER;
                            $this->sumRow->Nacharbeit += $param->DAUER;
                            return;
                            break;
                        case $param->STORUNG_ID == 72 && isset($param->ID_PARRENT) && $param->ID_PARRENT == 139:
                            $this->value = $this->SpeicherH2 += $param->DAUER;
                            $this->sumRow->SpeicherH2 += $param->DAUER;
                            return;
                            break;
                        case $param->STORUNG_ID == 72 && isset($param->ID_PARRENT) && $param->ID_PARRENT == 129:
                            $this->value = $this->SpeicherKTL += $param->DAUER;
                            $this->sumRow->SpeicherKTL += $param->DAUER;
                            return;
                            break;
                        case $param->STORUNG_ID == 72 && isset($param->ID_PARRENT) && $param->ID_PARRENT == 107:
                            $this->value = $this->value = $this->SpeicherFuller += $param->DAUER;
                            $this->sumRow->SpeicherFuller += $param->DAUER;
                            return;
                            break;
                        case $param->STORUNG_ID == 74 && (isset($param->ID_PARRENT) && $param->ID_PARRENT == 130 || $param->ID_PARRENT == 131):
                            $this->value = $this->TrocknerKTL += $param->DAUER;
                            $this->sumRow->TrocknerKTL += $param->DAUER;
                            return;
                            break;
                        case $param->STORUNG_ID == 74 && (isset($param->ID_PARRENT) && $param->ID_PARRENT == 148 || $param->ID_PARRENT == 149):
                            $this->value = $this->TrocknerUBS += $param->DAUER;
                            $this->sumRow->TrocknerUBS += $param->DAUER;
                            return;
                            break;
                        case $param->STORUNG_ID == 76 && isset($param->ID_PARRENT) && $param->ID_PARRENT == 140:
                            $this->value = $this->RuckführungRohSkid += $param->DAUER;
                            $this->sumRow->RuckführungRohSkid += $param->DAUER;
                            return;
                            break;
                        case $param->STORUNG_ID == 76 && (isset($param->ID_PARRENT) && $param->ID_PARRENT == 112 || $param->ID_PARRENT == 113):
                            $this->value = $this->RuckführungEHBLackskid += $param->DAUER;
                            $this->sumRow->RuckführungEHBLackskid += $param->DAUER;
                            return;
                            break;
                        case $param->STORUNG_ID == 76 && isset($param->ID_PARRENT) && $param->ID_PARRENT == 132:
                            $this->value = $this->RuckführungRohSkid += $param->DAUER;
                            $this->sumRow->RuckführungRohSkid += $param->DAUER;
                            return;
                            break;
                        case $param->STORUNG_ID == 81:
                            $this->value = $this->Nacharbeit += $param->DAUER;
                            $this->sumRow->Nacharbeit += $param->DAUER;
                            return;
                            break;
                        default :
                            break;
                    }
                }
            }
        }
        $tmp = new ErrorRow($param, $this);
        $this->schichtSum[$param->STORUNG_ID] += $param->DAUER;
        $this->errors[] = $tmp;
    }

    public function addListReportRow($param) {
        $this->errors[$param->STORUNG_ID][] = new ErrorRow($param, $this);
    }

    public function fixMissingRows($schicht) {
        $count = count($this->errors);
        if ($count >= 3)
            return;
        $num = 3 - $count;
        for ($c = 1; $c <= $num; $c++) {
            echo "<tr>";
            if ($num == 3 && $c == 1) {
                echo '<td rowspan="2" class="bgdarkblue fontwhite " >DS', $schicht->team, '</td><td colspan="23"></td>';
            } elseif (($num == 1 && $c == 1) || ($num == 2 && $c == 2) || ($num == 3 && $c == 3)) {
                echo '<td  class=" bgdarkblue fontwhite px25heightcell "><span class="inner2 rotate">';
                switch ($schicht->schicht) {
                    case 1:
                        echo "FS";
                        break;
                    case 2:
                        echo "SS";
                        break;
                    case 3:
                        echo "NS";
                        break;
                    default:
                        break;
                }
                echo '</span></td><td colspan="23"></td>';
            } else {
                echo "<td colspan='23'></td></tr>";
            }
        }
    }

    public function fixMissingRows2($schicht) {
        $co = 0;
        foreach ($this->errors as $e) {
            $co += count($e);
        }
        $count = $co;

        if ($count >= 3)
            return;
        $num = 3 - $count;
        for ($c = 1; $c <= $num; $c++) {
            echo "<tr>";
            if ($num == 3 && $c == 1) {
                echo '<td rowspan="2" class="bgdarkblue fontwhite " >DS', $schicht->team, '</td><td colspan="6" style="background-color:#fff;"></td>';
            } elseif (($num == 1 && $c == 1) || ($num == 2 && $c == 2) || ($num == 3 && $c == 3)) {
                echo '<td  class=" bgdarkblue fontwhite px25heightcell "><span class="inner2 rotate">';
                switch ($schicht->schicht) {
                    case 1:
                        echo "FS";
                        break;
                    case 2:
                        echo "SS";
                        break;
                    case 3:
                        echo "NS";
                        break;
                    default:
                        break;
                }
                echo '</span></td><td colspan="6" style="background-color:#fff;">&nbsp;</td>';
            } else {
                echo "<td colspan='6' style='background-color:#fff;'></td></tr>";
            }
        }
    }

}

class ErrorRow {

    public function __construct($param = NULL, $po = NULL) {
        if ($param != null) {
            if ($po != NULL) {
                $this->parentObj = $po;
            }
            switch ($param) {
                case $param->STORUNG_ID == 49:
                    $this->VBH = $this->value = $param->DAUER;
                    $po->sumRow->VBH += $param->DAUER;
                    break;
                case $param->STORUNG_ID == 50:
                    $this->KTL = $this->value = $param->DAUER;
                    $po->sumRow->KTL += $param->DAUER;
                    break;
                case $param->STORUNG_ID == 51:
                    $this->GAD = $this->value = $param->DAUER;
                    $po->sumRow->GAD += $param->DAUER;
                    break;
                case $param->STORUNG_ID == 52:
                    $this->UBS = $this->value = $param->DAUER;
                    $po->sumRow->UBS += $param->DAUER;
                    break;
                case $param->STORUNG_ID == 53:
                    $this->Schweller = $this->value = $param->DAUER;
                    $po->sumRow->Schweller += $param->DAUER;
                    break;
                case $param->STORUNG_ID == 54:
                    $this->SDM = $this->value = $param->DAUER;
                    $po->sumRow->SDM += $param->DAUER;
                    break;
                case $param->STORUNG_ID == 55:
                    $this->FAD = $this->value = $param->DAUER;
                    $po->sumRow->FAD += $param->DAUER;
                    break;
                case $param->STORUNG_ID == 85:
                    $this->PVCVersor = $this->value = $param->DAUER;
                    $po->sumRow->PVCVersor += $param->DAUER;
                    break;
                case $param->STORUNG_ID == 81:
                    $this->Nacharbeit = $this->value = $param->DAUER;
                    $po->sumRow->Nacharbeit += $param->DAUER;
                    break;
                case $param->STORUNG_ID == 72 && isset($param->ID_PARRENT) && $param->ID_PARRENT == 139:
                    $this->SpeicherH2 = $this->value = $param->DAUER;
                    $po->sumRow->SpeicherH2 += $param->DAUER;
                    break;
                case $param->STORUNG_ID == 72 && isset($param->ID_PARRENT) && $param->ID_PARRENT == 129:
                    $this->SpeicherKTL = $this->value = $param->DAUER;
                    $po->sumRow->SpeicherKTL += $param->DAUER;
                    break;
                case $param->STORUNG_ID == 72 && isset($param->ID_PARRENT) && $param->ID_PARRENT == 107:
                    $this->SpeicherFuller = $this->value = $param->DAUER;
                    $po->sumRow->SpeicherFuller += $param->DAUER;
                    break;
                case $param->STORUNG_ID == 74 && isset($param->ID_PARRENT) && ($param->ID_PARRENT == 130 || $param->ID_PARRENT == 131):
                    $this->TrocknerKTL = $this->value = $param->DAUER;
                    $po->sumRow->TrocknerKTL += $param->DAUER;
                    break;
                case $param->STORUNG_ID == 74 && isset($param->ID_PARRENT) && ($param->ID_PARRENT == 148 || $param->ID_PARRENT == 149):
                    $this->TrocknerUBS = $this->value = $param->DAUER;
                    $po->sumRow->TrocknerUBS += $param->DAUER;
                    break;
                case $param->STORUNG_ID == 76 && isset($param->ID_PARRENT) && $param->ID_PARRENT == 140:
                    $this->RuckführungRohSkid = $this->value = $param->DAUER;
                    $po->sumRow->RuckführungRohSkid += $param->DAUER;
                    break;
                case $param->STORUNG_ID == 76 && isset($param->ID_PARRENT) && ($param->ID_PARRENT == 112 || $param->ID_PARRENT == 113):
                    $this->RuckführungEHBLackskid = $this->value = $param->DAUER;
                    $po->sumRow->RuckführungEHBLackskid += $param->DAUER;
                    break;
                case $param->STORUNG_ID == 76 && isset($param->ID_PARRENT) && $param->ID_PARRENT == 132:
                    $this->RuckführungRohSkid = $this->value = $param->DAUER;
                    $po->sumRow->RuckführungRohSkid += $param->DAUER;
                    break;
                case $param->STORUNG_ID == 81:
                    $this->Nacharbeit = $this->value = $param->DAUER;
                    $po->sumRow->Nacharbeit += $param->DAUER;
                    break;
                default :
                    break;
            }

            $this->storungId = $param->STORUNG_ID;
            $this->type = $param->TYP;
            if (isset($param->ID_PARRENT)) {
                $this->parrentId = $param->ID_PARRENT;
            }
            $this->schicht = $param->SCHICHT;
            $this->Uhrzeit = $param->VON;
            $this->Linie = $param->LINIE;
            if ($param->U1_TEXT != NULL) {
                $this->Storungsbereich = $param->U1_TEXT;
            }
            if ($param->U2_TEXT != NULL) {
                $this->Storungsbereich .= "/" . $param->U2_TEXT;
            }
            if ($param->U3_TEXT != NULL) {
                $this->Storungsbereich .= "/" . $param->U3_TEXT;
            }
            if ($param->U4_TEXT != NULL) {
                $this->Storungsbereich .= "/" . $param->U4_TEXT;
            }
            $this->name = $param->STORUNG_TEXT;
            $this->dauer = $param->DAUER;
            $this->Bemerkung = $param->KOMENTAR;
            $this->originalError = $param;
        }
    }

    public $storungId = null;
    public $parrentId = null;
    public $parentObj = null;
    public $value = null;
    public $name = null;
    public $schicht;
    public $dauer = null;
    public $VBH = null;
    public $KTL = null;
    public $GAD = null;
    public $UBS = null;
    public $Schweller = null;
    public $SDM = null;
    public $FAD = null;
    public $SpeicherH2 = null;
    public $SpeicherKTL = null;
    public $SpeicherFuller = null;
    public $TrocknerKTL = null;
    public $TrocknerUBS = null;
    public $PVCVersor = null;
    public $RuckfuhrungRohSkid = null;
    public $RuckfuhrungEHBLackskid = null;
    public $RuckfuhrungLackskid = null;
    public $Nacharbeit = null;
    public $Sonst = null;
    public $Orga = null;
    public $Uhrzeit = null;
    public $Linie = null;
    public $Storungsbereich = null;
    public $Bemerkung = null;
    public $type = null;
    public $originalError = null;
    public $art = null;

}

class UrnReport extends ReportBase {

    public function __construct($date, $halle) {
        parent::__construct($date, $halle);
    }

    public function generateReport() {
        $ret = '';
        foreach ($this->Schicht as $schicht) {
            $ret .= $schicht->generateSchichtDiv();
        }
        return $ret;
    }

    public function generateHtmlTable() {
        $html = "<html><head>
    
        <style>
table,
table tr td,
table tr th {
	page-break-inside: avoid;
}
       
</style>
<script
  src='http://code.jquery.com/jquery-1.12.4.min.js'
  integrity='sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ='
  crossorigin='anonymous'></script>
</head><body> ";
        $return = ""; //"<table class='UrnReport' border=0>";
        // $return = $this->generateTableHeader();
        $return .= $this->generateSchichts();
        //$return .= $this->generateArts();

        $return .= "</table>";
        return $return;
    }

    public function generateTableHeader() {
        $str = "
                <thead>
                <tr style='background: #215B87;' >
                <td>
                </td>";
        foreach ($this->columns as $key => $value) {
            $str .= "<th class='fontwhite'>" . $value . "</th>
                ";
        }
        $str .= "</tr>
                </thead>
                ";
        return $str;
    }

    public function isGrayClass($param) {
        return $param->isO === TRUE;
    }
    public function isBlueClass($param) {
        return $param->isQ === TRUE;
    }

    public function grayaClass($param) {
        if ($this->isGrayClass($param)) {
            return " graya ";
        } else {
            return " grayb ";
        }
    }
    public function blueClass($param) {
        if ($this->isBlueClass($param)) {
            return " blue ";
        } else {
            return " ";
        }
    }

    public function generateSchichts() {
        //print_r($this->data);
        $s = "";
        foreach ($this->data as $value) {
            $s .= "<tr class=' ";
            $s .= $this->grayaClass($value);
            $s .= $this->blueClass($value);
			
			$s .= $this->conditionalFormatingByValue($value->error->Dauer) . " ";

            if ($value->Schicht != NULL) {
                $s .= "schicht ";
            } elseif ($value->Art != NULL) {
                $s .= "art ";
            }
            if (is_int($value->Linie)) {
                $s .= "linie ";
            }
            if ($value->ErrorTypeCnt != null) {
                $s .= " error  ";
            }
            $s .="' >
                    ";

            if ($value->Schicht != NULL) {
                $s .= "<td  class='fontwhite'  style='background: #215B87;' rowspan='" . $value->SchichtCnt . "'>" . $value->Schicht . "</td>
                    ";
            }
            if ($value->Art != NULL) {
                $s .= "<td class='fontWhite ' rowspan='" . $value->ArtCnt . "'>" . $value->Art . "</td>
                    ";
            }
            if (is_int($value->Linie)) {
                $s .= "<td class='' rowspan='" . $value->LinieCnt . "'>" . $value->Linie . "</td>
                    ";
            }
            $s .= "<td class='' ";
            if ($value->ErrorTypeCnt != null) {
                //   $s .= "rowspan='" . $value->ErrorTypeCnt . "'";
            }
            $s .= ">" . $value->error->storung . "</td>
                    ";
            $s .= "<td >" . $value->error->Uhrzeit . "</td>
                    ";
            $s .= "<td >" . $value->error->Dauer . "</td>
                    ";
            $s .= "<td class=''>" . $value->error->Modell . "</td>
                    ";
            $s .= "<td class=''>" . $value->error->Unterbereich . "</td>
                    ";
            $s .= "<td class=''>" . $value->error->Bemerkung . "</td>
                    ";
            $s .= "</tr>";
            if (isset($value->last)) {
                $s.= "<tr><td colspan='9' style='background: #215B87; '></td></tr>";
            }
        }
        //  $s .= "</tbody>";
        return $s;
    }

    public function generateSchicht($id) {
        $s = "";
        foreach (array("T", "O", "Q") as $index => $key) {
            $s .= $this->Schicht[$id]->Art[$key]->generateUrnArt($key);
        }
        $s .= "";
        return $s; //"schicht" . $id;
    }

    public function getAllRows() {
        //$this->loadDataByDate();
        //print_r($this->Schicht[1]->getSchichtRowa());
        $schicht1 = $this->Schicht[1]->getSchichtRows();
        $schicht2 = $this->Schicht[2]->getSchichtRows();
        $schicht3 = $this->Schicht[3]->getSchichtRows();
        $this->data = array_merge($this->data, $schicht1);
        $this->data = array_merge($this->data, $schicht2);
        $this->data = array_merge($this->data, $schicht3);
        // print_r($this->data);
    }

    public function conditionalFormatingByValue($param) {
        $class = "fontBlack";
        switch ($param) {
            default:
                return $class;

            case $param > 10 && $param < 50:
                return "fontOrange";

            case $param >= 50:
                return "fontRed";
        }
    }

}

class ReportBase {

    const ART_T = 24;
    const ART_O = 25;
    const ART_Q = 26;
    const TYPE_B8V = 28;
    const TYPE_A7PA = 29;
    const TYPE_A7VPA = 30;

    public static $SCHICHT = array(1 => "FS", 2 => "SS", 3 => "NS");
    public $Schicht = array();
    public $date;
    public $datum_tage_sql;
    public $halle;
    protected $columns = array();
    protected $order = "";
    protected $DataRowsNum = 0;
    protected $data = array();
    public $ReportErrors = array();

    public function __construct($date, $halle) {
        $this->date = $date;
        $this->halle = $halle;
        if ($this->date != null && $this->halle != null) {
            $this->_getReportErrors();
            $this->_createSchichts($this->date);
            $this->_getReportData();
        }


        $this->Sum = new ErrorRow();
//        $this->Schicht[1] = new UrnSchicht(1, $this);
//        $this->Schicht[2] = new UrnSchicht(2, $this);
//        $this->Schicht[3] = new UrnSchicht(3, $this);
        // $this->loadDataByDate();
        $this->columns = array("Art", "Linie", "Bereich", "Uhrzeit", "Dauer", "Modell", "Unterbereich", "Bemerkung");
    }

    public function AddError($param) {
        if (!array_key_exists($param->SCHICHT, $this->Sch)) {
            $this->Sch[$param->SCHICHT] = new Schihte($param, $this);
        }
        $this->Sch[$param->SCHICHT]->addRow($param);
    }

    protected function _loadReportData($param) {
        if (count($param) > 0) {
            foreach ($param as $value) {
                switch ($value->ART_ID) {
                    case UrnReport::ART_T:
                        $this->Schicht[intval($value->SCHICHT)]->Art["T"]->addError($value);
                        break;
                    case UrnReport::ART_O:
                        $this->Schicht[intval($value->SCHICHT)]->Art["O"]->addError($value);
                        break;
                    case UrnReport::ART_Q:
                        $this->Schicht[intval($value->SCHICHT)]->Art["Q"]->addError($value);
                        break;

                    default:
                        break;
                }
                $this->AddError($value);

                $this->Schicht[intval($value->SCHICHT)]->schichtSum[intval($value->STORUNG_ID)] += $value->DAUER;
                $this->ReportSum[$value->STORUNG_ID] += $value->DAUER;
            }
        }
    }

    private function _createSchichts($datum) {
        for ($s = 1; $s <= 3; $s++) {
            $param = new \stdClass();
            $param->DATUM = $datum;
            $param->SCHICHT = $s;

            $this->Sch[$s] = new UrnSchicht($param, $this);
            $this->Schicht[$s] = new UrnSchicht($param, $this);
        }
    }

    private function _getReportData() {
        $order = 'SCHICHT asc';
        $this->datum_tage_sql = "TO_DATE('" . $this->date . "','DD.MM.YYYY')";
        $today = "(DATUM = " . $this->datum_tage_sql . " AND TO_TIMESTAMP(SCHICHT_BEGIN,'HH24:MI') BETWEEN TO_TIMESTAMP('06:00','HH24:MI') AND TO_TIMESTAMP('22:00','HH24:MI'))";
        $where = "WHERE " . $today . " AND HALLE='" . $this->halle . "'";
        $alle_storungen = App::get('database')->SelectWhere2('VIEW_ERRORS', ['COLUMNS' => '*'], $where, $order, 'Errors');
        $this->_loadReportData($alle_storungen);
    }

    public function loadDataByDate() {
        $this->datum_tage_sql = "TO_DATE('" . $this->date . "','DD.MM.YYYY')";
        // $this->datum_tage_sql = "" . $this->date . "";
        $today = "(DATUM = " . $this->datum_tage_sql . " AND TO_TIMESTAMP(SCHICHT_BEGIN,'HH24:MI') BETWEEN TO_TIMESTAMP('06:00','HH24:MI') AND TO_TIMESTAMP('22:00','HH24:MI'))";
        $where = "WHERE " . $today . " AND HALLE='" . $this->halle . "'";
        $alle_storungen = App::get('database')->SelectWhere2('VIEW_ERRORS', ['COLUMNS' => '*'], $where, $this->order, 'Errors');

        foreach ($alle_storungen as $value) {
            switch ($value->ART_ID) {
                case UrnReport::ART_T:
                    $this->Schicht[intval($value->SCHICHT)]->Art["T"]->addError($value);
                    break;
                case UrnReport::ART_O:
                    $this->Schicht[intval($value->SCHICHT)]->Art["O"]->addError($value);
                    break;
                case UrnReport::ART_Q:
                    $this->Schicht[intval($value->SCHICHT)]->Art["Q"]->addError($value);
                    break;

                default:
                    break;
            }
        }
    }

    private function _getReportErrors() {
        $order = '';
        switch ($this->halle) {
            case 3:
                $where = " where H3_A = '1'";


                break;
            case 4:
                $where = " where H4_A = '1'";


                break;

            default:
                $where = " where H5_A = '1'";
                break;
        }
        $tmp = App::get('database')->SelectWhere2('STR_STORUNG', ['COLUMNS' => '*'], $where, $order, 'Storungen');

        $errors = array();
        $sum = array();
        foreach ($tmp as $value) {
            $errors[$value->ID] = $value->TEXT;
            $sum[$value->ID] = 0;
        }
        $this->ReportErrors = $errors;
        $this->ReportSum = $sum;
    }

    public function isGrayClass($param) {
        return $param->isO === TRUE;
    }

    public function grayaClass($param) {
        if ($this->isGrayClass($param)) {
            return " graya ";
        } else {
            return " grayb ";
        }
    }

    public function conditionalFormatingByValue($param) {
        $class = "fontBlack";
        switch ($param) {
            default:
                return $class;

            case $param > 10 && $param < 50:
                return "fontOrange";

            case $param >= 50:
                return "fontRed";
        }
    }

}

class ComplexReportDynamic extends ReportBase {

    public function __construct($date, $halle) {
        parent::__construct($date, $halle);
    }

    public function generateHtmlTable() {
        $table = '<style>
            '
                . ''
                . 'td {
    border: 1px black solid;
    padding: 5px;
}
.rotateo {
  text-align: center;
  white-space: nowrap;
  vertical-align: middle;
  
  font-weight: normal;
  color: #FFF;
}
.header{
  height: 150px;
}
.rotateo div {
  -webkit-transform: rotate(-90deg); 
     -moz-transform: rotate(-90deg); 
      -ms-transform: rotate(-90deg); 
       -o-transform: rotate(-90deg); 
          transform: rotate(-90deg);

    float: left;
    vertical-align:top;
    position: relative;
    top: 60px;
    height:20px;
    width:20px;
}
.sname div {
  -webkit-transform: rotate(-90deg); 
     -moz-transform: rotate(-90deg); 
      -ms-transform: rotate(-90deg); 
       -o-transform: rotate(-90deg); 
          transform: rotate(-90deg);

    float: left;
    vertical-align:bottom;
    position: relative;
    color: #FFF;
    height:20px;
    width:20px;
}
'
                
                . '</style>'
                . '<table border=1>';
        $table .= $this->generateHtmlTableHeader();
        $table .= $this->generateHtmlTableBody();
        $table .= '</table>'
                . '<script>'
                . ' $(".fr").each(function(index) {
                    var $ob = $($(this).children());
                    $ob.each(function(ind, val){
                        $v = $(val);
                        $($v.children()[0]).css("width", "20px");
                    });
});'
                . '</script>';

        return $table;
    }

    public function generateHtmlTableBody() {
        $tbody = "<tbody>";
        foreach ($this->Schicht as $s) {
            $tbody .= $this->generateSchicht($s);
            $e = $s->getSchichtRowsByArt();
            $cnt = 0;
            foreach ($e as $value) {
                $cnt += count($value);
            }
            $tbody .= $s->fixMissingRows($s, $cnt, $this->ReportErrors);
            $tbody .= $this->generateSchichtSum($s->schichtSum);
        }
        $tbody .= $this->generateSchichtSum($this->ReportSum, "bgdarkgray fontwhite");
        $tbody .= "</tbody>";
        return $tbody;
    }

    public function generateHtmlTableHeader() {
        $thead = "<thead><tr class = 'fr fontRed' style='background: #215B87; fullborder'>"
                . "<th class='rotateo header'><div>Schicht</div></th>"
                . "<th class='rotateo header'><div>Art</div></th>";
        foreach ($this->ReportErrors as $key => $value) {
            $thead .= "<th class='rotateo header'><div>" . $value . "</div></th>";
        }
        $thead .= "<th class='rotateo header'><div>Linie</div></th>"
                . "<th class='rotateo header'><div>Uhrzeit</div></th>"
                . "<th class='rotateo header'><div>Modell</div></th><th class = 'fr fontwhite' style='background: #215B87; fullborder'>Bemerkung</th><th class = 'fr fontwhite' style='background: #215B87; fullborder'>St�rungsbereich</th></tr></thead>";
        return $thead;
    }

    public function generateSchichtRow($r, $s, $index) {

        //$rows = '<tr style="background:white;">';
        $rows = ($index == 0) ? '<td rowspan="' . $s->getRowsNum . '"></td>' : "";
        $rows .= '<td>' . $r->originalError->ART . '</td>';

        foreach ($this->ReportErrors as $key => $value) {
            $rows .= "<td>";
            if ($r->storungId == $key) {
                $rows .= $r->dauer;
            }
            $rows .= "</td>";
        }

        $rows .= '<td>' . $r->Linie . '</td>'
                . '<td>' . $r->Uhrzeit . '</td>'
                . '<td>' . $r->type . '</td>'
                . '<td>' . $r->Bemerkung . '</td>'
                . '<td>' . $r->Storungsbereich . '</td>';
        return $rows;
    }

    public function generateSchichtSum($s, $class = "gray") {

        $sr = 2;
        if ($class == "gray") {
            $sr = 2;
        }
        $rows = '<tr class="' . $class . '" >'
                . '<td colspan="' . $sr . '">';
        if ($class == "gray") {
            $rows .= "Sum";
        } else {
            $rows .= "Total";
        }
        $rows .= '</td>';
        foreach ($s as $value) {
            $rows .= "<td>";
            if ($value != null && $value != 0) {
                $rows .= $value;
            }
            $rows .= "</td>";
        }
        $rows .= '<td></td>'
                . '<td></td>'
                . '<td></td>'
                . '<td></td>';
        if ($class != "gray") {
            
        }
        $rows .= '<td></td>';

        $rows .= '</tr>';
        return $rows;
    }

    public function generateSchicht($s) {
        $schicht = $s->getSchichtRowsByArt();
		$rowspan = 0;		
		foreach ($schicht as $v) {
			$rowspan += count($v);
		}
		$rowspan --;

        $str = "";
		$sAdded = false;
		$nAdded = false;	
            
			$counter = 0;
        foreach ($schicht as $art => $errors) {	
            foreach ($errors as $key => $value) {
                $str .= "<tr class='" . $this->conditionalFormatingByValue($value->error->Dauer) . $this->grayaClass($value) . "'>";

				if (!$sAdded) {
					$str .= "<td style='background: #215B87; font-weight: bold;' class='fontwhite'>DS" . $s->Team . "</td>";
					$sAdded = true;
				} 

				if (!$nAdded && $counter > 0) {
					$str .= "<td rowspan='" . ($rowspan ) . "'  class='fontwhite ' style='background: #215B87;' class='fontwhite'>" . $s->Name . "</td>";
					$nAdded = true;
				}
				$counter++;
				if($key == 0){
					$str .= "<td rowspan='" .count($errors) . "' style='color:#000; font-weight: bold;'>" . $art . "</td>";
				}

                foreach ($this->ReportErrors as $key1 => $val) {
                    $str .= "<td>";
                    if ($value->error->storungId == $key1) {
                        $str .= $value->error->Dauer;
                    }
                    $str .= "</td>";
                }
                $str .= '<td>' . $value->error->Linie . '</td>'
                        . '<td>' . $value->error->Uhrzeit . '</td>'
                        . '<td>' . $value->ErrorType . '</td>'
                        . '<td>' . $value->error->Bereich . '</td>'
                        . '<td>' . $value->error->Bemerkung . '</td>';
                $str .= "</tr>";

            }
        }

        return $str;
    }

}

class UrnReportRow {

    public $Schicht;
    public $SchichtCnt;
    public $Art;
    public $ArtCnt;
    public $Linie;
    public $LinieCnt;
    public $ErrorType;
    public $ErrorTypeCnt;
    public $type;
    public $error;
    public $isO = false;
    public $isQ = false;
    public $isArtTr = false;
    public $isLinieTr = false;
    public $isErrorTr = false;

}

class UrnSchicht {

    public $Art = array();
    public $Id;
    public $Name;
    public $Team;
    protected $DataRowsNum = 0;
    public $report;
    public $Sum;
    public $sumRow;
    public $schichtSum = array();

    public function __construct($id, $parent) {
        $this->report = $parent;
        $this->Id = $id->SCHICHT;
        $this->getData();
        $this->Art["T"] = new UrnArt("T", $this);
        $this->Art["O"] = new UrnArt("O", $this);
        $this->Art["Q"] = new UrnArt("Q", $this);


        foreach ($this->report->ReportErrors as $key => $value) {
            $this->schichtSum[$key] = 0;
        }
        $this->sumRow = new ErrorRow();

        $this->Sum = new ErrorRow();
        $weekNum = intval(date("W", strtotime($id->DATUM)));
        $year = date("Y", strtotime($id->DATUM));



        $where = " WHERE trunc(STR_DATE) = (Select TRUNC (Trunc(to_date ('$year','YYYY'),'yyyy')+($weekNum-1)*7,'IW') from dual)";
        $Cal = App::get('database')->SelectWhere2('STR_CALENDAR', ['COLUMNS' => "*"], $where, "", 'StrCalendar');

        if ($Cal != NULL && isset($Cal[0])) {
            $sc = explode("|", $Cal[0]->TEAM);
            $this->Team = $sc[$id->SCHICHT - 1];
        }
        //$this->schicht = $id->SCHICHT;
    }

    public function addRow($param) {
        $tmp = new ErrorRow($param, $this);
        $this->errors[] = $tmp;
    }

    public function getData() {
        $this->Name = UrnReport::$SCHICHT[$this->Id];
    }

    public function getRowsNum() {
        $this->DataRowsNum += $this->Art["T"]->countArtRows();
        $this->DataRowsNum += $this->Art["O"]->countArtRows();
        $this->DataRowsNum += $this->Art["Q"]->countArtRows();
        return $this->DataRowsNum;
    }

    public function getSchichtRows() {
        $array = array();
        $art = array();
        foreach (array("T", "O", "Q") as $key) {
            $schicht = $this->Art[$key]->getArtRows();
            $art[$key] = $this->Art[$key]->getArtRows();
            $array = array_merge($array, $schicht);
        }

        if (count($array) > 0) {
            $array[count($array) - 1]->last = 1;
            $array[0]->Schicht = $this->Name;
            $array[0]->SchichtCnt = count($array);
        }

        return $array;
    }

    public function getSchichtRowsByArt() {
        $array = array();
        $art = array();
        foreach (array("T", "O", "Q") as $key) {
            $schicht = $this->Art[$key]->getArtRows();
            $art[$key] = $this->Art[$key]->getArtRows();
            $array = array_merge($array, $schicht);
        }

        //print_r($array);
        if (count($array) > 0) {
            $array[count($array) - 1]->last = 1;
            $array[0]->Schicht = $this->Name;
            $array[0]->SchichtCnt = count($array);
        }

        return $art;
    }

    public function fixMissingRows($s, $num, $err) {
        if ($num >= 2)
            return;
        $num = 2 - $num;
        $str = "";
        for ($c = 1; $c <= $num; $c++) {
            $str .= "<tr >";
            if ($num == 2 && $c == 1) {
                $str = '<td  class="bgdarkblue fontwhite " >DS' . $s->Team . '</td><td  style="background-color:#fff;"></td>';
            } elseif (
                    ($num == 1 && $c == 1) ||
                    ($num == 2 && $c == 2) ||
                    ($num == 3 && $c == 3)
            ) {
                $str .= '<td  class="  " style="background: #215B87; color: #FFF;">';
                switch ($s->Id) {
                    case 1:
                        $str .= "FS";
                        break;
                    case 2:
                        $str .= "SS";
                        break;
                    case 3:
                        $str .= "NS";
                        break;
                    default:
                        break;
                }
                $str .= '</td><td></td>';
            }

            foreach ($err as $value) {
                $str .= "<td></td>";
            }
            $str .= "<td></td><td></td><td></td><td></td><td></td>";
            $str .= "</tr>";
        }
        return $str;
    }

}

class UrnArt {

    public $Name;
    public $Schicht;
    public $Linie = array();

    public function __construct($name, $parent) {
        $this->Schicht = $parent;
        $this->Name = $name;
        $this->Linie[0] = new UrnLinie($this);
        $this->Linie[1] = new UrnLinie($this);
        $this->Linie[2] = new UrnLinie($this);
    }

    public function countErrors() {
        return $this->Linie[0]->ErrorsNumber() +
                $this->Linie[1]->ErrorsNumber() +
                $this->Linie[2]->ErrorsNumber();
    }

    public function addAldError($error) {
        switch ($error->LINIE) {
            case 0:
                $this->Linie[0]->addAldError($error);
                break;

            case 1:
                $this->Linie[1]->addAldError($error);
                break;

            case 2:
                $this->Linie[2]->addAldError($error);
                break;

            default:
                break;
        }
    }

    public function addError($error) {
        switch ($error->LINIE) {
            case 0:
                $this->Linie[0]->addError($error);
                break;

            case 1:
                $this->Linie[1]->addError($error);
                break;

            case 2:
                $this->Linie[2]->addError($error);
                break;

            default:
                break;
        }
    }

    public function generateUrnArt($key) {
        $urnArt = "<tr></td><td></td><td></td><td></td><td></td><td></td>";
        $urnArt .= "<td rowspan='" . $this->countArtRows() . "'>" . $key . "</td>";
        $urnArt .= $this->Linie[0]->generateUrnLinie();
        $urnArt .= $this->Linie[1]->generateUrnLinie(FALSE);
        $urnArt .= $this->Linie[2]->generateUrnLinie(FALSE);
        $urnArt .= "";
        return $urnArt;
    }

    public function countArtRows() {
        $errorsNum = 0;
        $errorsNum += count($this->Linie[0]->Errors);
        $errorsNum += count($this->Linie[1]->Errors);
        $errorsNum += count($this->Linie[2]->Errors);
        return $errorsNum;
    }

    public function getArtRows() {
        $array = array();
        foreach (array(0, 1, 2) as $lin) {
            $array = array_merge($array, $this->Linie[$lin]->getLinieRows($lin));
        }
        if (count($array) > 0) {
            $array[0]->Art = $this->Name;

            $array[0]->ArtCnt = count($array);
        }

        return $array;
    }

    public function GenerateArtDiv() {
        $return = '<div class="ArtDiv">'
                . '<div class = "Art artLabel">' . $this->Name . '</div>'
                . ''
                . '<div class = "artContent">' . $this->GenerateLinies() . '</div>'
                . '</div>'
                . '';

        return $return;
    }

    public function GenerateLinies() {
        $return = '';
        foreach ($this->Linie as $key => $value) {
            $return .= $value->GenerateLinieDiv($key);
        }
        return $return;
    }

}

class UrnLinie {

    public $Errors = array();
    public $Art;

    public function __construct($parent) {
        $this->Art = $parent;
    }

    public function hasErrors() {
        return $this->ErrorsNumber() > 0;
    }

    public function ErrorsNumber() {
        return count($this->Errors);
    }

    public function addError($error) {
        if (!isset($this->Errors[$error->STORUNG_ID])) {
            $this->Errors[$error->STORUNG_ID] = new URNErrorNode($error);
        }
        $this->Errors[$error->STORUNG_ID]->Error[] = new UrnError($error, $this);
    }

    public function addAldError($error) {
        $this->Errors[$error->STORUNG_ID][] = $error;
    }

    public function getLinieRowspan() {
        $cnt = 0;
        foreach ($this->Errors as $key => $val) {
            $cnt += count($val->Error);
        }
        return $cnt;
    }

    public function getLinieRows($lin) {
        $array = array();

        foreach ($this->Errors as $key => $value) {
            $nodeRows = $value->getNodeRows($key);
            $array = array_merge($array, $nodeRows);
        }
        if (count($array) > 0) {
            $array[0]->Linie = $lin;
            $array[0]->LinieCnt = $this->getLinieRowspan();
        }
        return $array;
    }

    public function GenerateLinieDiv($lid) {
        $return = '';
        foreach ($this->Errors as $key => $value) {
            $return .= '<div class="LinieDiv">';
            $return .= $this->generateLinieLabel($lid);
            $return .= $this->generateLinieContent($key, $value);
            $return .= '</div>';
        }
        return $return;
    }

    public function generateLinieLabel($key) {
        return '<div class = "Linie">' . $key . '</div>';
    }

    public function generateLinieContent($key, $value) {
        $return = '<div class = "LinieContent">';

        $return .= '<div class="Bereich">' . $value[0]->STORUNG_TEXT . '</div>';
        foreach ($value as $error) {

            $return .= '<div class="ErrorContent">';
            $return .= '<div class="Uhrzeit">' . $error->Uhrzeit . "</div>";
            $return .= '<div class="Dauer">' . $error->Dauer . "</div>";
            $return .= '<div class="Modell">' . $error->Modell . "</div>";
            $return .= '<div class="Unterbereich">' . $error->Unterbereich . "</div>";
            $return .= '<div class="Bemerkung">' . $error->Bemerkung . "</div>";

            $return .= '</div>';
        }
        $return .= '</div>';
        return $return;
    }

}

class URNErrorNode {

    public $Error = array();
    public $text;

    public function __construct($error) {
        $this->text = $error->STORUNG_TEXT;
    }

    public function getNodeRows($k) {
        $rows = array();
        foreach ($this->Error as $key => $value) {
            $row = new UrnReportRow();
            if ($key == 0) {
                $row->ErrorType = $value->Modell;
                $row->ErrorTypeCnt = count($value->linie->Errors);
            }
            $row->error = $value;
            $row->isO = $value->linie->Art->Name == "O";
            $row->isQ = $value->linie->Art->Name == "Q";
            $rows[] = $row;
        }
        return $rows;
    }

}

class UrnError {

    public $Art;
    public $Linie;
    public $Bereich;
    public $Uhrzeit;
    public $Dauer;
    public $Modell;
    public $Unterbereich;
    public $Bemerkung;
    public $storung;
    public $storungId;
    public $linie;

    public function __construct($error, $parent) {
        $this->linie = $parent;
        $this->Art = $error->ART;
        $this->Linie = $error->LINIE;
        $this->Bereich = $error->STORUNG_TEXT;
        $this->Uhrzeit = $error->VON;
        $this->Dauer = $error->DAUER;
        $this->Modell = $error->TYP;
        $this->storung = $error->STORUNG_TEXT;
        $this->storungId = $error->STORUNG_ID;
        $Storungsbereich = "";
        if ($error->U1_TEXT != NULL) {
            $Storungsbereich = $error->U1_TEXT;
        }
        if ($error->U2_TEXT != NULL) {
            $Storungsbereich .= "/" . $error->U2_TEXT;
        }
        if ($error->U3_TEXT != NULL) {
            $Storungsbereich .= "/" . $error->U3_TEXT;
        }
        if ($error->U4_TEXT != NULL) {
            $Storungsbereich .= "/" . $error->U4_TEXT;
        }
        $this->Bemerkung = $error->KOMENTAR;
        if ($this->Bemerkung == "Jimmy") {
            echo "";
        }
        $this->Unterbereich = $Storungsbereich;
    }

}

class QualitatsschwerpunkteReport {

    public $data;
    public $date;
    public $halle;

    public function __construct($date, $halle) {
        $this->date = $date;
        $this->halle = $halle;
        $this->loadDataByDate();
        //$this->columns = array("Art", "Linie", "Bereich", "Uhrzeit", "Dauer", "Modell", "Unterbereich", "Bemerkung");
    }

    public function loadDataByDate() {
        $this->order = "";
        $this->datum_tage_sql = "TO_DATE('" . $this->date . "','DD.MM.YYYY')";
//Boris:        $today = "DATUM = " . $this->datum_tage_sql . " ";
        $today = "to_char (datum, 'DD.MM.YYYY') = '" . $this->date . "' ";
        $where = "WHERE " . $today . " AND HALLE='" . $this->halle . "'";

        //  $where = "WHERE " . $today . " AND HALLE='" . $this->halle . "'";
        $this->data = App::get('database')->SelectWhere2('STR_QUALITAETSSCHEWERPUNKTE', ['COLUMNS' => '*'], $where, $this->order, 'Qualitatesschewerpunkte');
    }

    public function generateHtmlTable() {
        $s = "<table border=1 width='100%'     >"
                . "<tr>"
                . "<th style='background: gray;' class='fontwhite'>Qualit�tsschwerpunkte</th>"
                . "</tr>"
                . "<tr>"
                . "<th style='background: #215B87; text-align: center;' class='fontwhite'>Bemerkung</th>"
                . "</tr>";
        foreach ($this->data as $value) {
            $s .= "<tr><td>" . $value->BEMERKUNG . "</td></tr>";
        }
        $s .= "</table>";
        return $s;
    }

}

class RueckweisungReport {

    public $data;
    public $date;
    public $halle;

    public function __construct($date, $halle) {
        $this->date = $date;
        $this->halle = $halle;
        $this->loadDataByDate();
        //$this->columns = array("Art", "Linie", "Bereich", "Uhrzeit", "Dauer", "Modell", "Unterbereich", "Bemerkung");
    }

    public function loadDataByDate() {
        $this->order = "";
        $this->datum_tage_sql = "TO_DATE('" . $this->date . "','DD.MM.YYYY')";
        $today = "DATUM = " . $this->datum_tage_sql . " ";
        $where = "WHERE " . $today;
        $this->data = App::get('database')->SelectWhere2('STR_RUECKWEISUNG', ['COLUMNS' => '*'], $where, $this->order, 'Qualitatesschewerpunkte');
    }

    public function generateHtmlTable() {
        $s = "<table border=1 width='100%'     >"
                . "<tr>"
                . "<th  colspan='4' style='background: gray;' class='fontwhite'>R�ckweisung Rohbau</th>"
                . "</tr>"
                . "<tr>"
                . "<th style='background: #215B87; text-align: center;' class='fontwhite'>Schicht</th>"
                . "<th style='background: #215B87; text-align: center;' class='fontwhite'>Type</th>"
                . "<th style='background: #215B87; text-align: center;' class='fontwhite'>Grund</th>"
                . "<th style='background: #215B87; text-align: center;' class='fontwhite'>Bemerkung</th>"
                . "</tr>";
        foreach ($this->data as $key => $value) {
            $s .="<tr>";
            $s .= "<td>" . $value->SCHICHT . "</td>";
            $s .= "<td>" . $value->TYPE . "</td>";
            $s .= "<td>" . $value->GRUND . "</td>";
            $s .= "<td>" . $value->BEMERKUNG . "</td>";

            $s .="</tr>";
        }
        $s .= "</table>";
        return $s;
    }

}

?>