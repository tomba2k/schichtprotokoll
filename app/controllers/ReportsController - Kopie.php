<?php

namespace App\Controllers;

use App\Core\App;
use DateTime;

class ReportsController{

	public function __construct(){
		$this->halle_id			= 3;
		$this->datum_tage		= date('d.m.Y H:i');
		$this->phpdate			= new DateTime($this->datum_tage);

		$this->datum_tage_sql	= "TO_DATE('".date('d.m.Y')."','DD.MM.YYYY')";
		//$this->datum_tage_sql	= "TO_DATE('".date('Y-m-d')."','YYYY-MM-DD')";

		if(isset($_POST['halle']) || isset($_POST['datum_tage'])){
			
			$this->halle_id			= $_POST['halle'];
			$this->datum_tage		= $_POST['datum_tage'];
			$this->phpdate			= new DateTime($this->datum_tage);

			$this->datum_tage_sql	= strtotime($this->datum_tage);
			$this->datum_tage_sql	= "TO_DATE('".date('d.m.Y',$this->datum_tage_sql)."','DD.MM.YYYY')";
			//$this->datum_tage_sql	= "TO_DATE('".date('Y-m-d',$this->datum_tage_sql)."','YYYY-MM-DD')";
		}

	}

	private function fixDateAndHalle($array)
	{
		foreach ($array as $alle_storunge) {
			 if($alle_storunge->HALLE == '5'){$alle_storunge->HALLE = '4A';}

			$my_date = $alle_storunge->DATUM;
			if (strpos($my_date, '-') !== false) {
    			$year = substr($my_date, 0,4);
    			$month = substr($my_date, 5,2);
    			$day = substr($my_date, 8,2);
    			$corected_date = $day.'.'.$month.'.'.$year;
    			$alle_storunge->DATUM = $corected_date;
			}
		}
		return $array;
	}

	public function alle_storungen(){
		$alle_storungen = App::get('database')->SelectWhere('VIEW_ERRORS',
																   ['COLUMNS'	=> '*'],
																   '',
																   '',
																   'Schichten');
		$alle_storungen = $this->fixDateAndHalle($alle_storungen);

		return view('reports.alle_storungen',['alle_storungen' => $alle_storungen]);
	}

	public function schichtprotokoll(){

		$arts = ['T','O','Q'];

		$today = "(DATUM = ".$this->datum_tage_sql." AND TO_TIMESTAMP(VON,'HH24:MI')>=TO_TIMESTAMP('06:00','HH24:MI'))";
		$tomorrow = "(DATUM = ".$this->datum_tage_sql."+1 AND TO_TIMESTAMP(BIS,'HH24:MI')<=TO_TIMESTAMP('05:59','HH24:MI'))";
		
		foreach ($arts as $key => $value) {
			$where = "WHERE ".$today. " AND ART='".$value."' AND HALLE='".$this->halle_id."'";

			$alle_storungen[$key] = App::get('database')->SelectWhere2('VIEW_ERRORS',
																   ['COLUMNS'	=> '*'],
																   $where,
																   '',
																   'Errors');
		
			
			$where = "WHERE ".$tomorrow. " AND ART='".$value."' AND HALLE='".$this->halle_id."'";
			$temps = App::get('database')->SelectWhere2('VIEW_ERRORS',
													   ['COLUMNS'	=> '*'],
													   $where,
													   '',
													   'Errors');
			foreach ($temps as $temp) {
				if (!empty($temp)){
					$alle_storungen[$key][] = $temp;
				}
			}
		}
		
		

		return view('reports.schichtprotokoll',['halle_id'			=> $this->halle_id,
												'datum_tage'		=> $this->datum_tage,
												'alle_storungen'	=> $alle_storungen
		]);
	}
}