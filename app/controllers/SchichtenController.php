<?php

namespace App\Controllers;

use App\Core\App;
use DateTime;

class SchichtenController{

	protected $schicht;
	protected $team;
	protected $date;
	protected $week;
	protected $phpdate;
	protected $schicht_exists;

	public function __construct(){
		$this->date ='';
		if(isset($_POST['jahre_woche']) && $_POST['jahre_woche'] != ''){
			$this->jahre_woche = $_POST['jahre_woche'];
		}
		else{
			$this->jahre_woche = 'w';
		}

		if(isset($_POST['datum_schichten']) && $_POST['datum_schichten'] != ''){
			if ($this->jahre_woche == 'w'){
				$this->date = strtotime($_POST['datum_schichten']);
			}
			else{
				$this->date = strtotime("01.02.".$_POST['datum_schichten']);
			}
			
			for ($i=1; $i <=3 ; $i++){
				$this->schicht[] = $i;
				$this->team[] = $_POST['team_'.$i];
			}
			
			$this->date = date('d.m.Y',$this->date);
			//$this->date = date('Y-m-d',$this->date);
			$this->phpdate = new DateTime($this->date);
			$this->year = $this->phpdate->format('Y');
			$this->week = $this->phpdate->format('W');
		}
	}

	// rotation of array - put first element to end
	private function array_rotate($arr){
  		$elm = array_shift($arr);
  		array_push($arr, $elm);
  		return $arr;
	}
	
	// get number of weeks in selected year
	private function getIsoWeeksInYear($year) {
   		$date = new DateTime();
    	$date->setISODate($year, 53);
    	//return $date;
    	return ($date->format("W") == "53" ? 53 : 52);
	}

	// get Monday date for selected week
	private function getStartDate($week, $year) {
		$dto = new DateTime();
		$dto->setISODate($year, $week);
		return $ret = $dto->format('d.m.Y');
	}

	// check if entry for selected day exists in database
	private function check_row($date, $schicht_exists){
		if ($date != ''){
				$datum_tage_sql = strtotime($date);
				$datum_tage_sql = "TO_DATE('".date('d.m.Y',$datum_tage_sql)."','DD.MM.YYYY')";
				//$datum_tage_sql = "TO_DATE('".date('Y-m-d',$datum_tage_sql)."','YYYY-MM-DD')";
				
				$schicht_exists = App::get('database')->SelectWhere('STR_CALENDAR',
													['COLUMNS' => '*'],
													['STR_DATE' => $datum_tage_sql],
													'',
													'Container');
				return $count = count($schicht_exists);
			}
			else{
				return $count = 0;
			}
	}

	// insert row to database
	private function insert_row ($date, $week, $team, $schicht){
		$date = "to_date('".$date."','YYYY-MM-DD')";
		$team_str= "'".implode('|', $team)."'";
		$schicht_str= "'".implode('|', $schicht)."'";

		App::get('database') -> insert('STR_CALENDAR', [
									   'STR_DATE'	=> $date,
									   'TEAM'		=> $team_str,
									   'SCHICHT'	=> $schicht_str,
									   'WEEK'		=> $week
								]);
	}

	public function schichten(){
		$schichten = App::get('database')->SelectAll('STR_CALENDAR', 'Schichten');
		foreach ($schichten as $schichte) {
			//$schichte->STR_DATE = date('d.m.Y',strtotime($schichte->STR_DATE));
		}

		return view('schichten',['schichten' => $schichten]);
	}

	public function store(){
		if ($this->jahre_woche == 'w'){
			$count = $this->check_row($this->date, $this->schicht_exists);
			if($count == 0){ $this->insert_row ($this->date, $this->week, $this->team, $this->schicht);	}
		}

		if ($this->jahre_woche == 'j'){
			$jahre_weeks = $this->getIsoWeeksInYear($this->year);
			//die(var_dump($jahre_weeks));
			for ($i=1; $i<=$jahre_weeks; $i++){
				
				$jahre_date = $this->getStartDate($i, $this->year);
				$jahre_date = strtotime($jahre_date);
				$jahre_date = date('Y-m-d',$jahre_date);
				$this->insert_row ($jahre_date, $i, $this->team, $this->schicht);
				$this->team = $this->array_rotate($this->team);
				//die(var_dump($this->team));

			} 
		}

		return redirect ('schichten');
	}

	public function delete(){

		$id = $_POST['schichten_id'];
		
		App::get('database') -> delete('STR_CALENDAR', [
			'ID'	=> $id
			]);
		
		return redirect ('schichten');	
	}
}

