<?php

namespace App\Controllers;

class PagesController
{
	
	public function home(){
		return view('index');
	}

	public function admin(){
		return view('admin');
	}

	public function pdf()
	{
		return view('pdf');
	}

}