<?php

namespace App\Controllers;

use App\Core\App;

class ArtController{

	public function __construct(){
	}

	public function art(){
		$art = App::get('database')->SelectWhere('STR_TYPART',['COLUMNS' => 'ID, TEXT, ACTIVE'],['TYPE' => "'A'"],'','Art');

		return view ('art',['arts' => $art]);
	}

	public function store(){

		if(isset($_POST['art'])){
			$art = $_POST['art'];
			$active = $_POST['active'];

			if ($art != '') {
				App::get('database') -> insert('STR_TYPART', [
					'TEXT'	=> "'".$art."'",
					'TYPE'	=> "'A'",
					'ACTIVE'=> "'".$active."'"
				]);
			}
		}

		return redirect ('art');
	}

	public function edit(){

		$id = $_POST['modal_id'];
		$art = $_POST['modal_art'];
		$active = $_POST['modal_active'];

		App::get('database') -> update('STR_TYPART', [
			'TEXT'	=> "'".$art."'",
			'ACTIVE'	=> "'".$active."'" ],[
			'ID'		=> $id
			]);
		
		return redirect ('art');	
	}
}

