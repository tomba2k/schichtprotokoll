<?php

namespace App\Controllers;

use App\Core\App;

class TypController{

	public function __construct(){
	}

	public function typ(){
		$typ = App::get('database')->SelectWhere('STR_TYPART',['COLUMNS' => 'ID, TEXT, ACTIVE'],['TYPE' => "'T'"],'','Typ');

		return view ('typ',['typs' => $typ]);
	}

	public function store(){

		if(isset($_POST['typ'])){
			$typ = $_POST['typ'];
			$active = $_POST['active'];

			if ($typ != '') {
				App::get('database') -> insert('STR_TYPART', [
					'TEXT'	=> "'".$typ."'",
					'TYPE'	=> "'T'",
					'ACTIVE'=> "'".$active."'"
				]);
			}
		}

		return redirect ('typ');
	}

	public function edit(){

		$id = $_POST['modal_id'];
		$typ = $_POST['modal_typ'];
		$active = $_POST['modal_active'];

		App::get('database') -> update('STR_TYPART', [
			'TEXT'	=> "'".$typ."'",
			'ACTIVE'	=> "'".$active."'" ],[
			'ID'		=> $id
			]);
		
		return redirect ('typ');	
	}
}

