<?php

namespace App\Controllers;

use App\Core\App;

class StorungenController{
	public function __construct(){
		$this->tab ='0';

		if(isset($_POST['tab']) && $_POST['tab'] != ''){
			$this->tab = $_POST['tab'];
		}
		if(isset($_POST['new_unterkat'])){
			foreach ($_POST['new_unterkat'] as $key => $value) {
				if (isset($value)){
					$this->tab		= $key;
					$this->storung	= $value;
				}
			}
		}
	}

	private function get_data(){
		$this->storungen = App::get('database')->SelectAll('STR_STORUNG', 'Storungen', 'NR ASC');
		$this->unterkategorien = App::get('database')->SelectAll('STR_UNTERKATEGORIE', 'Unterkategorien', 'TEXT ASC');
	}

	public function storungen(){
		$this->get_data();
		return view('storungen',['tab'				=> $this->tab,
								 'storungen'		=> $this->storungen,
								 'unterkategorien'	=> $this->unterkategorien
								]);
	}

	public function new_storung(){
		$new_storung = $_POST['new_storung'];
		$new_storung_nr = $_POST['new_storung_nr'];
		
		App::get('database') -> insert('STR_STORUNG', [
										'NR'		=> "'".$new_storung_nr."'",
										'TEXT'		=> "'".$new_storung."'",
										'ACTIVE'	=> 0
										]);

		$this->storungen();
	}

	public function new_unterkategorie(){
		App::get('database') -> insert('STR_UNTERKATEGORIE', [
										'UNTERKATEGORIE_NR'	=> "'".$this->tab."'",
										'TEXT'				=> "'".$this->storung."'",
										'ACTIVE'			=> 0
										]);

		$this->storungen();
	}

	public function active(){
		$table = $_POST['table'];
		$id = $_POST['id'];
		$column = $_POST['column'];
		$active = $_POST['active'];
		App::get('database')->update($table, [$column => "'".$active."'"], ['ID' => $id]);
	}


}