<?php require ('app/views/partials/head.php'); ?>

<div class='row nonprintable'>
	<div class='container'>
	    <h3>Schichtprotokoll</h3>
	</div>
	<form method='POST' role='form' id='form_tagesansicht' name='form_tagesansicht' action='reports.schichtprotokoll'>
	<div class='row'>
		<div class='container'>
		    <div class='col-xs-3'>
		        <div class='input-group' name='datum_tage' id='datum_tage'>
		            <span class='input-group-addon kursor'>Datum</span>
		                <input type='text' class='form-control datum_tage' name='datum_tage' id='datum_tage' value='<?=substr($datum_tage, 0,10) ;?>' style='min-width: 100px;'>
		            <span class='input-group-addon kursor' style='min-width: 10px;'><span class='glyphicon glyphicon-calendar' ></span></span>
		        </div>
		        <div class='input-group' name='halle_group' id='halle_group'>
		            <span class='input-group-addon'>Halle</span>
		            <select class='selectpicker halle' data-width='100%' id='halle' name='halle'>
		                    <option value=3 <?php if($halle_id==3){echo 'selected';}?>>Halle 3</option>
		                    <option value=4 <?php if($halle_id==4){echo 'selected';}?>>Halle 4</option>
		                    <option value=5 <?php if($halle_id==5){echo 'selected';}?>>Halle 4A</option>
		            </select>
		        </div>
		    </div>
		    <div class='col-xs-3'>
		    	<div class='col-xs-3'>
		    		<div><button type="submit" name='submit_form_tagesansicht' id='submit_form_tagesansicht' class="btn btn-success">Daten holen</button></div>
		    	</div>
		    </div>
		    <div class='col-xs-6' style='text-align: right;'>
		    	<a href="#" style='color: inherit;'><i id='submit_form_exportCSV' name='submit_form_exportCSV' class="fa fa-file-excel-o fa-2x" aria-hidden="true"></i></a>
		    	&nbsp;
	            <span id='print' name='print' class="glyphicon glyphicon-print kursor print" aria-hidden="true" title='Dr�cken'></span>
	        </div>

		</div>
	</div>
	</form>
</div>

<form method='POST' role='form' id='form_exportCSV' name='form_exportCSV' action='reports.schichtprotokoll.exportCSV'>
	<input type='hidden' name='report' id='report' value='schichtprotokoll'>
	<input type='hidden' name='datum_tage' id='datum_tage' value='<?=substr($datum_tage, 0,10) ;?>'>
	<input type='hidden' name='halle' id='halle' value='<?=$halle_id;?>'>
</form>

<div class='row nonprintable'><div class='container'><HR></div></div>
<?php
	if($halle_id==3) $halle_txt = '3';
	if($halle_id==4) $halle_txt = '4';
	if($halle_id==5) $halle_txt = '4A';
?>

<div class='row'>
	<div class='container printable' style="display: none;">
		<div class='table-responsive'>
			<table class='table table_header' style='width: 100% !important;'>
				<tr>
					<td style='width: 15%;'><h4 style='color: #337ab7;'>Halle <?=$halle_txt?></h4></td>
					<td style='width: 70%;'><h1>Schichtprotokoll</h1></td>
					<td style='width: 15%;'><h4 style='color: #337ab7;'><?=substr($datum_tage, 0,10) ;?></h4></td>
				</tr>
			</table>
		</div>
	</div>
</div>
<div class='row nonprintable'>
	<div class='container'>
		<div class='col-xs-6' style='padding-left: 0px;'>
			<h4 style='color: #337ab7;'>Datum: <?=substr($datum_tage, 0,10) ;?></h4>
		</div>
		<div class='col-xs-6' style='padding-right: 0px; text-align: right;'>
			<h4 style='color: #337ab7;'>Halle <?=$halle_txt?></h4>
		</div>
	</div>
</div>

<div class='row'>
	<div class='container printable' style='text-align: left;'>
		<span><strong>Technisch</strong></span>
			<span>L0: <span id='0T' name='0T'></span></span>
			<span>L1: <span id='1T' name='1T'></span></span>
			<span>L2: <span id='2T' name='2T'></span></span>
		<span><strong> || Organisatorisch</strong></span>
			<span>L0: <span id='0O' name='0O'></span></span>
			<span>L1: <span id='1O' name='1O'></span></span>
			<span>L2: <span id='2O' name='2O'></span></span>
		<span><strong> || Qualit�t</strong></span>
			<span>L0: <span id='0Q' name='0Q'></span></span>
			<span>L1: <span id='1Q' name='1Q'></span></span>
			<span>L2: <span id='2Q' name='2Q'></span></span>
	</div>
</div>


<?php
	for ($i=0; $i <=2 ; $i++) { 
		$sum[$i.'T'] =0;
		$sum[$i.'O'] =0;
		$sum[$i.'Q'] =0;
	}
?>
<?php for ($i=0; $i<=2; $i++): ?>
<div class='row myDataTable_<?=$i;?> dataTableDiv'>
<div class='container dataTableDiv'>
<div class='caption'>
	<?php
		$dauer_sum = 0;
		$error_count = 0;
		if ($i==0){echo 'Technisch';};
		if ($i==1){echo 'Organisatorisch';};
		if ($i==2){echo 'Qualit�t';};
	?>
</div>
<table id='myDataTable_<?=$i;?>' name='myDataTable_<?=$i;?>' class='table table-striped table-bordered schichten myDataTable dataTable' style='border-spacing: 0px; width: 100%; margin-bottom:0px!important'>
    <thead>
        <tr>
            <th style='width: 5%'>Datum</th>
            <th style='width: 5%'>Schicht</th>
            <th style='width: 5%'>Linie</th>
            <th style='width: 5%'>Anfang</th>
            <th style='width: 7%'>Dauer (Min)</th>
            <th style='width: 8%'>St�rung</th>
            <th style='width: 8%'>Ebene 1</th>
            <th style='width: 8%'>Ebene 2</th>
            <th style='width: 8%'>Ebene 3</th>
            <th style='width: 8%'>Ebene 4</th>
            <th style='width: 25%;'>Bemerkung manuell</th>
            <th style='width: 8%'>Model</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($alle_storungen[$i] as $alle_storunge): ?>
     	<?php
         	$schicht_txt='';
         	$schicht	= $alle_storunge->SCHICHT;
         	
			if ($schicht==1){$schicht_txt = 'Fr�hschicht';};
			if ($schicht==2){$schicht_txt = 'Sp�tschicht';};
			if ($schicht==3){$schicht_txt = 'Nachtschicht';};
		?>
	    <tr>
			<td><?= $alle_storunge->DATUM;?></td> 
	    	<td><?= $schicht_txt;?></td>
	    	<td><?= $alle_storunge->LINIE;?></td>
	    	<td><?= $alle_storunge->VON;?></td>
	    	<td><?= $alle_storunge->DAUER;?></td>
	    	<td><?= $alle_storunge->STORUNG_TEXT;?></td>
	    	<td><?= $alle_storunge->U1_TEXT;?></td>
	    	<td><?= $alle_storunge->U2_TEXT;?></td>
	    	<td><?= $alle_storunge->U3_TEXT;?></td>
	    	<td><?= $alle_storunge->U4_TEXT;?></td>
	    	<td><?= $alle_storunge->KOMENTAR;?></td>
	    	<td><?= $alle_storunge->TYP;?></td>
	    	
	    </tr>
	    	<?php
	    		$linie	= $alle_storunge->LINIE;
         		$art	= $alle_storunge->ART;
         		$dauer	= $alle_storunge->DAUER;
         		$switch = $linie.$art;
         		
         		$dauer_sum = $dauer_sum + $dauer;
				$error_count = $error_count + 1;
         		$sum[$switch] += $dauer;
	    	?>
        <?php endforeach;?>
 	</tbody>
</table>
<table class='table table-bordered schichten' style='border-spacing: 0px; width: 100%;'>
	<tr class='tfoot'>
		<td style='width: 15%; text-align: center;' class='dauerSum<?=$i;?>' data-value='<?=$dauer_sum;?>'>Dauer Sum: <?=$dauer_sum;?> Min</td>
		<td style='width: 70%;'></td>
		<td style='width: 15%; text-align: center;'>Datens�tze: <?=$error_count;?></td>
    </tr>
</table>
</div>
</div>
<BR>
<BR>
<?php endfor; ?>

<?php require ('app/views/partials/footer_vendor_scripts.php'); ?>

<!-- initalize selectpicker -->
<script type='text/javascript'>
    $('.selectpicker').selectpicker({
        size: 4
    });
</script>
<!-- /.initalize selectpicker -->

<!-- configure dateTime Picker -->
<script type='text/javascript'>
	var today = new Date();
    var format_date = 'DD.MM.YYYY';
    var viewMode = 'days';
	
    $('#datum_tage').datetimepicker({
        viewMode: viewMode,
        locale: moment.locale('de'),
        format: format_date,
        
        useCurrent: false,
        extraFormats: false,
        sideBySide: true
    });
</script>
<!-- /.configure dateTime Picker -->

<!-- activate dataTable -->
<script type='text/javascript'>
    $(document).ready(function() {
        $.fn.dataTable.moment('DD.MM.YYYY');
        $('.myDataTable').DataTable({
            searching: false,
            paging: false,
            info: false,
            ordering: false,
            pageLength: 50,
            lengthMenu: [ [50, 150, 250, -1], [50, 150, 250, "Alle"] ],
            language: {
            	emptyTable:   	'Keine Daten in der Tabelle vorhanden',
				info:         	'_START_ bis _END_ von _TOTAL_ Eintr�gen',
				infoEmpty:    	'0 bis 0 von 0 Eintr�gen',
				infoFiltered: 	'(gefiltert von _MAX_ Eintr�gen)',
				infoPostFix:  	'',
				thousands:  	'.',
				lengthMenu:   	'_MENU_ Eintr�ge anzeigen',
				loadingRecords:	'Wird geladen...',
				processing:   	'Bitte warten...',
				search:       	'Suchen',
				zeroRecords:  	'Keine Eintr�ge vorhanden.',
				paginate: {
					first:    	'Erste',
					previous: 	'Zur�ck',
					next:     	'N�chste',
					last:     	'Letzte'
				},
				aria: {
					sortAscending:  ': aktivieren, um Spalte aufsteigend zu sortieren',
					sortDescending: ': aktivieren, um Spalte absteigend zu sortieren'
				},
				select: {
    				rows: {
						_: '%d Zeilen ausgew�hlt',
						0: 'Zum Ausw�hlen auf eine Zeile klicken',
						1: '1 Zeile ausgew�hlt'
	    			}
				}
            }
        });
    });
</script>
<!-- /.activate dataTable -->
<!-- open print dialog on print icon click -->
<script type="text/javascript">
    $('#print').click(function(){
        window.print();
    });
</script>
<!-- /.open print dialog on print icon click -->

<!-- submit exportCSV form -->
<script type="text/javascript">
	$('#submit_form_exportCSV').click(function(){
		$('#form_exportCSV').submit();
	});
</script>
<!-- /.submit exportCSV form -->

<script type="text/javascript">
		var sum	= <?=json_encode($sum);?>;
		var dauerSum = 0;
		for(i=0; i<=2; i++){
			dauerSum = $('.dauerSum'+i).data('value');
			if(dauerSum ==0){
				$('.myDataTable_'+i).hide();
			}
		};

		function gethoursMinutes(i){
			var hours = Math.floor(i/60);
			var minutes = i % 60;

			if (minutes<10){
				minutes = '0'+minutes.toString();
			};
			hoursMinutes = hours.toString()+':'+minutes;

			return hoursMinutes;
		};

		for (i=0; i<=2; i++){
			$('#'+i+'T').text(gethoursMinutes(sum[i+'T']));
			$('#'+i+'O').text(gethoursMinutes(sum[i+'O']));
			$('#'+i+'Q').text(gethoursMinutes(sum[i+'Q']));
		};
</script>

<?php require ('app/views/partials/footer.php'); ?>