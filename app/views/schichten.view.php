<?php require ('app/views/partials/head_admin.php'); ?>


<form method='POST' role='form' id='form' name='form' action='schichten'>
<div class='container'>
    <div class='row'>
        <h3>Admin - Schichten</h3>
        <div class='form-group'>
            <input type='hidden' name='jahre_woche' id='jahre_woche' value=''>
            <div class='row'>
                <div class='col-xs-2'>
                    <input type='checkbox' checked name='mytoggle' id='mytoggle' data-toggle="toggle" data-on="Woche" data-off="Jahre" data-offstyle="success">
                </div>
            </div>
            <BR>
            <div class='row'>
            <div class='col-xs-2'>
                <div class='input-group date' name='datum_schichten' id='datum_schichten'>
                    <span class='input-group-addon datum'>Datum</span>
                        <input type='text' class='form-control' name='datum_schichten' id='datum_schichten' value='' style='min-width: 100px;'>
                    <span class='input-group-addon' style='min-width: 10px;'><span class='glyphicon glyphicon-calendar' ></span></span>
                </div>
            </div>
            </div>
            <BR>
            <div class='row'>
            <?php for ($team=1; $team <=3 ; $team++): ?>
            <div class='col-xs-3'>
                <div class='input-group' name='team_<?=$team?>' id='team_<?=$team?>'>
                	<span class='input-group-addon'>Team <?=$team?></span>
            		<select class='selectpicker' data-width='100%' id='team_<?=$team?>' name='team_<?=$team?>'>
                    		<option value='1'>Früschicht</option>
                            <option value='2'>Spätschicht</option>
                            <option value='3'>Nachtschicht</option>
                		
            		</select>
            	</div>
            </div>
         	<?php endfor; ?>
            </div>
            <BR>
            <div class='row'>
            <div class='col-xs-2'>
                <button type="submit" class="btn btn-info" id="save" name="save">Speichern</button>
            </div>
            </div>
        </div>
    </div>
</div>
</form>

<div class='container'><HR></div>

<div class='container'>
<form method='POST' role='form' id='form_delete' name='form_delete' action='schichten.delete'>
<div class="table-responsive">
<table id='myDataTable' class='table table-striped table-bordered table-hover schichten' data-order='[[ 0, "desc" ]]' style='cellspacing: 0; width: 30%;'>
	<thead>
		<tr>
			<th>Datum</th>
            <th>Woche (KW)</th>
			<th>Früschicht</th>
            <th>Spätschicht</th>
            <th>Nachtschicht</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($schichten as $schichte): ?>
			<tr>
				<td scope=row><?= $schichte->STR_DATE; ?></td>
                <td><?= $schichte->WEEK; ?></td>
				<td>Team <?= substr($schichte->TEAM, 0,1);?></td>
				<td>Team <?= substr($schichte->TEAM, 2,1);?></td>
				<td>Team <?= substr($schichte->TEAM, 4,1);?></td>
                <td>
                    <input type='hidden' id='schichten_id' name='schichten_id' class='schichten_id' value='' />
                    <button type='button' class='delete btn btn-danger btn-circle' id='delete_<?= $schichte->ID;?>' name='delete_<?= $schichte->ID;?>' data-value='<?= $schichte->ID;?>'><i class='glyphicon glyphicon-remove'></i></button>
                </td>
    		</tr>
    	<?php endforeach; ?>
	</tbody>
</table>
</div>
</form>
</div>

<!-- activate dataTable -->
<script type='text/javascript'>
    $(document).ready(function() {
        $.fn.dataTable.moment('DD.MM.YYYY');
        $('#myDataTable').DataTable({
            searching: false,
            paging: false,
            info: false
        });

        $('.ACTIVE_0').parent().css('background-color', '#FDE3A7');
    });
</script>
<!-- /activate dataTable -->
	
<?php require ('app/views/partials/footer_vendor_scripts.php'); ?>

<!-- initalize selectpicker -->
<script type='text/javascript'>
    $('.selectpicker').selectpicker({
        size: 4
    });
</script>

<!-- dateTime Picker -->
<script type='text/javascript'>
	var today = new Date();
    var format_days = 'DD.MM.YYYY';
    var format_years = 'YYYY';
    var viewMode_days = 'days';
    var viewMode_years = 'years';
	var picker = $('#datum_schichten').datetimepicker({
        viewMode: viewMode_days,
        locale: moment.locale('de'),
        format: format_days,
        defaultDate: today,
        useCurrent: false,
        extraFormats: false,
        sideBySide: true,
        calendarWeeks: true
    });

        $('#mytoggle').change(function() {
        if($(this).prop('checked')){
            $('.datum').text('Datum');
            $('#jahre_woche').val('w');
            picker.data("DateTimePicker").format(format_days);
            picker.data("DateTimePicker").viewMode(viewMode_days);

        }
        else{
            $('.datum').text('Jahre');
            $('#jahre_woche').val('j');
            picker.data("DateTimePicker").format(format_years);
            picker.data("DateTimePicker").viewMode(viewMode_years);

        };
    });
    
</script>

<!-- set schichten_id value --> 
<script type="text/javascript">
$('.delete').click(function(){
    $('.schichten_id').val($(this).data('value'));
});
</script>
<!-- /.set schichten_id value -->

<script type="text/javascript">
    $('.delete').click(function() {
    if (confirm('Bist du sicher?')) {
      $('#form_delete').submit();
    }
  });
</script>

<?php require ('app/views/partials/footer.php'); ?>