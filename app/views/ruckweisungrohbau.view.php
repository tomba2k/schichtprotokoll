<?php require ('app/views/partials/head.php'); ?>

<form method="POST" role="form" id="form_saveRuckweisungRohbau" name="form_saveRuckweisungRohbau" action="ruckweisungrohbau.store">
    <div class='container'>
        <div class='row' style='vertical-align: bottom;'>
            <h3>R�ckweisung Rohbau</h3>
            <div class='form-group col-xs-3'>
                <div class="input-group date" name="datum_tage" id="ddatum_tage">
                    <span class="input-group-addon">Datum <? echo $datum_tage;?></span>
                    <input type="text" class="form-control datum_tage nminput" name="datum_tage" id="datum_tage" value="<?=substr($datum_tage, 0,10);?>"  style="min-width: 100px;">
                    <span class="input-group-addon" style="min-width: 10px;"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
            </div>
	
		</div>

        <div class='row'>
            <div class='form-group col-xs-6'>
                <div class='input-group userrights' id='schicht_group'>
                    <span class='input-group-addon'>Schicht</span>
                    <select class='selectpicker nminput' data-width='100%' id='schicht' name='schicht'>
						<option value=''></option>
                        <option value='FS'>FS</option>
                        <option value='SS'>SS</option>
                        <option value='NS'>NS</option>
                    </select>
                </div>
            </div>
        </div>

        <div class='row'>
            <div class='form-group col-xs-6'>
                <div class='input-group team' id='typen_group'>
                    <span class='input-group-addon'>Typen</span>
                    <select class='selectpicker nminput' data-width='100%' id='typen' name='typen'>
						<option value=''></option>
						<option value='B8V'>B8V</option>
						<option value='A7PA'>A7PA</option>
						<option value='A7VPA'>A7VPA</option>
                    </select>
                </div>
            </div>
        </div>

        <div class='row'>
            <div class='form-group col-xs-6'>
                <div class='input-group active' id='grund_group'>
                    <span class='input-group-addon'>Grund</span>
                    <select class='selectpicker nminput' data-width='100%' id='grund' name='grund'>
						<option value=''></option>
                        <option value='0'>Ziehriss/Durchbrenner</option>
                        <option value='1'>Fehltei</option>
                        <option value='2'>Akkustikschott/Sch�umling</option>
                        <option value='4'>Kleberverschmutzung</option>
                        <option value='5'>Sonstiges</option>
                    </select>
                </div>
            </div>
        </div>
        <div class='row'>
            <div class='form-group col-xs-6'>
                <div class='input-group bemerkum' id='bemerkung_input'>
                    <span class='input-group-addon'>Bemerkung</span>
                    <input type='text' class='form-control nminput' placeholder='Bemerkung' id='bemerkung' name='bemerkung' aria-describedby='bemerkum_group'>
                </div>
            </div>
        </div>

        <div class='row'>
            <div class="form-group col-xs-3">
                <button type="submit" class="btn btn-info" id="submit_saveRuckweisungRohbau" name="submit_saveRuckweisungRohbau">Speichem</button>
           <div class='div_submit_form_tagesansicht' style='display: none;'>
                <BR>
                <button type="submit" name='submit_form_tagesansicht' id='submit_form_tagesansicht' class="btn btn-success">Daten holen</button>
            </div>
            </div>
        </div>
    </div>
</form>

<div class='container'><HR></div>

<div class='container'>
    <div class='table-responsive'>
        <table id='myDataTable' class='table table-striped table-bordered table-hover schichten' style='cellspacing: 0; width: 70%;'>
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Datum</th>
                    <th>Typ</th>
                    <th>Grund</th>
                    <th>Schicht</th>
                    <th>Bemerkung</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($ruckweisungrohbau as $item): ?>
                    <tr>
                        <?php foreach ($item as $key => $value): ?>
                                <td class='<?= $key . '_' . $value; ?>' id='<?= $key; ?>'><?= $value; ?></td>
     
                        <?php endforeach; ?>
						<td>
                            <button type='button' name='btn_editRuckweisungRohbau' id='btn_editRuckweisungRohbau' class='btn btn-xs btn-success btn_editRuckweisungRohbau' value='edit-ruckweisungrohbau'>Edit</button>
                            <button type='button' name='btn_deleteRuckweisungRohbau' id='btn_deleteRuckweisungRohbau' class='btn btn-xs btn-danger btn_deleteRuckweisungRohbau' value='btn_deleteRuckweisungRohbau'>Delete</button>
						</td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<!-- User change modal -->
<div class='modal fade' id='modal_editRuckweisungRohbau' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
    <div class='modal-dialog'>
        <div class='modal-content'>
            <div class='modal-header'>
                <h4>Change data</h4>
            </div>
            <form method="POST" role="form" id="form_updateRuckweisungRohbau" name="form_updateRuckweisungRohbau" action="ruckweisungrohbau.edit">
                <div class='modal-body'>
                    <div class='container'>
                        <input type='hidden' id='modalid' name='modalid' />
                        <table>
                            <tr>
                                <td>
									<div class="input-group date" name="datum_tage" id="datum_tage_">
										<span class="input-group-addon">Datum</span>
										<input type="text" class="form-control datum_tage" name="mdatum_tage" id="mdatum_tage"  style="min-width: 100px;">
										<span class="input-group-addon" style="min-width: 10px;">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
                                </td>
                            </tr>
                            <tr>
                                <td> 
                                    <div class='input-group col-xs-6' name='modalusername_addon' id='modalschicht'>
                                        <span class='input-group-addon'>Schicht</span>
										<select class='selectpicker' data-width='100%' id='mschicht' name='mschicht'>
											<option value='FS'>FS</option>
											<option value='SS'>SS</option>
											<option value='NS'>NS</option>
										</select>
									</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class='input-group' name='typen' id='modaltypen'>
                                        <span class='input-group-addon'>Typen</span>
										<select class='selectpicker' data-width='100%' id='mtypen' name='mtypen'>
											<option value='B8V'>B8V</option>
											<option value='A7PA'>A7PA</option>
											<option value='A7VPA'>A7VPA</option>
										</select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class='input-group' name='modalgrund' id='modalgrund'>
                                        <span class='input-group-addon'>Grund</span>
										<select class='selectpicker' data-width='100%' id='mgrund' name='mgrund'>
												<option value='Ziehriss/Durchbrenner'>Ziehriss/Durchbrenner</option>
												<option value='Fehltei'>Fehltei</option>
												<option value='Akkustikschott/Sch�umlin'>Akkustikschott/Sch�umlin</option>
												<option value='Kleberverschmutzung'>Kleberverschmutzung</option>
												<option value='Sonstiges'>Sonstiges</option>
											</select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class='input-group' name='modalbemerkung' id='modalbemerkung'>
                                        <span class='input-group-addon'>Bemerkung</span>
                                        <input type='text' class='form-control' id='mbemerkung' name='mbemerkung' value='' />
                                    </div>
                                </td>
                            </tr>
  
                        </table>
                    </div>
                </div>
                <div class='modal-footer'>
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Abbruch</button>
                    <button type='submit' class='btn btn-info btn_submitRuckweisungRohbau' id='btn_submitRuckweisungRohbau' name='btn_submitRuckweisungRohbau'>Update</button>
					
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /User change modal -->
<!-- define global functions -->
<script type="text/javascript">
    function isBetween(n, a, b) {
            return (n - a) * (n - b) <= 0
        };

    function fillVonBisHours (start, end) {
        if (Number(start)>Number(end)){
                
                
                for(i=Number(start); i<=23; i++){
                    if(i<10){i='0'+i};
                    $('#startzeit_h, #endzeit_h').append('<option value="'+i+'">' + i + '</option>');

                };
                for(i=0; i<=Number(end); i++){
                    if(i<10){i='0'+i};
                    $('#startzeit_h, #endzeit_h').append('<option value="'+i+'">' + i + '</option>');
                };
            }
            else {
                for(i=Number(start); i<=Number(end); i++){
                    if(i<10){i='0'+i};
                    $('#startzeit_h, #endzeit_h').append('<option value="'+i+'">' + i + '</option>');
                };
            };
    };
</script>
<!-- /.define global functions -->

<?php require ('app/views/partials/footer_vendor_scripts.php'); ?>
<!-- initalize dataTable -->
<script type='text/javascript'>
    $(document).ready(function() {
        $.fn.dataTable.moment('DD.MM.YYYY');
        $('#myDataTable').DataTable({
            searching: false,
            paging: false,
            info: false,
            language: {
                emptyTable:     'Keine Daten in der Tabelle vorhanden',
                info:           '_START_ bis _END_ von _TOTAL_ Eintr�gen',
                infoEmpty:      '0 bis 0 von 0 Eintr�gen',
                infoFiltered:   '(gefiltert von _MAX_ Eintr�gen)',
                infoPostFix:    '',
                thousands:      '.',
                lengthMenu:     '_MENU_ Eintr�ge anzeigen',
                loadingRecords: 'Wird geladen...',
                processing:     'Bitte warten...',
                search:         'Suchen',
                zeroRecords:    'Keine Eintr�ge vorhanden.',
                paginate: {
                    first:      'Erste',
                    previous:   'Zur�ck',
                    next:       'N�chste',
                    last:       'Letzte'
                },
                aria: {
                    sortAscending:  ': aktivieren, um Spalte aufsteigend zu sortieren',
                    sortDescending: ': aktivieren, um Spalte absteigend zu sortieren'
                },
                select: {
                    rows: {
                        _: '%d Zeilen ausgew�hlt',
                        0: 'Zum Ausw�hlen auf eine Zeile klicken',
                        1: '1 Zeile ausgew�hlt'
                    }
                }
            }
        });
    });
</script>
<!-- activate dataTable -->
<script type='text/javascript'>
    // autorefresh varables
    var autoplay = null;
    //var int = null;
    //var onoff = null;
    var onoff = '<?=$autorefresh;?>';
    var int = '<?=$autorefreshtime;?>';


    // start time, end time, dauer varabales
    var hours = <?= json_encode($hours);?>;

    var current_schicht = <?=$schicht;?>;
    // selection block variables
    var show_hide_speed = 0;
    var autotext_text ='';
    var auto_text_container ='';

	      // start time, end time, dauer varabales

    var current_date = new Date();
    var current_date_val = '<?=substr($datum_tage, 0,10);?>';
    var current_hour = current_date.getHours();
    if (current_hour<10){current_hour = '0'+current_hour;};

    var current_minute = current_date.getMinutes();
    if (current_minute<10){current_minute = '0'+current_minute;};
	var filtered = hours.filter(function (v,i) {
                        return v.SCHICHT==current_schicht;
                    });
    var start = Number(filtered[0].STR_TIME.substr(0,2));
    if (isBetween(Number(start),0,9)){start = '0'+start;};
    var end = Number(filtered[0].STR_TIME_END.substr(0,2));
    if (isBetween(Number(end),0,9)){end = '0'+end;};


           
</script>
<!-- configure dateTime Picker -->
<script type='text/javascript'>
    function initializeDateTimePicker(){
        var today = new Date();
        var format_date = 'DD.MM.YYYY';
        var viewMode = 'days';
        
        $('#datum_tage').datetimepicker({
            viewMode: viewMode,
           // locale: moment.locale('de'),
            format: format_date,
            
            useCurrent: false,
            extraFormats: false,
            sideBySide: true
        });        
        $('#mdatum_tage').datetimepicker({
            viewMode: viewMode,
           // locale: moment.locale('de'),
            format: format_date,
            
            useCurrent: false,
            extraFormats: false,
            sideBySide: true
        });
    }
    initializeDateTimePicker();
</script>
<!-- /.configure dateTime Picker -->
<!-- set values for modal window -->
<script type='text/javascript'>

    $('.btn_editRuckweisungRohbau').click(function () {
        $('#modal_editRuckweisungRohbau').modal('show');

        var id = $(this).closest('tr').children('#ID').text();
        var datum = $(this).closest('tr').children('#DATUM').text();
        var schicht = $(this).closest('tr').children('#SCHICHT').text();
        var typen = $(this).closest('tr').children('#TYPE').text();
        var grund = $(this).closest('tr').children('#GRUND').text();
        var bemerkung = $(this).closest('tr').children('#BEMERKUNG').text();

		//alert(bemerkung);

        $('#modalid').val(id).text(id);
        $('#mdatum_tage').val(datum);
        $('#mschicht').selectpicker('val', schicht);
        $('#mtypen').selectpicker('val', typen);
        $('#mgrund').selectpicker('val', grund);
        $('#mbemerkung').val(bemerkung);
    });
	
	
    $('#submit_saveRuckweisungRohbau').click(function (e) {
		e.preventDefault();

        var datum_tage = $('#datum_tage').val();
		var schicht = $('#schicht').val();
		var typen = $('#typen').val();
		var grund = $('#grund').val();
		var bemerkung = $('#bemerkung').val();
		
		if (datum_tage != "" && schicht != "" && typen != "" && grund != "" && bemerkung != "" ) {
			$('form#form_saveRuckweisungRohbau').submit();			
		} 

    });

	function checkValue(x){
		return (x =="")? "solid 1px #F00" : "solid 0px #F00"
	}
	function validateFields(){
		$("#schicht_group").css("border", checkValue($( "#schicht" ).val()));
		$("#typen_group").css("border", checkValue($( "#typen" ).val()));
		$("#grund_group").css("border", checkValue($( "#grund" ).val()));
		$("#bemerkung_input").css("border", checkValue($( "#bemerkung" ).val()));
		$("#modalbemerkung").css("border", checkValue($( "#mbemerkung" ).val()));
	}
	
	$bemerkung = $( "#bemerkung" );
	$bemerkung.keyup(function() {
		validateFields();
	});
	$mbemerkung = $( "#mbemerkung" );
	$mbemerkung.keyup(function() {
		validateFields();
	});
	$data = $( ".nminput" );
	$data.change(function() {
		validateFields();
	});

</script>
<!-- /set values for modal window -->
<!-- insert/edit submit controller -->
<script type="text/javascript">
    $('#btn_submitRuckweisungRohbau').click(function(e){
		e.preventDefault();
		// change date to yesterday if time between 00:00 and 05:59 AND is Nachtschicht
		if (Number(current_schicht) == 3 &&  isBetween(Number(current_hour),0,5) == true){
			today = moment();
			yesterday = moment(today).subtract(1, 'days');
			yesterday = moment(yesterday).format('DD.MM.YYYY');
			$('#datum_tage_modal').val(yesterday);
		};
		$('#form_modal').attr('action', 'schichtprotokoll.store');
		
		var datum_tage = $('#mdatum_tage').val();
		var schicht = $('#mschicht').val();
		var typen = $('#mtypen').val();
		var grund = $('#mgrund').val();
		var bemerkung = $('#mbemerkung').val();
		
		if (datum_tage != "" && schicht != "" && typen != "" && grund != "" && bemerkung != "" ) {
			$('form#form_updateRuckweisungRohbau').submit();			
		} else {
			$("#datum_tage_").css("border", checkValue(schicht));
			$("#modalschicht").css("border", checkValue(schicht));
			$("#modaltypen").css("border", checkValue(typen));
			$("#modalgrund").css("border", checkValue(grund));
			$("#modalbemerkung").css("border", checkValue(bemerkung));
		}
		$('#form_modal').submit();

    });


</script>	
	
	<!-- delete selected error -->
<script type="text/javascript">
    $('.btn_deleteRuckweisungRohbau').click(function () {
		var id = $(this).closest('tr').children('#ID').text();
		if(confirm('Delete Qualit�tsschwerpunkte?')){
			$.post( "ruckweisungrohbau.delete", { delete: id } );
			location.reload();
		};
    });
</script>
<!-- /.insert/edit submit controller -->
<?php require ('app/views/partials/footer.php'); ?>

