<?php require ('app/views/partials/head_admin.php'); ?>

<form method="POST" role="form" id="form_saveUser" name="form_saveUser" action="users">
<div class='container'>
    <div class='row'>
    <h3>Admin - Users</h3>
    <div class='form-group col-xs-2'>
        <div class='input-group name' id='name_group'>
            <span class='input-group-addon'>Name</span>
            <input type='text' class='form-control' placeholder='Name' id='name' name='name' aria-describedby='name_group'>
        </div>
    </div>
    <div class='form-group col-xs-2'>
        <div class='input-group surname' id='surname_group'>
            <span class='input-group-addon'>Surname</span>
            <input type='text' class='form-control' placeholder='Surname' id='surname' name='surname' aria-describedby='surname_group'>
        </div>
    </div>
    </div>

    <div class='row'>
    <div class='form-group col-xs-2'>
        <div class='input-group username' id='username_group'>
            <span class='input-group-addon'>Username</span>
            <input type='text' class='form-control' placeholder='Username' id='username' name='username' aria-describedby='username_group'>
        </div>
    </div>
    <div class='form-group col-xs-2'>
        <div class='input-group password' id='password_group'>
            <span class='input-group-addon'>Password</span>
            <input type='password' class='form-control' placeholder='Password' id='password' name='password' aria-describedby='password_group'>
        </div>
    </div>
    </div>

    <div class='row'>
    <div class='form-group col-xs-2'>
        <div class='input-group userrights' id='userrights_group'>
            <span class='input-group-addon'>Userrights</span>
            <select class='selectpicker' data-width='100%' id='userrights' name='userrights'>
                <option value='U'>User</option>
                <option value='A'>Administrator</option>
            </select>
        </div>
    </div>
    </div>

    <div class='row'>
    <div class='form-group col-xs-2'>
        <div class='input-group team' id='team_group'>
            <span class='input-group-addon'>Team</span>
            <select class='selectpicker' data-width='100%' id='team' name='team'>
                <?php for ($i=0; $i <=3 ; $i++): ?>
                <option value='<?=$i;?>'><?=$i;?></option>
                <?php endfor; ?>
            </select>
        </div>
    </div>
    </div>

    <div class='row'>
    <div class='form-group col-xs-2'>
        <div class='input-group active' id='active_group'>
            <span class='input-group-addon'>Active</span>
            <select class='selectpicker' data-width='100%' id='active' name='active'>
                <option value='0'>Nein</option>
                <option value='1'>Ja</option>
            </select>
        </div>
    </div>
    </div>

    <div class='row'>
    <div class="form-group col-xs-3">
        <button type="submit" class="btn btn-info" id="submit_saveUser" name="submit_saveUser">Save user</button>
    </div>
    </div>
</div>
</form>

<div class='container'><HR></div>

<div class='container'>
<div class='table-responsive'>
<table id='myDataTable' class='table table-striped table-bordered table-hover schichten' style='cellspacing: 0; width: 70%;'>
<thead>
	<tr>
		<th>Id</th>
        <th>Name</th>
        <th>Surname</th>
        <th>Username</th>
        <th>Team</th>
        <th>Userrights</th>
        <th>Active</th>
        <th></th>
	</tr>
</thead>
<tbody>
    <?php foreach ($users as $user): ?>
    <tr>
        <?php foreach ($user as $key => $value): ?>
            <?php if ($key !='PASSWORD'): ?>
                <td class='<?= $key.'_'.$value;?>' id='<?= $key;?>'><?=$value;?></td>
            <?php else: ?>
                <td class='<?= $key.'_'.$value;?>' id='<?= $key;?>'>
                    <button type='button' name='btn_editUser' id='btn_editUser' class='btn btn-xs btn-success btn_editUser' value='edit-user'>Edit</button>
                    <input type='hidden' id='PASS' name='PASS' value='<?= $value ?>' />
                </td>
            <?php endif; ?>

        <?php endforeach; ?>
    </tr>
    <?php endforeach; ?>
</tbody>
</table>
</div>
</div>

<!-- User change modal -->
<div class='modal fade' id='modal_editUser' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
    <div class='modal-dialog'>
        <div class='modal-content'>
            <div class='modal-header'>
                <h4>Change user data</h4>
            </div>
            <form method="POST" role="form" id="form_updateUser" name="form_updateUser" action="users.edit">
            <div class='modal-body'>
                <div class='container'>
                    <input type='hidden' id='modalid' name='modalid' value='' />
                    <table>
                        <tr>
                            <td>
                            <div class='input-group' name='modalname_addon' id='modalname_addon'>
                                <span class='input-group-addon'>Name</span>
                                <input type='text' class='form-control' id='modalname' name='modalname' value='' />
                            </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <div class='input-group' name='modalusername_addon' id='modalusername_addon'>
                                <span class='input-group-addon'>Username</span>
                                <input type='text' class='form-control' id='modalusername' name='modalusername' value='' />
                            </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <div class='input-group' name='modalpassword_addon' id='modalpassword_addon'>
                                <span class='input-group-addon'>Password</span>
                                <input type='password' class='form-control' id='modalpassword' name='modalpassword' value='' />
                            </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <div class='input-group' name='modalteam_addon' id='modalteam_addon'>
                                <span class='input-group-addon'>Team</span>
                                <select class='selectpicker' data-width='100%' id='modalteam' name='modalteam'>
                                    <option value='0'>0</option>
                                    <option value='1'>1</option>
                                    <option value='2'>2</option>
                                    <option value='3'>3</option>
                                </select>
                            </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <div class='input-group' name='modaluserrights_addon' id='modaluserrights_addon'>
                                <span class='input-group-addon'>Userrights</span>
                                <select class='selectpicker' data-width='100%' id='modaluserrights' name='modaluserrights'>
                                    <option value='U'>User</option>
                                    <option value='A'>Administrator</option>
                                </select>
                            </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <div class='input-group' name='modalactive_addon' id='modalactive_addon'>
                                <span class='input-group-addon'>Aktive</span>
                                <select class='selectpicker' data-width='100%' id='modalactive' name='modalactive'>
                                    <option value='0'>Nein</option>
                                    <option value='1'>Ja</option>
                                </select>
                            </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class='modal-footer'>
                <button type='button' class='btn btn-default' data-dismiss='modal'>Abbruch</button>
                <button type='submit' class='btn btn-info' id='submit_updateUser' name='submit_updateUser'>Update</button>
            </div>
            </form>
         </div>
    </div>
</div>
<!-- /User change modal -->

<!-- activate dataTable -->
<script type='text/javascript'>
    $(document).ready(function() {
        $('#myDataTable').DataTable({
            searching: false,
            paging: false,
            info: false
        });

        $('.ACTIVE_0').parent().css('background-color', '#FDE3A7');
    });
</script>
<!-- /activate dataTable -->

<!-- set values for modal window -->
<script type='text/javascript'>
    $('.btn_editUser').click(function(){
        $('#modal_editUser').modal('show');

        var id = $(this).closest('tr').children('#ID').text();
        var name = $(this).closest('tr').children('#NAME').text();
        var surname = $(this).closest('tr').children('#SURNAME').text();
        var team = $(this).closest('tr').children('#TEAM').text()*1;
        var username = $(this).closest('tr').children('#USERNAME').text();
        var password = $(this).closest('tr').children('#PASSWORD').children('#PASS').val();
        var userrights = $(this).closest('tr').children('#USERRIGHTS').text();
        var active = $(this).closest('tr').children('#ACTIVE').text()*1;

        $('#modalid').val(id);
        $('#modalname').val(name+' '+surname);
        $('#modalusername').val(username);
        $('#modalpassword').val(password);
       
        $('#modalteam').selectpicker('val', team);
        $('#modaluserrights').selectpicker('val', userrights);
        $('#modalactive').selectpicker('val', active);
    });
</script>
<!-- /set values for modal window -->

<?php require ('app/views/partials/footer_vendor_scripts.php'); ?>
<?php require ('app/views/partials/footer.php'); ?>

