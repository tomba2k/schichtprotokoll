<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-fixed-top navbar-dropshadow">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">
        <img alt="Brand" class="logo" src="public/images/vw_logo.png">
      </a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li><a href="/schichtprotokoll">Home</a></li>
        <li><a href="schichtprotokoll">Störungseingabe</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Reports <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="reports.alle_storungen?autorefresh=on&autorefreshtime=10">Störungsliste</a></li>
            <li><a href="#">Schichtprotokoll</a></li>
          </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Admin <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="users">Users</a></li>
            <li class="divider"></li>
            <li><a href="art">Art</a></li>
            <li><a href="typ">Typ</a></li>
            <li><a href="schichten">Schichten</a></li>
            <li class="divider"></li>
            <li><a href="storungen">Storungen</a></li>
          </ul>
        </li>
      </ul>
       <ul class="nav navbar-nav navbar-right">
        <li><p class="potpis">Boris Plivelic<br>+49(0)15204309292</p></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
       <li class="active"><a class="navbar-brand" href="/schichtprotokoll">Schichtprotokoll</a></li>
      </ul>
     
      <!-- Login -->
      <ul class="nav navbar-nav navbar-right">
        <li class="navbar-login"><a href="#loginFormModal" data-toggle="modal" data-target="#loginFormModal"><span class="glyphicon glyphicon-user"></span> <span class="sr-only">(current)</span></a></li>
      </ul>
      <!-- /.Login -->
    </div><!--/.nav-collapse -->
  </div>
</nav>

<!-- loginForm modal -->
<div id="loginFormModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridModalLabel">
  <div class="modal-dialog" style="width: 400px;" role="document">
    <div class="modal-content">
      <form class="form-horizontal" role="form">
        <div class="modal-header" style="text-align: center">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="gridModalLabel">Login</h4>
        </div>
        <div class="modal-body">
          <div class="container-fluid">        
            <div class="form-group">
              <label  class="col-sm-4 control-label" for="inputEmail3">Username:</label>
              <div class="col-sm-8">
                <input type="email" class="form-control" id="inputEmail3" placeholder="Username"/>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-4 control-label" for="inputPassword3" >Password:</label>
              <div class="col-sm-8">
                <input type="password" class="form-control" id="inputPassword3" placeholder="Password"/>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary">Login</button>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<!-- /.loginForm modal -->