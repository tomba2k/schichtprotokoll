<!DOCTYPE html>
<html lang="de">
<head>
    <title>Schichtprotokoll</title>
    
    <meta charset="ISO-8859-15">
    
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

     <meta name="description" content="VW Schichtprotokoll">
    <meta name="author" content="Bruno Dugorepec">

    <link rel="shortcut icon" href="public/images/vw_logo.ico">
    
    <title>Document</title>

    <link href="public/css/bootstrap.min.css" rel="stylesheet">
    <link href="public/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="public/css/bootstrap-toggle.css" rel="stylesheet">
    <link href="public/css/dataTables.bootstrap.min.css" rel="stylesheet"/>
    <link href="public/css/font-awesome.min.css" rel="stylesheet">
    <link href="public/css/datetimepicker.min.css" rel="stylesheet">
    <link href="public/css/style.css" rel="stylesheet">
    
    <script type="text/javascript" src="vendor/js/jquery.min.js"></script>
</head>
<body>
    <?php require ('app/views/partials/nav.php'); ?>
    <div class="container">
