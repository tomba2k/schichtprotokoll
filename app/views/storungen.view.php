<?php require ('app/views/partials/head_admin.php'); ?>
<?php $previous = $tab-1;?>
<div class='container'>
	<h3>Störungen</h3>
</div>

<!-- Storungen/Unterkategorie tabulator -->
<div class='row'>
<div class='container'> 
	<form method='POST' role='form' id='form_tab' name='form_tab' action='storungen'>
		<input type='hidden' name='tab' id='tab' value='<?= $tab;?>'>
	    <ul class='nav nav-pills'>
	        <li id='tab_li_storung' name='tab_li_storung' data-key ='0' class='tab <?php if ($tab == 0){echo "active";} ?>'><a href='#tab_storung' data-toggle='tab'>Störungen</a></li>
	        <?php for ($i=1; $i<=4; $i++): ?>
	        	<li id='tab_li_unterkategorie<?=$i;?>' name='tab_li_unterkategorie<?=$i;?>' data-key ='<?=$i;?>' class='tab <?php if ($tab == $i){echo "active";} ?>'><a href='#tab_unterkategorie<?=$i;?>' data-toggle='tab'>Unterkategorie <?=$i;?></a></li>
	    	<?php endfor; ?>
	    </ul>
	</form>
</div>
</div>
<!--/.Storungen/Unterkategorie tabulator -->

<?php if ($tab == '0'): ?>
<!-- TAB1 - Storungen -->
<div class='row'>
	<div class='container'>
        <div class='col-xs-3' style='min-width: 280px;'>
            <h4>Störungen</h4>
            <div class='well well-sm mainwell' style='min-width: 280px; max-height: 300px;overflow: auto;'>
        		<ul class='list-group checked-list-box'>
        			<?php foreach ($storungen as $key => $value): ?>
        				<?php $checked = ($value->ACTIVE == '1') ? 'check' : 'unchecked' ; ?>
                  		<li class='li_storungen kursor list-group-item <?=$checked;?>' id ='<?=$tab."_".$value->ID?>' name='<?=$tab."_".$value->ID?>' >
                  			<span id= '<?="chk_".$tab."_".$value->ID?>' name='<?="chk_".$tab."_".$value->ID?>' class='storung_chk kursor <?=$checked;?> glyphicon glyphicon-<?=$checked;?>'></span>
                  			<?=$value->NR?> - <?=$value->TEXT?>
                  		</li>  		
                  	<?php endforeach; ?>
                </ul>
            </div>
        </div>
        <div class='col-xs-4'>
        	<div class='container'>
	        	<h4>Hallen und Linien active status</h4>
	        	<?php for($i=3; $i<=5; $i++): ?>
	        	<div class='col-xs-4'>
	        		<ul class='left-padding-0'>
	        			<li class='<?=$tab;?>_li_halle kursor unchecked' id ='li_H<?=$i;?>_A' name='li_H<?=$i;?>_A'>
	        				<h5><span id= 'chk_H<?=$i;?>_A' name='chk_H<?=$i;?>_A' class='halle_chk kursor glyphicon glyphicon-unchecked'></span><strong>Halle <?=$retVal=($i==5) ? '4A' : $i;?></strong></h5>
	        			</li>
	        			<?php for($j=0; $j<=2; $j++): ?>
	        			<li class='<?=$tab;?>_li_linien kursor unchecked' id ='li_H<?=$i;?>L<?=$j;?>' name='li_H<?=$i;?>L<?=$j;?>' onclick=''>
		    				<span id= 'chk_H<?=$i;?>L<?=$j;?>' name='chk_H<?=$i;?>L<?=$j;?>' class='linien_chk kursor glyphicon glyphicon-unchecked'></span>
		    				<label>Linie <?=$j;?></label>
		    			</li>
		    			<?php endfor; ?>
		    		</ul>
		    	</div>
		    	<?php endfor; ?>
	    	</div>
    	</div>
    </div>
    
    <div class='container'>
    <HR>
    <form class='form-inline' role='form' method='POST' id='form_new_storung' name='form_new_storung' action='storungen.new_storung'>
    	<div class='col-xs-8'>
    		<h4>Neu Störung</h4>
    		<div class='col-xs-2' style='padding-left: 0px;'>
    		<div class='input-group'>
	            <span class='input-group-addon' style='min-width: 30px;'>NR</span>
	            <input type='text' class='form-control' name='new_storung_nr' id='new_storung_nr' value='' style='min-width: 60px;'>
	        	</div>
        	</div>
        	<div class='col-xs-6'>
        	<div class='input-group'>
        		<span class='input-group-addon'>Storung</span>
        		<input type='text' class='form-control' name='new_storung' id='new_storung' value='' style='min-width: 100px;'>
        	</div>
        	</div>
    	</div>
    </form>
	</div>
    <BR>

    <div class='container'>
    <div class='col-xs-2'>
       	<div><button type="button" name='submit_form_new_storung' id='submit_form_new_storung' class="btn btn-success">Daten holen</button></div>
    </div>
    </div>
</div>
<!-- /.TAB1 - Storungen -->
<?php endif; ?>

<?php if ($tab == '1'): ?>
<!-- TAB2 - Unterkategorie 1 -->
<div class='row'>
	<div class='container'>
		<div class='col-xs-3'>
            <h4>Unterkategorie 1</h4>
            <div class='well well-sm mainwell' style='min-width: 280px; max-height: 300px;overflow: auto;'>
        		<ul class='list-group checked-list-box'>
        			<?php foreach ($unterkategorien as $key => $value): ?>
        				<?php if($value->UNTERKATEGORIE_NR == '1'): ?>
	        				<?php $checked = ($value->ACTIVE == '1') ? 'check' : 'unchecked' ; ?>
	                  		<li class='kursor list-group-item <?=$checked;?> li_unterkat<?=$tab;?>' id ='li_<?=$tab."_".$value->ID?>' name='li_<?=$tab."_".$value->ID?>'>
	                  			<span id= '<?="chk_".$tab."_".$value->ID?>' name='<?="chk_".$tab."_".$value->ID?>' class='kursor chk_unterkat<?=$tab;?> glyphicon glyphicon-<?=$checked;?>'></span>
	                  			<?=$value->TEXT?>
	                  		</li>
	                  	<?php endif; ?>
                  	<?php endforeach; ?>
                </ul>
            </div>
        </div>
        
        <div class='col-xs-3'>
        	<div class='container'>
	        	<h4>Hallen und Linien active status</h4>
	        	<div class='col-xs-6'>
	        		<ul class='left-padding-0'>
	        			<?php for($i=3; $i<=5; $i++): ?>
	        			<li class='<?=$tab;?>_li_halle kursor unchecked' id ='li_<?=$tab;?>_H<?=$i;?>' name='li_<?=$tab;?>_H<?=$i;?>'>
	        				<span id= 'chk_<?=$tab;?>_H<?=$i;?>' name='chk_<?=$tab;?>_H<?=$i;?>' data-column='HALLE' class='<?=$tab;?>_halle_chk kursor glyphicon glyphicon-unchecked'></span>
	        				<label>Halle <?=$retVal=($i==5) ? '4A' : $i;?></label>
	        			</li>
	        			<?php endfor; ?>
	        		</ul>
		    	</div>
		    	<div class='col-xs-6'>
	        		<ul class='left-padding-0'>
		    			<?php for($j=0; $j<=2; $j++): ?>
	        			<li class='<?=$tab;?>_li_linien kursor unchecked' id ='li_<?=$tab;?>_L<?=$j;?>' name='li_<?=$tab;?>_L<?=$j;?>' onclick=''>
		    				<span id= 'chk_<?=$tab;?>_L<?=$j;?>' name='chk_<?=$tab;?>_L<?=$j;?>' data-column='LINIE_ARRAY' class='<?=$tab;?>_linien_chk kursor glyphicon glyphicon-unchecked'></span>
		    				<label>Linie <?=$j;?></label>
		    			</li>
		    			<?php endfor; ?>
		    		</ul>
		    	</div>
	    	</div>
    	</div>
        
        <div class='col-xs-3'>
            <h4>Störungen</h4>
            <div class='well well-sm' style='min-width: 280px; max-height: 300px;overflow: auto;'>
        		<ul class='list-group checked-list-box'>
        			<?php foreach ($storungen as $key => $value): ?>
               		<li class='kursor list-group-item li_unterkat<?=$tab;?>_storungen unchecked' id ='li_<?=$tab."_storung_".$value->ID?>' name='li_<?=$tab."_storung_".$value->ID?>'>
               			<span id= 'chk_<?=$tab."_storung_".$value->ID?>' name='chk_<?=$tab."_storung_".$value->ID?>' data-column='ID_PARRENT' class='kursor chk_unterkat<?=$tab;?>_storungen glyphicon glyphicon-unchecked'></span>
               			<?=$value->NR?> - <?=$value->TEXT?>
               		</li>  		
                  	<?php endforeach; ?>
                </ul>
            </div>
        </div>
	</div>

	<div class='container'>
	<HR>
	<form class='form-inline' role='form' method='POST' id='form_new_unterkategorie<?=$tab;?>' name='form_new_unterkategorie<?=$tab;?>' action='storungen.new_unterkategorie'>
		<div class='col-xs-8'>
			<h4>Neu Unterkategorie 1</h4>
			
	    	<div class='input-group'>
	    		<span class='input-group-addon'>Name</span>
	    		<input type='text' class='form-control new_unterkat<?=$tab;?>' name='new_unterkat[<?=$tab;?>]' id='new_unterkat[<?=$tab;?>]' value='' style='min-width: 100px;'>
	    	</div>
		</div>
	</form>
	</div>
	<BR>
	<div class='container'>
	    <div class='col-xs-2'>
	       	<div><button type="button" name='submit_form_new_unterkategorie<?=$tab;?>' id='submit_form_new_unterkategorie<?=$tab;?>' class="btn btn-success">Daten holen</button></div>
	    </div>
	</div>
</div>
<!-- /.TAB2 - Unterkategorie 1 -->
<?php endif; ?>

<?php if ($tab == '2' || $tab == '3' || $tab == '4'): ?>
<!-- TAB3/4/5 - Unterkategorie 2/3/4 -->
<div class='row'>
	<div class='container'>
		<div class='col-xs-3'>
            <h4>Unterkategorie <?=$tab; ?></h4>
            <div class='well well-sm mainwell' style='min-width: 280px; max-height: 300px;overflow: auto;'>
        		<ul class='list-group checked-list-box'>
        			<?php foreach ($unterkategorien as $key => $value): ?>
        				<?php if($value->UNTERKATEGORIE_NR == $tab): ?>
	        				<?php $checked = ($value->ACTIVE == '1') ? 'check' : 'unchecked' ; ?>
	                  		<li class='kursor list-group-item <?=$checked;?> li_unterkat<?=$tab;?>' id ='li_<?=$tab."_".$value->ID?>' name='li_<?=$tab."_".$value->ID?>'>
	                  			<span id= '<?="chk_".$tab."_".$value->ID?>' name='<?="chk_".$tab."_".$value->ID?>' class='kursor chk_unterkat<?=$tab;?> glyphicon glyphicon-<?=$checked;?>'></span>
	                  			<?=$value->TEXT?>
	                  		</li>
	                  	<?php endif; ?>
                  	<?php endforeach; ?>
                </ul>
            </div>
        </div>
        
        <div class='col-xs-3'>
        	<div class='container'>
	        	<h4>Hallen und Linien active status</h4>	        	
	        	<div class='col-xs-6'>
	        		<ul class='left-padding-0'>
	        			<?php for($i=3; $i<=5; $i++): ?>
	        			<li class='<?=$tab;?>_li_halle kursor unchecked' id ='li_<?=$tab;?>_H<?=$i;?>' name='li_<?=$tab;?>_H<?=$i;?>'>
	        				<span id= 'chk_<?=$tab;?>_H<?=$i;?>' name='chk_<?=$tab;?>_H<?=$i;?>' data-column='HALLE' class='<?=$tab;?>_halle_chk kursor glyphicon glyphicon-unchecked'></span>
	        				<label>Halle <?=$retVal=($i==5) ? '4A' : $i;?></label>
	        			</li>
	        			<?php endfor; ?>
	        		</ul>
		    	</div>
		    	<div class='col-xs-6'>
	        		<ul class='left-padding-0'>
		    			<?php for($j=0; $j<=2; $j++): ?>
	        			<li class='<?=$tab;?>_li_linien kursor unchecked' id ='li_<?=$tab;?>_L<?=$j;?>' name='li_<?=$tab;?>_L<?=$j;?>' onclick=''>
		    				<span id= 'chk_<?=$tab;?>_L<?=$j;?>' name='chk_<?=$tab;?>_L<?=$j;?>' data-column='LINIE_ARRAY' class='<?=$tab;?>_linien_chk kursor glyphicon glyphicon-unchecked'></span>
		    				<label>Linie <?=$j;?></label>
		    			</li>
		    			<?php endfor; ?>
		    		</ul>
		    	</div>
	    	</div>
    	</div>
    	<div class='col-xs-3' style='display:<?=$show;?>'>
            <h4>Unterkategorie <?=$previous;?></h4>
            <div class='well well-sm' style='min-width: 280px; max-height: 300px;overflow: auto;'>
        		<ul class='list-group checked-list-box'>
        			<?php foreach ($unterkategorien as $key => $value): ?>
        				<?php if($value->UNTERKATEGORIE_NR == $previous): ?>
	                  		<li class='kursor list-group-item li_unterkat<?=$previous;?> unchecked' id ='li_<?=$previous."_".$value->ID?>' name='li_<?=$previous."_".$value->ID?>'>
	                  			<span id= '<?="chk_".$previous."_".$value->ID?>' name='<?="chk_".$previous."_".$value->ID?>' data-column='ID_PARRENT' class='kursor chk_unterkat<?=$previous;?> glyphicon glyphicon-unchecked'></span>
	                  			<?=$value->TEXT?>
	                  		</li>
	                  	<?php endif; ?>
                  	<?php endforeach; ?>
                </ul>
            </div>
        </div>
    	<?php if ($tab == '3' ): ?>
        <div class='col-xs-3'>
            <h4>Unterkategorie 1</h4>
            <div class='well well-sm' style='min-width: 280px; max-height: 300px;overflow: auto;'>
        		<ul class='list-group checked-list-box'>
        			<?php foreach ($unterkategorien as $key => $value): ?>
        				<?php if($value->UNTERKATEGORIE_NR == '1'): ?>
	                  		<li class='kursor list-group-item li_unterkat9 unchecked' id ='li_9_<?=$value->ID?>' name='li_9_<?=$value->ID?>'>
	                  			<span id= 'chk_9_<?=$value->ID?>' name='chk_9_<?=$value->ID?>' data-column='U1_ID' class='kursor chk_unterkat9 glyphicon glyphicon-unchecked'></span>
	                  			<?=$value->TEXT?>
	                  		</li>
	                  	<?php endif; ?>
                  	<?php endforeach; ?>
                </ul>
            </div>
        </div>
        <?php endif; ?>
        <div class='col-xs-3'>
            <h4>Störungen</h4>
            <div class='well well-sm' style='min-width: 280px; max-height: 300px;overflow: auto;'>
        		<ul class='list-group checked-list-box'>
        			<?php foreach ($storungen as $key => $value): ?>
               		<li class='kursor list-group-item li_unterkat<?=$tab;?>_storungen unchecked' id ='li_<?=$tab."_storung_".$value->ID?>' name='li_<?=$tab."_storung_".$value->ID?>'>
               			<span id= 'chk_<?=$tab."_storung_".$value->ID?>' name='chk_<?=$tab."_storung_".$value->ID?>' data-column='ID_STORUNG' class='kursor chk_unterkat<?=$tab;?>_storungen glyphicon glyphicon-unchecked'></span>
               			<?=$value->NR?> - <?=$value->TEXT?>
               		</li>  		
                  	<?php endforeach; ?>
                </ul>
            </div>
        </div>
	</div>

	<div class='container'>
	<HR>
	<form class='form-inline' role='form' method='POST' id='form_new_unterkategorie<?=$tab;?>' name='form_new_unterkategorie<?=$tab;?>' action='storungen.new_unterkategorie'>
		<div class='col-xs-8'>
			<h4>Neu Unterkategorie <?=$tab;?></h4>
	    	<div class='input-group'>
	    		<span class='input-group-addon'>Name</span>
	    		<input type='text' class='form-control new_unterkat<?=$tab;?>' name='new_unterkat[<?=$tab;?>]' id='new_unterkat[<?=$tab;?>]' value='' style='min-width: 100px;'>
	    	</div>
	    	
		</div>
	</form>
	</div>
	<BR>
	<div class='container'>
	    <div class='col-xs-2'>
	       	<div><button type="button" name='submit_form_new_unterkategorie<?=$tab;?>' id='submit_form_new_unterkategorie<?=$tab;?>' class="btn btn-success">Daten holen</button></div>
	    </div>
	</div>
</div>
<!-- /.TAB3/4/5 - Unterkategorie 2/3/4 -->
<?php endif; ?>

<!-- fade unchecked checkboxes  on document load -->
<script type="text/javascript">
	$(document).ready(function() {
    	$('.unchecked').fadeTo(300, 0.5);
	});
</script>
<!-- fade unchecked checkboxes  on document load -->

<!-- define global variables -->
<script type="text/javascript">
	var tab = <?=$tab;?>;
	var previous = <?=$previous;?>;
	var storungen= <?= json_encode($storungen);?>;
	var unterkategorien= <?= json_encode($unterkategorien);?>;
</script>
<!-- /.define global variables -->

<!-- define global funcions -->
<script type="text/javascript">
	//case insensitive text search extension for jQuery
	$.expr[":"].icontains = $.expr.createPseudo(function(arg) {
  		return function( elem ) {
   			return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
  		};
	});
	
	function selectedItem(id, className){
		$(className+'.selected').css('background-color','#fff');
		$(className+'.selected').removeClass('selected');
		$('#'+id+className).css('background-color','#337ab7');
		$('#'+id+className).addClass('selected');
	};

	function setHalleLinie(array){
		for(i=3; i<=5; i++){
			halle = 'H'+i+'_A';
			if(array[halle]=='1'){
				$('#chk_'+halle).removeClass('glyphicon-unchecked');
				$('#chk_'+halle).addClass('glyphicon-check');
				$('#li_'+halle).fadeTo(300, 1);
			}
			else{
				$('#chk_'+halle).removeClass('glyphicon-check');
				$('#chk_'+halle).addClass('glyphicon-unchecked');
				$('#li_'+halle).fadeTo(300, 0.5);
				//$('.linien_chk').prop('onclick','return false;')
			};
			for(j=0; j<=2; j++){
				linie = 'H'+i+'L'+j;
				if(array[linie]=='1'){
					$('#chk_'+linie).removeClass('glyphicon-unchecked');
					$('#chk_'+linie).addClass('glyphicon-check');
					$('#li_'+linie).fadeTo(300, 1);
				}
				else{
					$('#li_'+linie).fadeTo(300, 0.5);
					$('#chk_'+linie).removeClass('glyphicon-check');
					$('#chk_'+linie).addClass('glyphicon-unchecked');
				};
			};
		};
		halle ='';
		linie ='';
	};

	function returnActive (id, my_this, className){
		//console.log(my_this);
		if(my_this.hasClass('glyphicon-check')){
			my_this.removeClass('glyphicon-check');
			my_this.addClass('glyphicon-unchecked');
			$('#'+id+className).fadeTo(300, 0.5);
			return active='0';
		}
		else{
			my_this.removeClass('glyphicon-unchecked');
			my_this.addClass('glyphicon-check');
			$('#'+id+className).fadeTo(300, 1);
			return active='1';
		};
	};


	function resetHelperWells (tab, previous){
		$('.selected').css('background-color','#fff');
		
		if(Number(tab)==1){
			$('.li_unterkat1_storungen').each(function(){
				$(this).fadeTo(300, 0.5);
			});
			$('.chk_unterkat1_storungen').each(function(){
				$(this).removeClass('glyphicon-check');
				$(this).addClass('glyphicon-unchecked');
			});
		}

		$('.'+tab+'_li_halle').each(function(){
			$(this).fadeTo(300, 0.5);
		});

		$('.'+tab+'_li_linien').each(function(){
			$(this).fadeTo(300, 0.5);
		});
		
		$('.unterkat'+tab+'_storungen_checkbox').each(function(){
			$(this).removeClass('glyphicon-check');
			$(this).addClass('glyphicon-unchecked');
		});
		
		$('.'+tab+'_halle_chk').each(function(){
			$(this).removeClass('glyphicon-check');
			$(this).addClass('glyphicon-unchecked');
		});

		$('.'+tab+'_linien_chk').each(function(){
			$(this).removeClass('glyphicon-check');
			$(this).addClass('glyphicon-unchecked');
		});

		if(Number(tab)>1){
			$('.li_unterkat'+tab+'_storungen').each(function(){
				$(this).fadeTo(300, 0.5);
			});
			$('.chk_unterkat'+tab+'_storungen').each(function(){
				$(this).removeClass('glyphicon-check');
				$(this).addClass('glyphicon-unchecked');
			});

		}
		if (Number(previous) >= 1){
			$('.li_unterkat'+previous).each(function(){
				$(this).fadeTo(300, 0.5);
			});
			$('.chk_unterkat'+previous).each(function(){
				$(this).removeClass('glyphicon-check');
				$(this).addClass('glyphicon-unchecked');
			});
		}
		if (Number(previous) >= 2){
			$('.li_unterkat9').each(function(){
				$(this).fadeTo(300, 0.5);
			});
			$('.chk_unterkat9').each(function(){
				$(this).removeClass('glyphicon-check');
				$(this).addClass('glyphicon-unchecked');
			});
		}

		
	}
	
	function UpdateArray(array, id, column, newValue) {
		for (var i in array) {
	    	if (array[i].ID == id) {
	    		my_array = array[i];
	        	my_array[column] = newValue;
	        	break;
	    	};
		};
	};

	function filteredArray(array, id){
		filtered = array.filter(function (v) {
                        return v.ID == id;
                    });
		return filtered[0];
	};

	function ajaxCall (type, url, table, id, active, column){
		$.ajax({
			type:		type,
			url:		url,
			data:		{table: table, id: id, active: active, column: column},
			success:	function(){ },
			error:		function(){alert('error');}
		});
	}
</script>
<!-- /.define global funcions -->

<!-- TAB1 - Storungen Controllers -->
<script type="text/javascript">
if (Number(tab)==0){
	$('.li_storungen').click(function(){
		selectedItem(this.id, '.li_storungen');
		array= filteredArray(storungen, this.id.substr(2));
		setHalleLinie(array);
	});

	$('.storung_chk').click(function(){
		active = returnActive(this.id.substr(4), $(this), '.li_storungen');
		ajaxCall('POST', 'storungen.active', 'STR_STORUNG', this.id.substr(6), active, 'ACTIVE');
	});

	$('.halle_chk, .linien_chk').click(function(){
		storung_id= $('.selected').attr('id').substr(2);
		active = returnActive('li_'+this.id.substr(4), $(this), '');

		UpdateArray(storungen, storung_id, this.id.substr(4), active);

		ajaxCall('POST', 'storungen.active', 'STR_STORUNG', storung_id, active, this.id.substr(4));
	});

	$('#submit_form_new_storung').click(function(){
		nr=   $('#new_storung_nr').val();
		text= $('#new_storung').val();
        num=  $(".li_storungen:contains("+nr+")").length;
        num1= $(".li_storungen:icontains("+text+")").length;
        if (num > 0 || num1 >0) {
        	alert ('Storung ist bereits vorhanden!');
        }
        else{
        	$('#form_new_storung').submit();
        };
    });
}
</script>
<!-- /.TAB1 - Storungen Controllers -->

<!-- TAB2 - Unterkategorie 1 Controllers -->
<script type="text/javascript">
if (Number(tab) == 1){
	$('.li_unterkat1').click(function(){
		resetHelperWells (tab);
		selectedItem(this.id, '.li_unterkat1');
		
		filtered= filteredArray(unterkategorien, this.id.substr(5));
		linie_array = filtered.LINIE_ARRAY;
		halle_array = filtered.HALLE;
		
		$.each(linie_array.split('|').slice(1,-1), function(index,item) {
			if (item == 1){
				$('#li_'+tab+'_L'+index).fadeTo(300, 1);
				$('#chk_'+tab+'_L'+index).removeClass('glyphicon-unchecked');
				$('#chk_'+tab+'_L'+index).addClass('glyphicon-check');
			};
		});
		$.each(halle_array.split('|').slice(1,-1), function(index,item) {
			$('#li_'+tab+'_H'+item).fadeTo(300, 1);
			$('#chk_'+tab+'_H'+item).removeClass('glyphicon-unchecked');
			$('#chk_'+tab+'_H'+item).addClass('glyphicon-check');
		});

		parrent_ids = filtered.ID_PARRENT;
		array = [];
		$.each(parrent_ids.split('|').slice(1,-1), function(index,item) {
    		array.push(item); 
		});
		$.each(array, function(key, value){
			$('#li_'+tab+'_storung_'+value).fadeTo(300, 1);
			$('#chk_'+tab+'_storung_'+value).removeClass('glyphicon-unchecked');
			$('#chk_'+tab+'_storung_'+value).addClass('glyphicon-check');
        });
	});
	
	$('.chk_unterkat1').click(function(){
		active = returnActive('li_'+this.id.substr(4), $(this), '.li_unterkat1');
		ajaxCall('POST', 'storungen.active', 'STR_UNTERKATEGORIE', this.id.substr(6), active, 'ACTIVE');
	});

	$('.1_linien_chk, .1_halle_chk').click(function(){
		unterkategorie_id= $('.selected').attr('id').substr(5);
		returnActive('li_'+this.id.substr(4), $(this), '');
		column = $(this).data('column');
		active='|';
		if (column == 'HALLE'){
			pocetak = 3;
			kraj = 5;
		}
		else{
			pocetak = 0;
			kraj = 2;
		}

		for (i=pocetak; i<=kraj; i++){
			if (column == 'HALLE'){
				if ($('#chk_1_H'+i).hasClass('glyphicon-check') ){active = active + i +'|';};
			}
			else{
				if ($('#chk_1_L'+i).hasClass('glyphicon-check') )
					{active = active +'1|';}
				else
					{active = active +'0|';};
			};
		}
		UpdateArray(unterkategorien, unterkategorie_id, column, active);
		ajaxCall('POST', 'storungen.active', 'STR_UNTERKATEGORIE', unterkategorie_id, active, column);
	});

	$('.li_unterkat1_storungen').click(function(){
		selectedItem(this.id, '.li_unterkat1_storungen');
	});
	
	$('.chk_unterkat1_storungen').click(function(){
		unterkategorie_id= $('.selected').attr('id').substr(5);
		returnActive('li_'+this.id.substr(4), $(this), '');
		column = $(this).data('column');
		active = '|';
		$('.chk_unterkat1_storungen').each(function() {
    		var currentElement = $(this);

    		if (currentElement.hasClass('glyphicon-check')){
    			active = active + currentElement.attr('id').substr(14) + '|';
    		};
		});
		UpdateArray(unterkategorien, unterkategorie_id, column, active);
		ajaxCall('POST', 'storungen.active', 'STR_UNTERKATEGORIE', unterkategorie_id, active, column);
	});

	$('#submit_form_new_unterkategorie1').click(function(){
		text = $('.new_unterkat1').val();
		num= $(".li_unterkat1:icontains("+text+")").length;
        if (num > 0) {
        	alert ('Unterkategorie ist bereits vorhanden!');
        }
        else{
        	$('#form_new_unterkategorie1').submit();
        };
    });
}
</script>
<!-- /.TAB2 - Unterkategorie 1 Controllers -->

<!-- TAB3/4/5 - Unterkategorie 2/3/4 Controllers -->
<script type="text/javascript">
if(Number(tab)>=2){
	$('.li_unterkat'+tab).click(function(){
		resetHelperWells (tab, previous);
		selectedItem(this.id, '.li_unterkat'+tab);
		
		filtered= filteredArray(unterkategorien, this.id.substr(5));
		//filteredPrevious = filteredArray(unterkategorien, this.id.substr(5));
		//filteredU1 = filteredArray(unterkategorien, 1);
		linie_array = filtered.LINIE_ARRAY;
		halle_array = filtered.HALLE;
		
		$.each(linie_array.split('|').slice(1,-1), function(index,item) {
			if (item == 1){
				$('#li_'+tab+'_L'+index).fadeTo(300, 1);
				$('#chk_'+tab+'_L'+index).removeClass('glyphicon-unchecked');
				$('#chk_'+tab+'_L'+index).addClass('glyphicon-check');
			};
		});
		
		$.each(halle_array.split('|').slice(1,-1), function(index,item) {
			$('#li_'+tab+'_H'+item).fadeTo(300, 1);
			$('#chk_'+tab+'_H'+item).removeClass('glyphicon-unchecked');
			$('#chk_'+tab+'_H'+item).addClass('glyphicon-check');
		});

		parrent_ids = filtered.ID_PARRENT;
		array = [];
		$.each(parrent_ids.split('|').slice(1,-1), function(index,item) {
    		array.push(item); 
		});
		$.each(array, function(key, value){
			$('#li_'+previous+'_'+value).fadeTo(300, 1);
			$('#chk_'+previous+'_'+value).removeClass('glyphicon-unchecked');
			$('#chk_'+previous+'_'+value).addClass('glyphicon-check');
        });

        if (Number(tab) == 3){
        	u1_ids = filtered.U1_ID;
			array = [];
			$.each(u1_ids.split('|').slice(1,-1), function(index,item) {
	    		array.push(item); 
			});
			$.each(array, function(key, value){
				$('#li_9_'+value).fadeTo(300, 1);
				$('#chk_9_'+value).removeClass('glyphicon-unchecked');
				$('#chk_9_'+value).addClass('glyphicon-check');
	        });
        };

        storung_ids = filtered.ID_STORUNG;
		array = [];
		$.each(storung_ids.split('|').slice(1,-1), function(index,item) {
    		array.push(item); 
		});
		$.each(array, function(key, value){
			$('#li_'+tab+'_storung_'+value).fadeTo(300, 1);
			$('#chk_'+tab+'_storung_'+value).removeClass('glyphicon-unchecked');
			$('#chk_'+tab+'_storung_'+value).addClass('glyphicon-check');
        });
	});
	
	$('.chk_unterkat'+tab).click(function(){
		active = returnActive('li_'+this.id.substr(4), $(this), '.li_unterkat'+tab);
		ajaxCall('POST', 'storungen.active', 'STR_UNTERKATEGORIE', this.id.substr(6), active, 'ACTIVE');
	});

	$('.'+tab+'_linien_chk, .'+tab+'_halle_chk').click(function(){
		unterkategorie_id= $('.selected').attr('id').substr(5);
		returnActive('li_'+this.id.substr(4), $(this), '');
		column = $(this).data('column');
		active='|';
		if (column == 'HALLE'){
			pocetak = 3;
			kraj = 5;
		}
		else{
			pocetak = 0;
			kraj = 2;
		}

		for (i=pocetak; i<=kraj; i++){
			if (column == 'HALLE'){
				if ($('#chk_'+tab+'_H'+i).hasClass('glyphicon-check') ){active = active + i +'|';};
			}
			else{
				if ($('#chk_'+tab+'_L'+i).hasClass('glyphicon-check') )
					{active = active +'1|';}
				else
					{active = active +'0|';};
			};
		}
		UpdateArray(unterkategorien, unterkategorie_id, column, active);
		ajaxCall('POST', 'storungen.active', 'STR_UNTERKATEGORIE', unterkategorie_id, active, column);
	});

	$('.li_unterkat'+previous).click(function(){
		selectedItem(this.id, '.li_unterkat'+previous);
	});
	
	$('.chk_unterkat'+previous).click(function(){
		unterkategorie_id= $('.selected').attr('id').substr(5);

		returnActive('li_'+this.id.substr(4), $(this), '');
		column = $(this).data('column');
		active = '|';
		$('.chk_unterkat'+previous).each(function() {
    		var currentElement = $(this);

    		if (currentElement.hasClass('glyphicon-check')){
    			active = active + currentElement.attr('id').substr(6) + '|';
    		};
		});
		UpdateArray(unterkategorien, unterkategorie_id, column, active);
		ajaxCall('POST', 'storungen.active', 'STR_UNTERKATEGORIE', unterkategorie_id, active, column);
	});

	$('.li_unterkat9').click(function(){
		selectedItem(this.id, '.li_unterkat9');
	});
	
	$('.chk_unterkat9').click(function(){
		unterkategorie_id= $('.selected').attr('id').substr(5);

		returnActive('li_'+this.id.substr(4), $(this), '');
		column = $(this).data('column');
		active = '|';
		$('.chk_unterkat9').each(function() {
    		var currentElement = $(this);

    		if (currentElement.hasClass('glyphicon-check')){
    			active = active + currentElement.attr('id').substr(6) + '|';
    		};
		});
		UpdateArray(unterkategorien, unterkategorie_id, column, active);
		ajaxCall('POST', 'storungen.active', 'STR_UNTERKATEGORIE', unterkategorie_id, active, column);
	});

	$('.li_unterkat'+tab+'_storungen').click(function(){
		selectedItem(this.id, '.li_unterkat'+tab+'_storungen');
	});
	
	$('.chk_unterkat'+tab+'_storungen').click(function(){
		unterkategorie_id= $('.selected').attr('id').substr(5);

		returnActive('li_'+this.id.substr(4), $(this), '');
		column = $(this).data('column');
		active = '|';
		$('.chk_unterkat'+tab+'_storungen').each(function() {
    		var currentElement = $(this);

    		if (currentElement.hasClass('glyphicon-check')){
    			active = active + currentElement.attr('id').substr(14) + '|';
    		};
		});
		UpdateArray(unterkategorien, unterkategorie_id, column, active);
		ajaxCall('POST', 'storungen.active', 'STR_UNTERKATEGORIE', unterkategorie_id, active, column);
	});

	$('#submit_form_new_unterkategorie'+tab).click(function(){
		text = $('.new_unterkat'+tab).val();
		num= $('.li_unterkat'+tab+':icontains('+text+')').length;
        if (num > 0) {
        	alert ('Unterkategorie ist bereits vorhanden!');
        }
        else{
        	$('#form_new_unterkategorie'+tab).submit();
        };
    });
};
</script>
<!-- /.TAB3/4/5 - Unterkategorie 2/3/4 Controllers -->




<!-- submit Storungen/Unterkategorie tabulator -->
<script type="text/javascript">
    $('.tab').click(function(){
        $('#tab').val($(this).data('key'));
        $('#form_tab').submit();
    });
</script>
<!-- /.submit Storungen/Unterkategorie tabulator -->

<?php require ('app/views/partials/footer_vendor_scripts.php'); ?>
<?php require ('app/views/partials/footer.php'); ?>