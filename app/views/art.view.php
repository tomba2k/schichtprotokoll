<?php require ('app/views/partials/head_admin.php'); ?>

<form method="POST" role="form" id="form_saveArt" name="form_saveArt" action="art">
<div class='container'>
    <div class='row'>
    <h3>Admin - Art</h3>
    <div class='col-xs-2'>
        <div class='input-group art' name='art_group' id='art_group'>
            <span class='input-group-addon'>Art</span>
            <input type='text' class='form-control' id='art' name='art'>
        </div>
    </div>
    <div class='form-group col-xs-2'>
        <div class='input-group active' id='active_group' name='active_group'>
            <span class='input-group-addon'>Aktiv</span>
            <select class='selectpicker' data-width='100%' id='active' name='active'>
                <option value='0'>Nein</option>
                <option value='1'>Ja</option>
            </select>
        </div>
    </div>
    </div>
    <div class='row'>
    <div class="form-group col-xs-3">
        <button type="submit" class="btn btn-info" id="submit_saveArt" name="submit_saveArt">Speichern</button>
    </div>
    </div>
</div>
</form>

<div class='container'><HR></div>

<div class='container'>
<div class='table-responsive'>
<table id='myDataTable' class='table table-striped table-bordered table-hover schichten' style='cellspacing: 0; width: 30%;'>
<thead>
	<tr>
		<th>Id</th>
        <th>Art</th>
        <th>Aktiv</th>
        <th></th>
	</tr>
</thead>
<tbody>
    <?php foreach ($arts as $art): ?>
    <tr>
        <?php foreach ($art as $key => $value): ?>
            <td class='<?= $key.'_'.$value;?>' id='<?= $key;?>'><?=$value ?></td>
        <?php endforeach; ?>
        <td><button type='button' name='btn_editArt' id='btn_editArt' class='btn btn-xs btn-success btn_editArt' value='edit-art'>Edit</button></td>
    </tr>
    <?php endforeach; ?>
</tbody>
</table>
</div>
</div>

<!-- Art change modal -->
<div class='modal fade' id='modal_editArt' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
    <div class='modal-dialog'>
        <div class='modal-content'>
            <div class='modal-header'>
                <h4>Art �ndern</h4>
            </div>
            <form method="POST" role="form" id="form_updateArt" name="form_updateArt" action="art.edit">
            <div class='modal-body'>
                <div class='container'>
                    <input type='hidden' id='modal_id' name='modal_id' value='' />
                    <table>
                        <tr>
                            <td>
                            <div class='input-group' name='modal_art_addon' id='modal_art_addon'>
                                <span class='input-group-addon'>Art</span>
                                <input type='text' id='modal_art' name='modal_art' value='' />
                            </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <div class='input-group' name='modal_active_addon' id='modal_active_addon'>
                                <span class='input-group-addon'>Aktiv</span>
                                <select class='selectpicker' data-width='100%' id='modal_active' name='modal_active'>
                                    <option value='0'>Nein</option>
                                    <option value='1'>Ja</option>
                                </select>
                            </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class='modal-footer'>
                <button type='button' class='btn btn-default' data-dismiss='modal'>Abbruch</button>
                <button type='submit' class='btn btn-info' id='submit_updateArt' name='submit_updateArt'>Speichern</button>
            </div>
            </form>
         </div>
    </div>
</div>
<!-- /Art change modal -->

<!-- activate dataTable -->
<script type='text/javascript'>
    $(document).ready(function() {
        $('#myDataTable').DataTable({
            searching: false,
            paging: false,
            info: false
        });

        $('.ACTIVE_0').parent().css('background-color', '#FDE3A7');
    });
</script>
<!-- /activate dataTable -->

<!-- set values for modal window -->
<script type='text/javascript'>
    $('.btn_editArt').click(function(){
        $('#modal_editArt').modal('show');

        var id = $(this).closest('tr').children('#ID').text();
        var art = $(this).closest('tr').children('#TEXT').text();
        var active = $(this).closest('tr').children('#ACTIVE').text()*1;

        $('#modal_id').val(id);
        $('#modal_art').val(art);
        $('#modal_active').selectpicker('val', active);
    });
</script>
<!-- /set values for modal window -->

<?php require ('app/views/partials/footer_vendor_scripts.php'); ?>
<?php require ('app/views/partials/footer.php'); ?>

