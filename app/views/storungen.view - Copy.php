<?php require ('app/views/partials/head_admin.php'); ?>

<div class='container'>
	<h3>Störungen</h3>
</div>

<!-- Storungen/Unterkategorie tabulator -->
<div class='row'>
<div class='container'> 
	<form method='POST' role='form' id='form_tabulator' name='form_tabulator' action='storungen'>
		<input type='hidden' name='tabulator' id='tabulator' value='<?= $tabulator;?>'>
	    <ul class='nav nav-pills'>
	        <li id='li_storung' name='li_storung' data-key ='0' class='tabulator li_storung <?php if ($tabulator == 0){echo "active";} ?>'><a href='#tab_storung' data-toggle='tab'>Störungen</a></li>
	        <?php for ($i=1; $i<=4; $i++): ?>
	        	<li id='li_unterkategorie<?=$i;?>' name='li_unterkategorie<?=$i;?>' data-key ='<?=$i;?>' class='tabulator li_unterkategorien <?php if ($tabulator == $i){echo "active";} ?>'><a href='#tab_unterkategorie<?=$i;?>' data-toggle='tab'>Unterkategorie <?=$i;?></a></li>
	    	<?php endfor; ?>
	    </ul>
	</form>
</div>
</div>

<!--/.Storungen/Unterkategorie tabulator -->

<!-- TAB1 - Storungen -->
<?php $display = ($tabulator == '0') ?'block' :'none'; ?>
<div class='row' style='display: <?=$display;?>'>
	<div class='container'>
        <div class='col-xs-3'>
            <h4>Störungen</h4>

            <div class='well' style='min-width: 280px; max-height: 300px;overflow: auto;'>
        		<ul class='list-group checked-list-box'>
        			<?php foreach ($storungen as $key => $value): ?>
        				<?php $checked = ($value->ACTIVE == '1') ? 'check' : 'unchecked' ; ?>
                  		<li class='kursor list-group-item <?=$checked;?> li_storungen' id ='<?=$tabulator."_".$value->ID?>' name='<?=$tabulator."_".$value->ID?>' data-key='ACTIVE' >
                  			<span id= '<?=$tabulator."_icon_".$value->ID?>' name='<?=$tabulator."_icon_".$value->ID?>' data-key='ACTIVE' class='kursor storung_checkbox glyphicon glyphicon-<?=$checked;?>'></span>
                  			<?=$value->NR?> - <?=$value->TEXT?>
                  		</li>  		
                  	<?php endforeach; ?>
                </ul>
            </div>
        </div>
        <div class='col-xs-4'>
        	<h4>Hallen und Linien active status</h4>
        	<?php for($i=3; $i<=5; $i++): ?>
        	<div class='col-xs-4'>
        		<ul class=''>
        			<li class='kursor li_halle' id ='li_H<?=$i;?>_A' name='li_H<?=$i;?>_A' data-key='H<?=$i;?>_A' >
        				<h5><span id= 'H<?=$i;?>_A' name='H<?=$i;?>_A' data-key= 'H<?=$i;?>_A' class='kursor halle_checkbox glyphicon glyphicon-unchecked'></span><strong>Halle <?=$retVal=($i==5) ? '4A' : $i;?></strong></h5>
        			</li>
        			<?php for($j=0; $j<=2; $j++): ?>
        			<li class='kursor li_linien' id ='li_H<?=$i;?>L<?=$j;?>' name='li_H<?=$i;?>L<?=$j;?>' data-key='H<?=$i;?>L<?=$j;?>' >
	    				<span id= 'H<?=$i;?>L<?=$j;?>' name='H<?=$i;?>L<?=$j;?>' data-key= 'H<?=$i;?>L<?=$j;?>' class='kursor linien_checkbox glyphicon glyphicon-unchecked'></span>
	    				<label>Linie <?=$j;?></label>
	    			</li>
	    			
	    			<?php endfor; ?>
	    		</ul>
	    	</div>
	    	<?php endfor; ?>
    	</div>
    </div>
    
    <div class='container'>
    <HR>
    <form class='form-inline' role='form' method='POST' id='form_new_storung' name='form_new_storung' action='storungen.new_storung'>
    	<div class='col-xs-8'>
    		<h4>Neu Störung</h4>
    		<div class='col-xs-2' style='padding-left: 0px;'>
    		<div class='input-group'>
	            <span class='input-group-addon' style='min-width: 30px;'>NR</span>
	            <input type='text' class='form-control' name='new_storung_nr' id='new_storung_nr' value='' style='min-width: 60px;'>
	        	</div>
        	</div>
        	<div class='col-xs-6'>
        	<div class='input-group'>
        		<span class='input-group-addon'>Storung</span>
        		<input type='text' class='form-control' name='new_storung' id='new_storung' value='' style='min-width: 100px;'>
        	</div>
        	</div>
    	</div>
    </form>
	</div>
    <BR>
    
    <div class='container'>
    <div class='col-xs-2'>
       	<div><button type="button" name='submit_form_new_storung' id='submit_form_new_storung' class="btn btn-success">Daten holen</button></div>
    </div>
    </div>
</div>
<!-- /.TAB1 - Storungen -->

<!-- TAB2 - Unterkategorie 1 -->
<?php $display = ($tabulator == '1') ?'block' :'none'; ?>
<div class='row' style='display: <?=$display;?>'>
	<div class='container'>
		<div class='col-xs-3'>
            <h4>Unterkategorie 1</h4>

            <div class='well' style='min-width: 280px; max-height: 300px;overflow: auto;'>
        		<ul class='list-group checked-list-box'>
        			<?php foreach ($unterkategorien as $key => $value): ?>
        				<?php if($value->UNTERKATEGORIE_NR == '1'): ?>
	        				<?php $checked = ($value->ACTIVE == '1') ? 'check' : 'unchecked' ; ?>
	                  		<li class='kursor list-group-item <?=$checked;?> li_unterkat' id ='<?=$tabulator."_".$value->ID?>' name='<?=$tabulator."_".$value->ID?>' data-key='ACTIVE' >
	                  			<span id= '<?=$tabulator."_.icon_".$value->ID?>' name='<?=$tabulator."_icon_".$value->ID?>' data-key='ACTIVE' class='kursor untekat_checkbox glyphicon glyphicon-<?=$checked;?>'></span>
	                  			<?=$value->TEXT?>
	                  		</li>
	                  	<?php endif; ?>
                  	<?php endforeach; ?>
                </ul>
            </div>
        </div>
        <div class='col-xs-2'>
        	<h4>Linien active status</h4>

       		<ul class=''>
       			<?php for($j=0; $j<=2; $j++): ?>
       			<li class='kursor li_unterkat_linien unchecked' id ='li_linie_<?=$j;?>' name='li_linie_<?=$j;?>' data-key='<?=$j;?>' >
    				<span id= 'linie_<?=$j;?>' name='linie_<?=$j;?>' data-key= '<?=$j;?>' class='kursor unterkat_linien_checkbox glyphicon glyphicon-unchecked'></span>
    				<label>Linie <?=$j;?></label>
    			</li>
    			<?php endfor; ?>
    		</ul>
    	</div>
        <div class='col-xs-3'>
            <h4>Störungen</h4>

            <div class='well' style='min-width: 280px; max-height: 300px;overflow: auto;'>
        		<ul class='list-group checked-list-box'>
        			<?php foreach ($storungen as $key => $value): ?>
               		<li class='kursor list-group-item li_unterkat_storungen' id ='<?=$tabulator."_".$value->ID?>' name='<?=$tabulator."_".$value->ID?>' data-key='ID_PARRENT' >
               			<span id= '<?="icon_".$value->ID?>' name='<?="icon_".$value->ID?>' data-key='ID_PARRENT' class='kursor unterkat_storungen_checkbox glyphicon glyphicon-unchecked'></span>
               			<?=$value->NR?> - <?=$value->TEXT?>
               		</li>  		
                  	<?php endforeach; ?>
                </ul>
            </div>
        </div>
	</div>

	<div class='container'>
	<HR>
	<form class='form-inline' role='form' method='POST' id='form_new_unterkategorie' name='form_new_unterkategorie' action='storungen.new_unterkategorie'>
		<div class='col-xs-8'>
			<h4>Neu Unterkategorie 1</h4>
			
	    	<div class='input-group'>
	    		<span class='input-group-addon'>Name</span>
	    		<input type='text' class='form-control' name='new_unterkat' id='new_unterkat' value='' style='min-width: 100px;'>
	    	</div>
	    	
		</div>
	</form>
	</div>
	<BR>
	<div class='container'>
	    <div class='col-xs-2'>
	       	<div><button type="button" name='submit_form_new_unterkategorie' id='submit_form_new_unterkategorie' class="btn btn-success">Daten holen</button></div>
	    </div>
	</div>
</div>

<!-- /.TAB2 - Unterkategorie 1 -->


<script type="text/javascript">
    $('.tabulator').click(function(){
        $('#tabulator').val($(this).data('key'));
        $('#form_tabulator').submit();
    });
</script>

<script type="text/javascript">
	$(document).ready(function() {
    	$('.unchecked').fadeTo(300, 0.5);
	});

	var id='';
	var col_name='';
	var active='';
	var storungen= <?= json_encode($storungen);?>;
	var unterkategorien= <?= json_encode($unterkategorien);?>;

	console.log(storungen[0]);

	// ------ FUNCTION DEFINITIONS ------ 
	function defaultClasses (id, className){
		$(className+'.selected').css('background-color','#fff');
		$(className+'.selected').removeClass('selected');
		$('#'+id+className).css('background-color','#337ab7');
		$('#'+id+className).addClass('selected');
	};

	function halleLinieClasses(array){
		for(i=3; i<=5; i++){
			halle = 'H'+i+'_A';
			if(array[halle]=='1'){
				$('#'+halle).removeClass('glyphicon-unchecked');
				$('#'+halle).addClass('glyphicon-check');
				$('#li_'+halle).fadeTo(300, 1);
			}
			else{
				$('#'+halle).removeClass('glyphicon-check');
				$('#'+halle).addClass('glyphicon-unchecked');
				$('#li_'+halle).fadeTo(300, 0.5);
			};
			for(j=0; j<=2; j++){
				linie = 'H'+i+'L'+j;
				if(array[linie]=='1'){
					$('#'+linie).removeClass('glyphicon-unchecked');
					$('#'+linie).addClass('glyphicon-check');
					$('#li_'+linie).fadeTo(300, 1);
				}
				else{
					$('#li_'+linie).fadeTo(300, 0.5);
					$('#'+linie).removeClass('glyphicon-check');
					$('#'+linie).addClass('glyphicon-unchecked');
				};

			};
		};
	};

	function returnActive (id, my_this, className){
		//console.log(my_this);
		if(my_this.hasClass('glyphicon-check')){
			my_this.removeClass('glyphicon-check');
			my_this.addClass('glyphicon-unchecked');
			$('#'+id+className).fadeTo(300, 0.5);
			return active='0';
		}
		else{
			my_this.removeClass('glyphicon-unchecked');
			my_this.addClass('glyphicon-check');
			$('#'+id+className).fadeTo(300, 1);
			return active='1';
		};
	};
	
	function UpdateArray(array, id, column, newValue) {
		for (var i in array) {
	    	if (array[i].ID == id) {
	    		my_array = array[i];
	        	my_array[column] = newValue;
	        	break;
	    	};
		};
	};

	function filteredArray(array, id){
		filtered = array.filter(function (v) {
                        return v.ID == id;
                    });
		return filtered[0];
	};

	function ajaxCall (type, url, table, id, active, column){
		$.ajax({
			type:		type,
			url:		url,
			data:		{table: table, id: id, active: active, column: column},
			success:	function(){ },
			error:		function(){alert('error');}
		});
	}
	// ------ /.FUNCTION DEFINITIONS ------ 

	// ------ TAB1 - Storungen Controllers ------
	$('.storung_checkbox').click(function(){
		my_this = $(this);
		id= this.id.substring(5);
		defaultClasses(id, '.li_storungen');
		
		id= $('.selected').attr('id');
		column = my_this.data('key');

		array= filteredArray(storungen, id);
		halleLinieClasses(array);
		active = returnActive(id, my_this,'.li_storungen');

		ajaxCall('POST', 'storungen.active', 'STR_STORUNG', id, active, column);
	});

	$('.li_storungen').click(function(){
		my_this = $(this);
		id= $('.selected').attr('id');
		li_id= this.id;
		
		defaultClasses(li_id, '.li_storungen');
		array= filteredArray(storungen, li_id);
		halleLinieClasses(array);
	});

	$('.halle_checkbox, .linien_checkbox').click(function(){
		my_this = $(this);
		id= $('.selected').attr('id');
		column = my_this.data('key');
		li_id= 'li_'+this.id;
		active = returnActive(li_id, my_this, '');
		//alert(active);
		UpdateArray(storungen, id, column, active);

		ajaxCall('POST', 'storungen.active', 'STR_STORUNG', id, active, column);
	});

	$('#submit_form_new_storung').click(function(){
		nr=   $('#new_storung_nr').val()
		text= $('#new_storung').val();
        num=  $(".li_storungen:contains("+nr+")").length;
        num1= $(".li_storungen:contains("+text+")").length;
        if (num > 0 || num1 >0) {
        	alert ('This Storung allready exists!');
        }
        else{
        	$('#form_new_storung').submit();
        };
    });
    // ------ /.TAB1 - Storungen Controllers ------

    // ------ /.TAB2 - Unterkategorie 1 Controllers ------
    $('.untekat_checkbox').click(function(){
		my_this = $(this);
		id= this.id.substring(5);
		defaultClasses(id, '.li_unterkat');

		id= $('.selected').attr('id');
		column = my_this.data('key');

		active = returnActive(id, my_this,'.li_unterkat');

		ajaxCall('POST', 'storungen.active', 'STR_UNTERKATEGORIE', id, active, column);
	});
	$('.li_unterkat').click(function(){
		li_id= this.id;
		defaultClasses(li_id, '.li_unterkat');
		my_this = $(this);
		id= $('.li_unterkat.selected').attr('id');
		
		$('.li_unterkat_storungen.selected').css('background-color','#fff');
		$('.li_unterkat_storungen.selected').removeClass('selected');
		$('.li_unterkat_linien').fadeTo(300, 0.5);
		$('.unterkat_linien_checkbox').removeClass('glyphicon-check');
		$('.unterkat_linien_checkbox').addClass('glyphicon-unchecked');
		$('.li_unterkat_storungen').each(function(){
			$(this).fadeTo(300, 0.5);
		});
		$('.unterkat_storungen_checkbox').each(function(){
			$(this).removeClass('glyphicon-check');
			$(this).addClass('glyphicon-unchecked');
		});

		filtered = filteredArray(unterkategorien, id);
		linie_array = filtered.LINIE_ARRAY;
		$.each(linie_array.split('|').slice(1,-1), function(index,item) {
			if (item == 1){
				$('#li_linie_'+index).fadeTo(300, 1);
				$('#linie_'+index).removeClass('glyphicon-unchecked');
				$('#linie_'+index).addClass('glyphicon-check');
			};
		});

		filtered = filteredArray(unterkategorien, li_id);
		parrent_ids = filtered.ID_PARRENT;
		array = [];
		$.each(parrent_ids.split('|').slice(1,-1), function(index,item) {
    		array.push(item); 
		});

		$.each(array, function(key, value){
			$('#'+value+'.li_unterkat_storungen').fadeTo(300, 1);
			$('#icon_'+value+'.unterkat_storungen_checkbox').removeClass('glyphicon-unchecked');
			$('#icon_'+value+'.unterkat_storungen_checkbox').addClass('glyphicon-check');
        });
	});

	$('.li_unterkat_storungen').click(function(){
		my_this = $(this);
		li_id= this.id;
		id= $('.li_unterkat.selected').attr('id');
		defaultClasses(li_id,'.li_unterkat_storungen');

		console.log(filtered);
	});

	$('.unterkat_storungen_checkbox').click(function(){
		my_this = $(this);
		id= this.id.substring(5);
		defaultClasses(id, '.li_unterkat_storungen');

		id= $('.selected').attr('id');
		column = my_this.data('key');

		active = returnActive(id, my_this, '.li_unterkat_storungen');

		//ajaxCall('POST', 'storungen.active', 'STR_UNTERKATEGORIE', id, active, column);
	});

	$('#submit_form_new_unterkategorie').click(function(){
		text = $('#new_unterkat').val();
		num= $(".li_unterkat:contains("+text+")").length;
        if (num > 0) {
        	alert ('This Storung allready exists!');
        }
        else{
        	//$('#form_new_storung').submit();
        };
    });



    // ------ /.TAB2 - Unterkategorie 1 Controllers ------
</script>

<?php require ('app/views/partials/footer_vendor_scripts.php'); ?>
<?php require ('app/views/partials/footer.php'); ?>