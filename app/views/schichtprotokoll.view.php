<?php require ('app/views/partials/head.php'); ?>

<form method='POST' role='form' id='form_tagesansicht' name='form_tagesansicht' action='schichtprotokoll'>
    <input type='hidden' name='halle_id' id='halle_id' value='<?= $halle_id;?>'>
    <div class='container'>
        <h3 id='main_title' name='main_title'>Störungseingabe Aktuell</h3>
    </div>
    <div class='container'>
    <div class='row' style='vertical-align: bottom;'>
        <div class='col-xs-4' style="max-width: 300px">
            <div class='input-group date' name='mdatum_tage' id='mdatum_tage'>
                <span class='input-group-addon'>Schichtprot Von</span>
                <input type='text' class='form-control datum_tage' name='datum_tage' id='datum_tage' value='<?=substr($datum_tage, 0,10);?>' readonly='readonly' style='min-width: 100px;'>
                <span class='input-group-addon' style='min-width: 10px;'><span class='glyphicon glyphicon-calendar' ></span></span>
            </div>

            <div class='input-group' name='schicht_group' id='schicht_group'>
                <span class='input-group-addon'>Schicht</span>
                <select disabled='disabled' class='selectpicker schicht' data-width='100%' id='schicht' name='schicht' style='display: none;'>
                    <option value=1 <?php if($schicht==1){echo 'selected';}?>>Frühschicht</option>
                    <option value=2 <?php if($schicht==2){echo 'selected';}?>>Spätschicht</option>
                    <option value=3 <?php if($schicht==3){echo 'selected';}?>>Nachtschicht</option>
                </select>
            </div>

            <div class='input-group' name='ds_group' id='ds_group'>
                <span class='input-group-addon'>DS</span>
                <select disabled='disabled' class='selectpicker ds' data-width='100%' id='ds' name='ds'>
                <?php for ($i=1; $i <=3 ; $i++): ?>
                    <option value=<?= $i;?> <?php if($ds==$i){echo 'selected';}?>><?= $i;?></option>
                <?php endfor; ?>
                </select>
            </div>
            <div class='input-group div_submit_form_tagesansicht' style='display: none;'>
                <BR>
                <button type="submit" name='submit_form_tagesansicht' id='submit_form_tagesansicht' class="btn btn-success">Daten holen</button>
            </div>
        </div>
        <div class='col-xs-5' style="height: 100%;">
            <div class='form-group'>
                <div class='col-xs-3' style="max-width: 100px;">
                    <input type='hidden' name='autorefresh_checked' id='autorefresh_checked' value='<?= $autorefresh;?>'>
                    <?php $checked = ($autorefresh =='on') ? 'checked' : ''; ?>
                    <input type='checkbox' <?=$checked;?> name='autorefresh' id='autorefresh' data-toggle="toggle" data-on="On" data-off="Off">
                </div>
                <div class='bottom-align-text col-xs-6 div_autorefreshtime'>
                <div class='input-group' name='autorefreshtime_group' id='autorefreshtime_group'>
                    <span class='input-group-addon refresh_addon'>Autorefresh</span>
                    <input type='number' class='form-control' name='autorefreshtime' id='autorefreshtime' style='min-width: 60px;' value='' >
                     <span class='input-group-addon refresh_addon' style='min-width: 0px;'>Min</span>
                </div>
                </div>    
            </div>
        </div>
        <!--
        <div class='col-xs-3' style='text-align: right;'>
            <span id='print' name='print' class="glyphicon glyphicon-print kursor print" aria-hidden="true" title='Drücken'></span>
        </div>
        -->
    </div>
</div>
</form>

<div class='container'><HR></div>

<!--Halle tabulator -->
<div id='tabulator' name='tabulator' class='container'> 
    <ul  class='nav nav-pills'>
        <li id='li_halle3' name='li_halle3' class='li_halle <?php if ($halle_id == 3){echo "active";} ?>'><a href='#tab_halle3' data-toggle='tab'>Halle 3</a></li>
        <li id='li_halle4' name='li_halle4' class='li_halle <?php if ($halle_id == 4){echo "active";} ?>'><a href='#tab_halle4' data-toggle='tab'>Halle 4</a></li>
        <li id='li_halle5' name='li_halle5' class='li_halle <?php if ($halle_id == 5){echo "active";} ?>'><a href='#tab_halle5' data-toggle='tab'>Halle 4A</a></li>
    </ul>
</div>
<!--/.Halle tabulator -->

<div class='container'>
    <button type="button" name='button_newErrorModal' id='button_newErrorModal' class="btn btn-primary" data-toggle="modal" data-target="#newErrorModal">
        Neue Störung
    </button>
</div>

<!--Errors table -->
<div class='container'>
<div class='table-responsive'>
<table id='myDataTable' class='table table-striped table-bordered table-hover schichten' style='cellspacing: 0; width: 100%;'>
    <thead>
        <tr>
            <th>Datum</th>
            <th>Schichtbeginn</th>
            <th>Anfangszeit</th>
            <th>Dauer (min)</th>
            <th>Art</th>
            <th>Störung</th>
            <th>Halle</th>
            <th>Linie</th>
            <th>Ebene 1</th>
            <th>Ebene 2</th>
            <th>Ebene 3</th>
            <th>Ebene 4</th>
            <th style='width: 30%;'>Bemerkung manuell</th>
            <th>Modell</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($filtered_errors as $filtered_error): ?>
        <tr>
            <td><?= $filtered_error->DATUM;?></td>
            <td><?= $filtered_error->SCHICHT_BEGIN;?></td>
            <td><?= $filtered_error->VON.':00';?></td>
            <td><?= $filtered_error->DAUER;?></td>
            <td><?= $filtered_error->ART;?></td>
            <td><?= $filtered_error->STORUNG_TEXT;?></td>
            <td><?= $filtered_error->HALLE;?></td>
            <td><?= $filtered_error->LINIE;?></td>
            <td><?= $filtered_error->U1_TEXT;?></td>
            <td><?= $filtered_error->U2_TEXT;?></td>
            <td><?= $filtered_error->U3_TEXT;?></td>
            <td><?= $filtered_error->U4_TEXT;?></td>
            <td><?= $filtered_error->KOMENTAR;?></td>
            <td><?= $filtered_error->TYP;?></td>
            <td>
                <?php $error_id = $filtered_error->ID_ERROR; ?>
                <span id='update_<?=$error_id;?>' name='update_<?=$error_id;?>' class="glyphicon glyphicon-pencil kursor update" aria-hidden="true" title='Aktualisieren'></span>&nbsp;&nbsp;
                <span id='copire_<?=$error_id;?>' name='copire_<?=$error_id;?>' class="glyphicon glyphicon-copy kursor kopieren" aria-hidden="true" title='Kopieren'></span>&nbsp;&nbsp;
                <span id='delete_<?=$error_id;?>' name='delete_<?=$error_id;?>' class="glyphicon glyphicon-remove kursor delete submit_form_modal_delete" aria-hidden="true" title='Löschen'></span>

            </td>
        </tr>
        <?php endforeach; ?>
        
    </tbody>
</table>
</div>
</div>
<!--/.Errors table -->

<div class='container'>
<table class='round_corners'>
    <tbody>
        <tr>
            <td><strong>Techniche Störungsdauer</strong></td>
            <td>Linie 0:</td>
            <td><strong><?= $dauer_sum['l0_t'] ?> Min</strong></td>
            <td>Linie 1:</td>
            <td><strong><?= $dauer_sum['l1_t'] ?> Min</strong></td>
            <td>Linie 2:</td>
            <td><strong><?= $dauer_sum['l2_t'] ?> Min</strong></td>
            <td>Gesamt</td>
            <td><strong><?= $dauer_sum['l0_t']+$dauer_sum['l1_t']+$dauer_sum['l2_t'] ?> Min</strong></td>
        </tr>
        <tr>
            <td><strong>Organisatorische Störungsdauer</strong></td>
            <td>Linie 0:</td>
            <td><strong><?= $dauer_sum['l0_o'] ?> Min</strong></td>
            <td>Linie 1:</td>
            <td><strong><?= $dauer_sum['l1_o'] ?> Min</strong></td>
            <td>Linie 2:</td>
            <td><strong><?= $dauer_sum['l2_o'] ?> Min</strong></td>
            <td>Gesamt</td>
            <td><strong><?= $dauer_sum['l0_o']+$dauer_sum['l1_o']+$dauer_sum['l2_o'] ?> Min</strong></td>
        </tr>
        <tr>
            <td><strong>Qualitätsbedingte Störungsdauer</strong></td>
            <td>Linie 0:</td>
            <td><strong><?= $dauer_sum['l0_q'] ?> Min</strong></td>
            <td>Linie 1:</td>
            <td><strong><?= $dauer_sum['l1_q'] ?> Min</strong></td>
            <td>Linie 2:</td>
            <td><strong><?= $dauer_sum['l2_q'] ?> Min</strong></td>
            <td>Gesamt</td>
            <td><strong><?= $dauer_sum['l0_q']+$dauer_sum['l1_q']+$dauer_sum['l2_q'] ?> Min</strong></td>
        </tr>
    </tbody>
</table>
</div>

<!-- newErrorForm modal -->
<div id="newErrorModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method='POST' role='form' id='form_modal' name='form_modal' action=''>
        <input type='hidden' name='halle_id_modal' id='halle_id_modal' value='' >
        <input type='hidden' name='datum_tage_modal' id='datum_tage_modal' value='' >
        <input type='hidden' name='schicht_modal' id='schicht_modal' value='' >
        <input type='hidden' name='ds_modal' id='ds_modal' value='' >
        <input type='hidden' name='autorefresh_checked_modal' id='autorefresh_checked_modal' value='' >
        <input type='hidden' name='autorefreshtime_modal' id='autorefreshtime_modal' value='' >
        <input type='hidden' name='error_id' id='error_id' value='' >
        <input type='hidden' name='delete' id='delete' value=''>
        <input type='hidden' name='halle_id_delete' id='halle_id_delete' value='<?= $halle_id;?>'>

        <div class="modal-header" style="text-align: center">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title moj_naslov" id="gridModalLabel"></h4>
        </div>
        <div class="modal-body">
            <div class='container-fluid'>
                <div class='form-group'>
                    <div class='row'>
                        <div class='col-xs-12'>
                            <table>
                                <tbody>
                                    <tr>
                                        <td style='vertical-align:top;'>
                                            <div class='input-group'>
                                                <h5><strong>Störungen</strong></h5>
                                                <select class='form-control box_picker' data-width='100%' id='storungen' name='storungen' size='18'>
                                                    <?php foreach ($filtered_storungen as $key => $value): ?>
                                                        <option value='<?=$value->ID?>' data-key='<?=$key?>'><?=$value->TEXT?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </td>
                                        <td style='vertical-align:top;'>
                                            <div class='input-group' name='div_linien' id='div_linien' style='display: none;'>
                                                 <h5><strong>Linien</strong></h5>
                                                <select class='form-control box_picker' data-width='100%' id='linien' name='linien' size='16'></select>
                                            </div>
                                        </td>
                                        <?php for ($i=1; $i <=4 ; $i++): ?>
                                        <td style='vertical-align:top;'>
                                            <div class='input-group' name='div_unterkategorie<?= $i;?>' id='div_unterkategorie<?= $i;?>' style='display: none;'>
                                                <h5><strong>Unterkategorie <?= $i; ?></strong></h5>
                                                <select class='form-control box_picker' data-width='100%' id='unterkategorie<?= $i;?>' name='unterkategorie<?= $i;?>' size='18'></select>
                                            </div>
                                        </td>
                                        <?php endfor; ?>   
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class='row' ><div class='col-xs-12'><HR></div></div>
                    <div class='row'>
                        <div class='col-xs-12'>
                        <table style="width: 100%;">
                            <tbody>
                                <tr>
                                    <td style='vertical-align: top; width: 4%;'>
                                        <h4>Art</h4>
                                        <select class='form-control box_picker' data-width='100%' id='arts' name='arts' size='5' style='min-width: 40px;'>
                                            <?php foreach ($arts as $key => $value): ?>
                                                <option value='<?=$value->ID?>'><?=$value->TEXT?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                    <td style='vertical-align: top;width: 8%;'>
                                        <h4>Typ</h4>
                                        <select class='form-control box_picker' data-width='100%' id='typs' name='typs' size='5' style='min-width: 90px;'>
                                            <?php foreach ($typs as $key => $value): ?>
                                                <option value='<?=$value->ID?>'><?=$value->TEXT?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </td>
                                    <td style='vertical-align: top; width: 20%;'>
                                        <h4>Störungsdauer</h4>
                                        <div class='input-group'>
                                            <span class='input-group-addon' style='width: 45%;'>Startzeit</span>
                                            <select class='form-control zeit_h' data-width='100%' id='startzeit_h' name='startzeit_h' style='min-width: 65px;'>
                                                
                                            </select>
                                            <span class='input-group-addon' style="min-width: 1%; width: 1%;">-</span>
                                            <select class='form-control zeit_m' data-width='100%' id='startzeit_m' name='startzeit_m' style='min-width: 65px;'>
                                                <?php for($i =0; $i<=59; $i++): ?>
                                                    <?php if($i<10){$i='0'.$i;} ?>
                                                    <option value='<?=$i?>'><?=$i?></option>
                                                <?php endfor; ?>
                                            </select>
                                        </div>
                                        <div class='input-group'>
                                            <span class='input-group-addon' style='width: 45%;'>Dauer in Min</span>
                                                <input type='number' class='form-control dauer' name='dauer' id='dauer' value='0'>
                                        </div>
                                        <div class='input-group'>
                                            <span class='input-group-addon' style='width: 45%;'>Endzeit</span>
                                            <select class='form-control zeit_h' data-width='100%' id='endzeit_h' name='endzeit_h' style='min-width: 65px;'>
                                                
                                            </select>
                                            <span class='input-group-addon ' style="min-width: 1%; width: 1%;">-</span>
                                            <select class='form-control zeit_m' data-width='100%' id='endzeit_m' name='endzeit_m' style='min-width: 65px;'>
                                                <?php for($i =0; $i<=59; $i++): ?>
                                                    <?php if($i<10){$i='0'.$i;} ?>
                                                    <option value='<?=$i?>'><?=$i?></option>
                                                <?php endfor; ?>
                                            </select>
                                        </div>
                                    </td>
                                    <td style='vertical-align: top; width: 68%;'>
                                        <h4>Bemerkung</h4>
                                        <div class='input-group'>
                                            <span class='input-group-addon' >Aktueller Auswahl</span>
                                            <input type='text' class='form-control bemerkung' name='autotext' id='autotext' value='' readonly='true'>
                                        </div>
                                        <div>
                                            <textarea id='user_text' name='user_text' class='form-control user_text' maxlength='200' rows='3'></textarea>
                                        </div>
                                        
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Abbruch</button>
          <span id='save' name='save'><button type="button" id='submit_form_modal_save' name='submit_form_modal_save' class="btn btn-primary">Speichern</button></span>
          <span id='edit' name='edit' style='display: none;'><button type="button" id='submit_form_modal_edit' name='submit_form_modal_edit' class="btn btn-primary">Aktualisiren</button></span>
        </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<!-- /.newErrorForm modal -->


<?php require ('app/views/partials/footer_vendor_scripts.php'); ?>
<?php
function utf8_encode_deep(&$input) {
        if (is_string($input)) {
            $input = utf8_encode($input);
        } 
        else if (is_array($input)) {
            foreach ($input as &$value) {
                if (is_array($value)){
                    foreach ($value as &$key) {
                        utf8_encode_deep($key);
                    }
                    unset($key);
                }
                else{
                    utf8_encode_deep($value);
                }
            }
            unset($value);
        }
        else if (is_object($input)) {
            $vars = array_keys(get_object_vars($input));
            foreach ($vars as $var) {
                utf8_encode_deep($input->$var);
            }
        }
        return $input;
    }
    $x_filtered_errors = utf8_encode_deep($filtered_errors);
?>
<!-- define global functions -->
<script type="text/javascript">
    function isBetween(n, a, b) {
            return (n - a) * (n - b) <= 0
        };

    function fillVonBisHours (start, end) {
        if (Number(start)>Number(end)){
                
                
                for(i=Number(start); i<=23; i++){
                    if(i<10){i='0'+i};
                    $('#startzeit_h, #endzeit_h').append('<option value="'+i+'">' + i + '</option>');

                };
                for(i=0; i<=Number(end); i++){
                    if(i<10){i='0'+i};
                    $('#startzeit_h, #endzeit_h').append('<option value="'+i+'">' + i + '</option>');
                };
            }
            else {
                for(i=Number(start); i<=Number(end); i++){
                    if(i<10){i='0'+i};
                    $('#startzeit_h, #endzeit_h').append('<option value="'+i+'">' + i + '</option>');
                };
            };
    };
</script>
<!-- /.define global functions -->

<!-- initalize dataTable -->
<script type='text/javascript'>
    $(document).ready(function() {
        $.fn.dataTable.moment('DD.MM.YYYY');
        $('#myDataTable').DataTable({
            searching: false,
            paging: false,
            info: false,
            language: {
                emptyTable:     'Keine Daten in der Tabelle vorhanden',
                info:           '_START_ bis _END_ von _TOTAL_ Einträgen',
                infoEmpty:      '0 bis 0 von 0 Einträgen',
                infoFiltered:   '(gefiltert von _MAX_ Einträgen)',
                infoPostFix:    '',
                thousands:      '.',
                lengthMenu:     '_MENU_ Einträge anzeigen',
                loadingRecords: 'Wird geladen...',
                processing:     'Bitte warten...',
                search:         'Suchen',
                zeroRecords:    'Keine Einträge vorhanden.',
                paginate: {
                    first:      'Erste',
                    previous:   'Zurück',
                    next:       'Nächste',
                    last:       'Letzte'
                },
                aria: {
                    sortAscending:  ': aktivieren, um Spalte aufsteigend zu sortieren',
                    sortDescending: ': aktivieren, um Spalte absteigend zu sortieren'
                },
                select: {
                    rows: {
                        _: '%d Zeilen ausgewählt',
                        0: 'Zum Auswählen auf eine Zeile klicken',
                        1: '1 Zeile ausgewählt'
                    }
                }
            }
        });
    });
</script>
<!-- /.initalize dataTable -->
<!-- define global variables -->
<script type="text/javascript">
    // autorefresh varables
    var autoplay = null;
    //var int = null;
    //var onoff = null;
    var onoff = '<?=$autorefresh;?>';
    var int = '<?=$autorefreshtime;?>';

    // schicht and DS varables
    var schichten = <?= json_encode($schichten);?>;

    // selection block variables
    var show_hide_speed = 0;
    var autotext_text ='';
    var auto_text_container ='';

    var halle_id = <?= $halle_id;?>;
    var storungen = <?= json_encode($filtered_storungen);?>;
    var errors = <?= json_encode($x_filtered_errors);?>;
    var unterkategorie = <?= json_encode($unterkategorien);?>;

    // start time, end time, dauer varabales
    var hours = <?= json_encode($hours);?>;
    var current_schicht = <?=$schicht;?>;
    var current_ds = <?=$ds;?>;
    var current_date = new Date();
    var current_date_val = '<?=substr($datum_tage, 0,10);?>';
    var current_hour = current_date.getHours();
    if (current_hour<10){current_hour = '0'+current_hour;};

    var current_minute = current_date.getMinutes();
    if (current_minute<10){current_minute = '0'+current_minute;};


    var filtered = hours.filter(function (v,i) {
                        return v.SCHICHT==current_schicht;
                    });
    var start = Number(filtered[0].STR_TIME.substr(0,2));
    if (isBetween(Number(start),0,9)){start = '0'+start;};
    var end = Number(filtered[0].STR_TIME_END.substr(0,2));
    if (isBetween(Number(end),0,9)){end = '0'+end;};

    var zeit = {};
    zeit['startzeit_h'] = current_hour;
    zeit['startzeit_m'] = current_minute;
    zeit['endzeit_h'] = current_hour;
    zeit['endzeit_m'] = current_minute;
    //console.log(zeit);
</script>
<!-- /.define global variables -->



<!-- initalize selectpicker -->
<script type='text/javascript'>
    $('.selectpicker').selectpicker({
        size: 4
    });
</script>
<!-- /.initalize selectpicker -->

<!-- configure dateTime Picker -->
<script type='text/javascript'>
    function initializeDateTimePicker(){
        var today = new Date();
        var format_date = 'DD.MM.YYYY';
        var viewMode = 'days';
        
        $('#datum_tage').datetimepicker({
            viewMode: viewMode,
            locale: moment.locale('de'),
            format: format_date,
            
            useCurrent: false,
            extraFormats: false,
            sideBySide: true
        });
    }
    initializeDateTimePicker();
</script>
<!-- /.configure dateTime Picker -->


<!-- open print dialog on print icon click -->
<script type="text/javascript">
    $('#print').click(function(){
        window.print();
    });
</script>
<!-- /.open print dialog on print icon click -->

<!-- update/copy selected error -->
<script type="text/javascript">
    var x_error_id = '';
    var x_filtered_error = '';
    var x_storung_id = '';
    var x_u1_id = '';
    var x_u2_id = '';
    var x_u3_id = '';
    var x_u4_id = '';
    var x_art_id = '';
    var x_typ_id = '';
    var x_von_h = '';
    var x_von_m = '';
    var x_bis_h = '';
    var x_bis_m = '';
    var x_dauer = '';
    var x_datum = '';
    var x_linie = '';
    var x_schicht = '';
    var x_halle = '';
    var x_komentar = '';

    $('.update, .kopieren').click(function(){
        x_error_id = this.id.substring(7);
        $('#error_id').val(x_error_id);
        x_filtered_error = errors.filter(function (v,i) {
                        return v.ID_ERROR==x_error_id;
                    });

        console.log(x_filtered_error);
        x_storung_id = x_filtered_error[0].STORUNG_ID;
        x_u1_id = x_filtered_error[0].U1_ID;
        x_u2_id = x_filtered_error[0].U2_ID;
        x_u3_id = x_filtered_error[0].U3_ID;
        x_u4_id = x_filtered_error[0].U4_ID;
        x_art_id = x_filtered_error[0].ART_ID;
        x_typ_id = x_filtered_error[0].TYP_ID;
        x_von_h = x_filtered_error[0].VON.slice(0,2);
        x_von_m = x_filtered_error[0].VON.slice(-2);
        x_bis_h = x_filtered_error[0].BIS.slice(0,2);
        x_bis_m = x_filtered_error[0].BIS.slice(-2);
        x_dauer = x_filtered_error[0].DAUER;
        x_datum = x_filtered_error[0].DATUM;
        x_linie = x_filtered_error[0].LINIE;
        x_schicht = x_filtered_error[0].SCHICHT;
        x_halle = x_filtered_error[0].HALLE;
        x_komentar = x_filtered_error[0].KOMENTAR;

        zeit['startzeit_h'] = x_von_h;
        zeit['startzeit_m'] = x_von_m;
        zeit['endzeit_h'] = x_bis_h;
        zeit['endzeit_m'] = x_bis_m;

        $('#storungen').val(x_storung_id).trigger('click');
        $('#linien').val(x_linie).trigger('click');
        $('#unterkategorie1').val(x_u1_id).trigger('click');
        $('#unterkategorie2').val(x_u2_id).trigger('click');
        $('#unterkategorie3').val(x_u3_id).trigger('click');
        $('#unterkategorie4').val(x_u4_id).trigger('click');
        
        $('#typs').val(x_typ_id);
        $('#arts').val(x_art_id);
        
        fillVonBisHours(start, end);
        $('#startzeit_h').val(x_von_h);
        $('#startzeit_m').val(x_von_m);
        $('#endzeit_h').val(x_bis_h);
        $('#endzeit_m').val(x_bis_m);
        $('#dauer').val(x_dauer);

        $('#user_text').val(x_komentar).trigger('keyup');
        
        $('.selectpicker').selectpicker('refresh');
    });
    $('.update').click(function(){
        $('#save').hide();
        $('#edit').show();

        tekst = '<span class="appended"> Aktualisieren ' + x_datum + ', ' + $("#schicht option:selected").text() + ', DS=' + $("#ds option:selected").text() + '</span>';
        $('.moj_naslov').append(tekst);

        $('#newErrorModal').modal('show');
    });
    $('.kopieren').click(function(){
        tekst = '<span class="appended"> Kopieren '+ x_datum +', ' + $("#schicht option:selected").text() + ', DS=' + $("#ds option:selected").text() + '</span>';
        $('.moj_naslov').append(tekst);

        $('#newErrorModal').modal('show');
    });
</script>
<!-- /.update selected error -->

<!-- Schicht and DS autoselect controller -->
<script type="text/javascript">
    if(schichten.length <= 0){
        alert ('Keine Schicht für diese Woche definiert, bitte definieren im Admin');
    };
    if(schichten.length > 0){
        //$('#schicht option:selected').val().trigger('change');

        $('#schicht').change(function(){
            var schicht = this.value;
            switch (schicht){
                case '1':
                    team = schichten[0].TEAM.substr(0,1);
                    $('#ds').val(team);
                    $('.selectpicker').selectpicker('refresh');
                    break;
                case '2':
                    team = schichten[0].TEAM.substr(2,1);
                    $('#ds').val(team);
                    $('.selectpicker').selectpicker('refresh');
                     break;
                case '3':
                    team = schichten[0].TEAM.substr(4,1);
                    $('#ds').val(team);
                    $('.selectpicker').selectpicker('refresh');
                     break;
            };
        });

        $('#ds').change(function(){
            var ds = this.value;
            switch (ds){
                case '1':
                    var index = schichten[0].TEAM.indexOf('1');
                    schicht = schichten[0].SCHICHT.substr(index,1);
                    $('#schicht').val(schicht);
                    $('.selectpicker').selectpicker('refresh');
                    break;
                case '2':
                    var index = schichten[0].TEAM.indexOf('2');
                    schicht = schichten[0].SCHICHT.substr(index,1);
                    $('#schicht').val(schicht);
                    $('.selectpicker').selectpicker('refresh');
                     break;
                case '3':
                    var index = schichten[0].TEAM.indexOf('3');
                    schicht = schichten[0].SCHICHT.substr(index,1);
                    $('#schicht').val(schicht);
                    $('.selectpicker').selectpicker('refresh');
                     break;
            };
        });
    }
    else {
        alert ('Keine Schicht für diese Woche definiert, bitte wählen Sie manuell');
    };
</script>
<!-- /.Schicht and DS autoselect controller -->

<!-- form tagesansicht submit controller -->
<script type="text/javascript">
    $('.li_halle').click(function(){
        $('#halle_id').val(this.id.slice(-1));
        $('select').prop('disabled', false);
        $('#form_tagesansicht').submit();
    });
</script>
<script type="text/javascript">
    $('#button_newErrorModal').click(function(){
        is_selected = true;
        date_selected   = $('.datum_tage').val();
        date_variable   = current_date_val;
        schicht_selected= $('.schicht option:selected').val();
        schicht_variable= current_schicht;
        ds_selected     = $('.ds option:selected').val();
        ds_variable     = current_ds;
        if (date_selected != date_variable) {is_selected = false;};
        if (schicht_selected != schicht_variable) {is_selected = false;};
        if (ds_selected != ds_variable) {is_selected = false;};

        if (is_selected == false){
            alert ('Button DATEN-HOLEN wurde nicht geklickt. Die Seite wird mit entsprechenden Werten aktualisiert.');
            $('select').prop('disabled', false);
            $('#form_tagesansicht').submit();
        };
    });
</script>
<!-- /.form tagesansicht submit controller -->

<!-- autorefresh controller -->
<script>
    $( document ).ready(function() {
        if(onoff == 'on'){
            $('#autorefreshtime').val(int);
            $('#main_title').text('Störungseingabe Aktuell');
            autoplay = setInterval(function() {
                    $('select').prop('disabled', false);
                    $('#form_tagesansicht').submit();
                }, int*1000*60);
        }
        else{
            $('#main_title').text('Störungseingabe in der Vergangenheit');
            $('#autorefreshtime').val(int);
            $('#autorefreshtime').prop('readonly', true);
            $('#autorefreshtime').css('color', '#e6e6e6');
            $('.refresh_addon').css('color', '#e6e6e6');
            
            $('.datum_tage').prop('readonly', false);
            $('select').prop('disabled', false);
            $('.selectpicker').selectpicker('refresh');
            $('.div_submit_form_tagesansicht').show();
            
            clearInterval(autoplay);
        };
    });

    $('#autorefreshtime').keyup(function(){
        int = $(this).val()*1000*60;
        autoplay = setInterval(function() {
                $('select').prop('disabled', false);
                $('#form_tagesansicht').submit();
            }, int);
    });

    $('#autorefresh').change(function() {
        if($(this).prop('checked')){
            $('#main_title').text('Störungseingabe Aktuell');
            $("#autorefreshtime").prop("readonly", false);
            $('.refresh_addon').css('color', '#333');
            $('#autorefreshtime').css('color', '#333');
            int = $('#autorefreshtime').val();
            $('.div_autorefreshtime').show(300);
            $('#autorefresh_checked').val('on');
            
            $('.datum_tage').prop('readonly', true);
            $('select').prop('disabled', true);
            $('.selectpicker').selectpicker('refresh');
            $('.div_submit_form_tagesansicht').hide(300);
            
            autoplay = setInterval(function() {
                    $('select').prop('disabled', false);
                    $('#form_tagesansicht').submit();
                }, int*1000*60);
            
            $('#datum_tage').val(current_date_val);
            $('select').prop('disabled', false);
            $('#form_tagesansicht').submit();
        }
        else{
            $('#main_title').text('Störungseingabe in der Vergangenheit');
            $('#autorefresh_checked').val('off');
            $('#autorefreshtime').prop('readonly', true);
            $('#autorefreshtime').css('color', '#e6e6e6');
            $('.refresh_addon').css('color', '#e6e6e6');
            
            $('select').prop('disabled', false);
            $('.datum_tage').prop('readonly', false);
            $('.selectpicker').selectpicker('refresh');
            $('.div_submit_form_tagesansicht').show(300);
            
            clearInterval(autoplay);
        }
    });
</script>
<!-- /.autorefresh controller -->

<!-- before open modal check if it is current schicht. Fill times for currend Schicht and append text, date and time to Title-->
<script type="text/javascript">
    $('#button_newErrorModal').click(function(){
        var valid = true; //set true to disable current schicht check

        if (onoff == 'off') {
            current_hour = start;
            //if (Number(current_hour)<10){current_hour = '0'+current_hour;};
            current_minute = '00';
        };

        if (Number(start)>Number(end)){
            a = isBetween(Number(current_hour),Number(start),23);
            b = isBetween(Number(current_hour),0,Number(end));
            
            if (a == true || b == true){
                valid = true;
            };
        }

        else {
            if (Number(current_hour) >= Number(start) && Number(current_hour) <= Number(end)){
                valid = true;
            }
        };

        if (valid == true){
            
            fillVonBisHours(start, end);
            
            $('#startzeit_h').val(current_hour);
            $('#startzeit_m').val(current_minute);
            $('#endzeit_h').val(current_hour);
            $('#endzeit_m').val(current_minute);
            $('#dauer').val('0');
            $('.selectpicker').selectpicker('refresh');
            zeit['startzeit_h'] = current_hour;
            zeit['startzeit_m'] = current_minute;
            zeit['endzeit_h'] = current_hour;
            zeit['endzeit_m'] = current_minute;
            zeit['dauer'] = '0';
            

            tekst = '<span class="appended"> Neueingabe '+ current_date_val +', ' + $("#schicht option:selected").text() + ', DS=' + $("#ds option:selected").text() + '</span>';
            $( '.moj_naslov' ).append(tekst);
        }
        else{
            alert ('Not current schicht! Page will reload after you click OK');
            $('#form_tagesansicht').submit();
        };
    });
</script>
<!-- /.before open modal check if it is current schicht. Fill times for currend Schicht and append text, date and time to Title-->

<!-- von-bis-dauer time javascript controller -->
<script type="text/javascript">
    $('.zeit_h').change(function(){
        valid = false;
        zeit[this.id] = $(this).val();

        if (Number(start)<Number(end)){
            if(Number(zeit['startzeit_h']) > Number(zeit['endzeit_h'])){
                valid = true;
            };
        }
        else {
            a1 = isBetween(Number(zeit['startzeit_h']),Number(start),23);
            a2 = isBetween(Number(zeit['endzeit_h']),Number(start),23);
            b1 = isBetween(Number(zeit['startzeit_h']),0,Number(end));
            b2 = isBetween(Number(zeit['endzeit_h']),0,Number(end));

            if (a1 == true && a2 == true){
               if(Number(zeit['startzeit_h']) > Number(zeit['endzeit_h'])){
                    valid = true;
                };
            };
            if( b1 == true && b2 == true){
                if(Number(zeit['startzeit_h']) > Number(zeit['endzeit_h'])){
                    valid = true;
                };
            };
            if(b1 == true && a2 == true){
                valid = true;
            }
        };
        
        if(valid == true){
            zeit['startzeit_h'] = $(this).val();
            zeit['endzeit_h'] = $(this).val();
            $('#endzeit_h').val(zeit['endzeit_h']);
            $('#startzeit_h').val(zeit['startzeit_h']);

            $('.selectpicker').selectpicker('refresh');
        };

        if(Number(zeit['startzeit_h']) == Number(zeit['endzeit_h']) &&  Number(zeit['startzeit_m']) > Number(zeit['endzeit_m'])){
            zeit['endzeit_m'] = zeit['startzeit_m'];
            $('#endzeit_m').val(zeit['endzeit_m']);
        }
    });

        
    $('.zeit_m').change(function(){
        zeit[this.id] = $(this).val();
        if((Number(zeit['startzeit_h']) == Number(zeit['endzeit_h'])) && (Number(zeit['startzeit_m'])>Number(zeit['endzeit_m']))){
            zeit['startzeit_m'] = $(this).val();
            zeit['endzeit_m'] = $(this).val();
            $('#endzeit_m').val(zeit['endzeit_m']);
            $('#startzeit_m').val(zeit['startzeit_m']);

            $('.selectpicker').selectpicker('refresh');
        };
    });

    $('.zeit_h, .zeit_m').change(function(){
        end_hours_to_min = Number(zeit['endzeit_h'])*60;
        end_min_total = Number(end_hours_to_min) + Number(zeit['endzeit_m']);

        if(Number(zeit['startzeit_h'])>Number(zeit['endzeit_h'])){
            start_hours_to_min = Number(zeit['startzeit_h'])*60;
            start_min_total = 1440 - (Number(start_hours_to_min) + Number(zeit['startzeit_m']));

            dauer = Number(start_min_total)+Number(end_min_total);
        }
        else {
            start_hours_to_min = Number(zeit['startzeit_h'])*60;
            start_min_total = Number(start_hours_to_min) + Number(zeit['startzeit_m']);

            dauer = Number(end_min_total)-Number(start_min_total);
        };
        zeit['dauer'] = dauer;
        $('#dauer').val(dauer);
    });

    $('#dauer').click(function(){ $(this).val(''); });
    $('#dauer').focusout(function(){
        $('#dauer').val(zeit['dauer']);
    });
    $('#dauer').keyup(function(){
        zeit['dauer'] = $(this).val();

        if ((Number(zeit['startzeit_m'])+Number(zeit['dauer'])) >59){
            zeit['endzeit_h'] = Number(zeit['startzeit_h']) + parseInt((Number(zeit['startzeit_m'])+Number(zeit['dauer']))/60);
            if (isBetween(Number(zeit['endzeit_h']),0,9)){zeit['endzeit_h'] = '0'+zeit['endzeit_h'];};
            console.log(parseInt(Number(zeit['dauer'])/60));
            

            if (Number(start)>Number(end)){
                if (Number(zeit['endzeit_h'])>23){
                    zeit['endzeit_h'] = Number(zeit['endzeit_h'])-24;
                    if (isBetween(Number(zeit['endzeit_h']),0,9)){zeit['endzeit_h'] = '0'+zeit['endzeit_h'];};
                };
            };

            zeit['endzeit_m'] = (Number(zeit['startzeit_m'])+Number(zeit['dauer'])) % 60;
            if (isBetween(Number(zeit['endzeit_m']),0,9)){zeit['endzeit_m'] = '0'+zeit['endzeit_m'];};

            if (Number(start)<Number(end)){
                if (Number(zeit['endzeit_h'])>Number(end)){
                    alert('Dauer longer than schicht! Setting end time to end of schicht');
                    zeit['endzeit_h'] = end;
                    zeit['endzeit_m'] = 59;
                }
            }
            else {
                if (isBetween(Number(zeit['endzeit_h']),Number(end)+1,Number(start)-1)){
                    alert('Dauer longer than schicht! Setting end time to end of schicht');
                    zeit['endzeit_h'] = end;
                    zeit['endzeit_m'] = 59;
                }

            }

            end_hours_to_min = Number(zeit['endzeit_h'])*60;
            end_min_total = Number(end_hours_to_min) + Number(zeit['endzeit_m']);

            if(Number(zeit['startzeit_h'])>Number(zeit['endzeit_h'])){
                start_hours_to_min = Number(zeit['startzeit_h'])*60;
                start_min_total = 1440 - (Number(start_hours_to_min) + Number(zeit['startzeit_m']));

                dauer = Number(start_min_total)+Number(end_min_total);
            }
            else {
                start_hours_to_min = Number(zeit['startzeit_h'])*60;
                start_min_total = Number(start_hours_to_min) + Number(zeit['startzeit_m']);

                dauer = Number(end_min_total)-Number(start_min_total);
            };

            zeit['dauer']= dauer;
            if (isBetween(Number(zeit['dauer']),0,9)){zeit['dauer'] = '0'+zeit['dauer'];};
            $('#dauer').val(dauer);
          

            $('#endzeit_h').val(zeit['endzeit_h']);
            $('#endzeit_m').val(zeit['endzeit_m']);
            $('.selectpicker').selectpicker('refresh');
        }
        else {
            zeit['startzeit_h'] = $('#startzeit_h option:selected').val();
            zeit['startzeit_m'] = $('#startzeit_m option:selected').val();
            zeit['endzeit_h'] = zeit['startzeit_h'];
            zeit['endzeit_m'] = Number(zeit['startzeit_m']) + Number(zeit['dauer']);
            if (isBetween(Number(zeit['endzeit_m']),0,9)){zeit['endzeit_m'] = '0'+zeit['endzeit_m'];};

            $('#endzeit_h').val(zeit['startzeit_h']);
            $('#endzeit_m').val(zeit['endzeit_m']);
            $('.selectpicker').selectpicker('refresh');
        }
        console.log(zeit);
    });
</script>
<!-- /.von-bis-dauer time javascript controller -->

<!-- selection javascript controller block -->
<script type="text/javascript">
    $('select.box_picker').click(function(){
        var switch_cases = this.id.substr(0, this.id.length-1);
        var unterkat_nr = Number(this.id.slice(-1))+1;

        switch (switch_cases) {
            case 'storunge':
                for (i=1; i<=4; i++){
                    $('#unterkategorie'+i).html("");
                    $('#div_unterkategorie'+i).hide(show_hide_speed);
                };
                var array_id = $('#storungen option:selected').data('key');
                var storungen_id = $('#storungen option:selected').val();
                var filtered = storungen[array_id];
                
                $('#div_linien').show(show_hide_speed);
                $('#linien').html("");
                
                for (i=0; i<=2; i++){
                    var b = 'H'+halle_id+'L'+i;
                    var linie_array_pos = i * 2 + 1; 
                    if (filtered[b] == '1'){
                        $('#linien').append("<option data-key='"+array_id+"' data-linie_position = '"+linie_array_pos+"' data-storungen_id ='"+storungen_id+"' value='"+i+"'>"+i+"</option>");
                    };
                };
                $('#autotext').val(autotext_text);
            break;

            case 'linie':
                var filtered ='';
                for (i=2; i<=4; i++){
                    $('#unterkategorie'+i).html("");
                    $('#div_unterkategorie'+i).hide(show_hide_speed);
                };
                // position of linie boolean in STR_UNTERKATEGORIE.LINIE_ARRAY 
                var linie_position = $('#linien option:selected').data('linie_position');
                // ID of STR_STORUNG.ID for wich this unterkategoris is available
                if (linie_position){
                    var storungen_id = $('#linien option:selected').data('storungen_id');
                    // filtered unterkategorie1 by STR_STORUNG.ID and linie number
                    filtered = unterkategorie.filter(function (v,i) {
                        return v.ID_PARRENT.indexOf('|'+storungen_id+'|')>=0 && v.HALLE.indexOf('|'+halle_id+'|')>=0 && v.LINIE_ARRAY.substr(linie_position, 1) == 1;
                    });
                };
     
                if (filtered.length != 0){
                    $("#unterkategorie1").html("");

                    $.each(filtered, function(key, value){
                        $('#unterkategorie1').append("<option data-linie_position='"+linie_position+"' data-storungen_id='"+storungen_id+"' data-unterkategorie_nr='"+value.UNTERKATEGORIE_NR+"' value='"+value.ID+"'>"+value.TEXT+"</option>");
                    });
                    filtered ='';
                    storungen_id ='';
                    linie_position = '';

                    $('#div_unterkategorie1').show(show_hide_speed);
                }
                else {
                    $("#unterkategorie1").html("");
                    $('#div_unterkategorie1').hide(show_hide_speed);
                    filtered ='';
                    storungen_id ='';
                    linie_position = '';
                };
                $('#autotext').val(autotext_text);
            break;

            case 'unterkategorie':
            var kat_name = $(this).attr('name');
                var filtered ='';
                for (i=unterkat_nr+1; i<=4; i++){
                    $('#unterkategorie'+i).html("");
                    $('#div_unterkategorie'+i).hide(show_hide_speed);
                };
                //alert($(this).attr('name'));
                var storungen_id = $('#storungen option:selected').val();
                var unterkategorie1_id = $('#unterkategorie1 option:selected').val();
                var unterkategorie_id = $('#unterkategorie'+(unterkat_nr-1)+' option:selected').val();
                var unterkategorie_text = $('#unterkategorie'+(unterkat_nr-1)+' option:selected').text();
                if (unterkategorie_id){
                    var linie_position = $('#unterkategorie'+(unterkat_nr-1)+' option:selected').data('linie_position');
                    // filtered unterkategorie1 by STR_STORUNG.ID and linie number
                    filtered = unterkategorie.filter(function (v,i) {
                        return  v.ID_PARRENT.indexOf('|'+unterkategorie_id+'|')>=0 && 
                                v.HALLE.indexOf('|'+halle_id+'|')>=0 && 
                                v.ID_STORUNG.indexOf('|'+storungen_id+'|')>=0 && 
                                v.LINIE_ARRAY.substr(linie_position, 1) == 1;
                    });
                    if (kat_name=='unterkategorie2'){
                        novo = filtered.filter(function (v,i) {
                            u = v.U1_ID.indexOf('|'+unterkategorie1_id+'|')>=0;
                            return u;
                        });
                        filtered = novo;
                    };
                }; 
                if (filtered.length != 0){
                    $('#unterkategorie'+unterkat_nr).html("");

                    $.each(filtered, function(key, value){
                        $('#unterkategorie'+unterkat_nr).append("<option data-linie_position='"+linie_position+"' data-unterkategorie_nr='"+value.UNTERKATEGORIE_NR+"' value='"+value.ID+"'>"+value.TEXT+"</option>");
                    });

                    filtered ='';
                    storungen_id ='';
                    unterkategorie_id ='';
                    linie_position = '';

                    $('#div_unterkategorie'+unterkat_nr).show(show_hide_speed);
                }
                else {
                    $('#unterkategorie'+unterkat_nr).html("");
                    $('#div_unterkategorie'+unterkat_nr).hide(show_hide_speed);
                    filtered ='';
                    storungen_id ='';
                    unterkategorie_id ='';
                    linie_position = '';
                };

                for (i=1; i<unterkat_nr; i++){
                    var unterkategorie_text = $('#unterkategorie'+(i)+' option:selected').text();
                    if (i==1){
                        autotext_text = autotext_text + unterkategorie_text;
                    }
                    else{
                        autotext_text = autotext_text + '/'+unterkategorie_text;
                    };
                };

                $('#autotext').val(autotext_text);
                auto_text_container = autotext_text;

                autotext_text ='';
            break;
        };
    });

    $('#user_text').keyup(function() {
        input_text = auto_text_container + '/' + $(this).val();
        input_text = input_text.substring(0,200);
        $('#autotext').val(input_text);
    });
</script>
<!-- /.selection javascript controller block -->

<!-- idle time controller -->
<script type="text/javascript">
    /*
    var idleTime = 0;
    $(document).ready(function () {
        //Increment the idle time counter every 30 seconds.
        var idleInterval = setInterval(timerIncrement, 30000); // 30 seconds

        //Zero the idle timer on mouse movement.
        $(this).mousemove(function (e) {
            idleTime = 0;
        });
        $(this).keypress(function (e) {
            idleTime = 0;
        });
    });

    function timerIncrement() {
        idleTime = idleTime + 1;
        if (idleTime > 1) { // 1 minute
            $('#newErrorModal').modal('hide');
        }
    }
    */
</script>
<!-- /.idle time controller -->

<!-- clear modal data on close -->
<script type="text/javascript">
    $('#newErrorModal').on('hidden.bs.modal', function () {
        $('.box_picker, .zeit_h, .zeit_m, .dauer, .bemerkung, .user_text').val('');
        $('#startzeit_h, #endzeit_h').empty();
         for (i=1; i<=4; i++){
            $('#unterkategorie'+i).html("");
            $('#div_unterkategorie'+i).hide();
        };
        $('#linien').html("");
        $('#div_linien').hide();
        $('.appended').remove();
        $('#save').show();
        $('#edit').hide();
    });
    </script>
<!-- /.clear modal data on close -->

<!-- controller for checking if all required selections are made before submit insert/edit -->
<script type="text/javascript">
    var submit = '';
    $('#submit_form_modal_save, #submit_form_modal_edit').click(function(){
        submit = true;
        //alert($('#arts option:selected').val());
        if($('#storungen option:selected').val() === undefined ||
            $('#linien option:selected').val() === undefined ||
            $('#arts option:selected').val() === undefined){
                submit = false;
                alert('Bitte Auswählen Störung, Linie und Art!');
        };
        
        if ($('#dauer').val() == '0'){
            submit = false;
            alert('Dauer kann nicht 0 sein!');   
        };
        if (submit==true){
            $('#halle_id_modal').val(<?= $halle_id;?>);
            $('#halle_id').val(<?= $halle_id;?>);
            $('select').prop('disabled', false);
            $('#datum_tage_modal').val($('.datum_tage').val());
            $('#autorefresh_checked_modal').val($('#autorefresh_checked').val());
            $('#autorefreshtime_modal').val($('#autorefreshtime').val());

            $('#schicht_modal').val($('#schicht option:selected').val());
            $('#ds_modal').val($('#ds option:selected').val());
        };
    });
</script>
<!-- /.controller for checking if all required selections are made before submit insert/edit -->

<!-- insert/edit submit controller -->
<script type="text/javascript">
    $('#submit_form_modal_save').click(function(){
        if (submit==true){
            // change date to yesterday if time between 00:00 and 05:59 AND is Nachtschicht
            if (Number(current_schicht) == 3 &&  isBetween(Number(current_hour),0,5) == true){
                today = moment();
                yesterday = moment(today).subtract(1, 'days');
                yesterday = moment(yesterday).format('DD.MM.YYYY');
                $('#datum_tage_modal').val(yesterday);
            };
            $('#form_modal').attr('action', 'schichtprotokoll.store');
            $('#form_modal').submit();
        };
    });
    $('#submit_form_modal_edit').click(function(){
        if (submit==true){
            $('#form_modal').attr('action', 'schichtprotokoll.edit');
            $('#form_modal').submit();
        };
    });
</script>
<!-- /.insert/edit submit controller -->

<!-- delete selected error -->
<script type="text/javascript">
    $('.delete').click(function(){
        id = this.id.substring(7);
        if(confirm('Bist du sicher dass Sie diesen Störung löschen möchten?')){
            
            $('#delete').val(id);
            $('#halle_id_modal').val(<?= $halle_id;?>);
            $('#halle_id').val(<?= $halle_id;?>);
            $('select').prop('disabled', false);
            $('#datum_tage_modal').val($('.datum_tage').val());
            $('#autorefresh_checked_modal').val($('#autorefresh_checked').val());
            $('#autorefreshtime_modal').val($('#autorefreshtime').val());

            $('#schicht_modal').val($('#schicht option:selected').val());
            $('#ds_modal').val($('#ds option:selected').val());

            $('#form_modal').attr('action', 'schichtprotokoll.delete');
            $('#form_modal').submit();
        };
    });
</script>
<!-- /.delete selected error -->

<?php require ('app/views/partials/footer.php'); ?>