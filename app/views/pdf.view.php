<?php
namespace App\Controllers;
?>
<head>
    <title>TCPDF Batch</title>
</head>
<?php

use App\Core\App;
use TCPDF;
use DateTime;

$date		= new DateTime('-1 day');
$datum		= $date->format('d.m.Y');
$datum_tage_sql	= "TO_DATE('".$datum."','DD.MM.YYYY')";



$day		= "(DATUM = ".$datum_tage_sql." AND TO_TIMESTAMP(SCHICHT_BEGIN,'HH24:MI') BETWEEN TO_TIMESTAMP('06:00','HH24:MI') AND TO_TIMESTAMP('22:00','HH24:MI'))";
$columns	= 'ID_ERROR, STORUNG_ID, STORUNG_TEXT,	U1_ID, U1_TEXT, U2_ID, U2_TEXT,	U3_ID, U3_TEXT,	U4_ID, U4_TEXT, ART_ID, ART, TYP_ID, TYP, KOMENTAR,	LINIE, SCHICHT, SCHICHT_BEGIN, HALLE, VON, BIS, DAUER, date_format_yy (datum) as DATUM,	STANDARDSTORUNGSART, USER_ID, USERNAME';

$order		= 'SCHICHT, LINIE, to_number(DAUER) DESC';

function convertToHoursMins($time) {
    if ($time < 60) {
    	if ($time<10){$time = '0'.$time;}
        $vrijeme = '00:'.$time;
        return $vrijeme;
    }
    $hours = floor($time / 60);
    if ($hours<10){$hours = '0'.$hours;}
    $minutes = ($time % 60);
    if ($minutes<10){$minutes = '0'.$minutes;}
    $vrijeme = $hours.':'.$minutes;
    return $vrijeme;
}

/*
	start PDF export block
*/

$head='
	<!DOCTYPE html>
		<html lang="de">
		<head>
		    <title>TCPDF Batch</title>
		    <meta charset="ISO-8859-15">
		    <STYLE>
				* {
					font-size: 8px;
				}

				body {
					width: 100%;
				}

				table, th, tr, td {
					clear: both;
					width: 100%;
					border-collapse: collapse;
					border: 1px solid black;
					text-align: center;
					vertical-align: middle;
					padding: 2px;
				}

				.tfoot td {
					font-weight: bold;
					text-align: center;
				}

				.container {
					width: 100%;
					margin-right: auto;
			    	margin-left: auto;
				}
				
				.thead tr td, .caption {
					font-size: 12px;
					font-weight: bold;
					text-align: center;
					padding: 2px;
				}

				.caption {
					font-size: 10px;
				}
			</STYLE>
		</head>
		<body>';
$footer='</body>';

for ($j=3; $j<=5; $j++){
	$pdf = new TCPDF('L');  

	$pdf->setPrintHeader(false);
	$pdf->setPrintFooter(false);
	$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
	
	for ($i=0; $i <=2 ; $i++) { 
		$sum[$i.'T'] =0;
		$sum[$i.'O'] =0;
		$sum[$i.'Q'] =0;
	}

	
	$table ='';
	$thead = '';
	$tfoot = '';
	if($j == 5){$halle_txt = '4A';}else{$halle_txt = $j;}

	$all_arts = App::get('database')->SelectWhere('STR_TYPART', ['COLUMNS'=>'TEXT'], ['TYPE'=>"'A'", 'ACTIVE'=>"'1'"], '', 'Art');
	foreach ($all_arts as $key) {$all_art[] = $key->TEXT;}


	
	// set header information (Halle nr, Title, Date)
	$header ='
		<table class="thead">
			<tr>
				<td style="width: 15%;">Halle '.$halle_txt.'</td>
				<td style="width: 70%;">Schichtprotokoll</td>
				<td style="width: 15%;">'.$datum.'</td>
			</tr>
		</table>
		<BR>
	';
	
	foreach ($all_art as $key => $value) {
		$halle =  " AND HALLE='".$j."'";
		$where = "WHERE ".$day. " AND ART='".$value."'" . $halle;
		$alle_storungen[$key] = App::get('database')->SelectWhere2('VIEW_ERRORS', ['COLUMNS' => $columns], $where, $order, 'Errors');
	}

	for ($i=0; $i<=2; $i++){
		
		$dauer_sum = 0;
		$error_count = 0;
		$tr ='';
		
		if($i==0) $caption = '<div class="caption">Technisch</div>';
		if($i==1) $caption = '<div class="caption">Organisatorisch</div>';
		if($i==2) $caption = '<div class="caption">Qualit&auml;t</div>';
				
		$thead ='
		    <thead>
		        <tr>
		            <th style="width: 5%">Datum</th>
		            <th style="width: 8%">Schicht</th>
		            <th style="width: 4%">Linie</th>
		            <th style="width: 5%">Anfang</th>
		            <th style="width: 7%">Dauer (Min)</th>
		            <th style="width: 8%">St&ouml;rung</th>
		            <th style="width: 8%">Ebene 1</th>
		            <th style="width: 8%">Ebene 2</th>
		            <th style="width: 8%">Ebene 3</th>
		            <th style="width: 8%">Ebene 4</th>
		            <th style="width: 25%;">Bemerkung manuell</th>
		            <th style="width: 6%">Model</th>
		        </tr>
		    </thead>
		';
		
		foreach ($alle_storungen[$i] as $alle_storunge){
			
	       	$schicht_txt = "";
			$schicht = $alle_storunge->SCHICHT;

			$linie	= $alle_storunge->LINIE;
	 		$art	= $alle_storunge->ART;
	 		$dauer	= $alle_storunge->DAUER;
	 		$switch = $linie.$art;
	 		
	 		$dauer_sum = $dauer_sum + $dauer;
			$error_count = $error_count + 1;
	 		$sum[$switch] += $dauer;
			         	
			if($schicht==1) $schicht_txt = "Fr&uuml;hschicht";
			if($schicht==2) $schicht_txt = "Sp&auml;tschicht";
			if($schicht==3) $schicht_txt = "Nachtschicht";

			$tr .='
			    <tr>
					<td style="width: 5%">'.$alle_storunge->DATUM.'</td> 
			    	<td style="width: 8%">'.$schicht_txt.'</td>
			    	<td style="width: 4%">'.$alle_storunge->LINIE.'</td>
			    	<td style="width: 5%">'.$alle_storunge->VON.'</td>
			    	<td style="width: 7%">'.$alle_storunge->DAUER.'</td>
			    	<td style="width: 8%">'.utf8_encode($alle_storunge->STORUNG_TEXT).'</td>
			    	<td style="width: 8%">'.utf8_encode($alle_storunge->U1_TEXT).'</td>
			    	<td style="width: 8%">'.utf8_encode($alle_storunge->U2_TEXT).'</td>
			    	<td style="width: 8%">'.utf8_encode($alle_storunge->U3_TEXT).'</td>
			    	<td style="width: 8%">'.utf8_encode($alle_storunge->U4_TEXT).'</td>
			    	<td style="width: 25%">'.utf8_encode($alle_storunge->KOMENTAR).'</td>
			    	<td style="width: 6%">'.utf8_encode($alle_storunge->TYP).'</td>
			    </tr>
			';
		}

		$tfoot = '
			<table>
				<tr class="tfoot">
					<td style="width: 15%;">Dauer Sum: '.$dauer_sum.' Min</td>
					<td style="width: 70%;"></td>
					<td style="width: 15%;">Datens&auml;tze: '.$error_count.'</td>
			    </tr>
			</table>
		';
		if ($error_count !=0){
			$table .= $caption.'<table>'.$thead.'<tbody>'.$tr.'</tbody></table>'.$tfoot.'<BR><BR>';
		}
		

	}

	$sum_of_errors ='
			<div class="container" style="text-align: left;">
				<span><strong>Technisch</strong></span>
					<span>L0: '.convertToHoursMins($sum["0T"]).'</span>
					<span>L1: '.convertToHoursMins($sum["1T"]).'</span>
					<span>L2: '.convertToHoursMins($sum["2T"]).'</span>
				<span><strong> || Organisatorisch</strong></span>
					<span>L0: '.convertToHoursMins($sum["0O"]).'</span>
					<span>L1: '.convertToHoursMins($sum["1O"]).'</span>
					<span>L2: '.convertToHoursMins($sum["2O"]).'</span>
				<span><strong> || Qualit&auml;t</strong></span>
					<span>L0: '.convertToHoursMins($sum["0Q"]).'</span>
					<span>L1: '.convertToHoursMins($sum["1Q"]).'</span>
					<span>L2: '.convertToHoursMins($sum["2Q"]).'</span>
			</div>
	';

	$filename= "Schichtprotokoll_Halle{$halle_txt}_{$datum}.pdf"; 
	$filelocation = $_SERVER['TMP'];//"E:\\";
	$fileNL = $filelocation."\\".$filename;

	$pdf->AddPage();
	$data = $head.$header.$sum_of_errors.$table.$footer;

	$pdf->writeHTML($data, true, false, false, false, '');
	$pdf->Output($fileNL,'F');
}