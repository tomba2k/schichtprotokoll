<?php require ('app/views/partials/head.php'); ?>

<div class='row nonprintable'>
    <div class='container'>
        <h3>Schichtprotokoll</h3>
    </div>
    <form method='POST' role='form' id='form_tagesansicht' name='form_tagesansicht' action='reports.advancedreport'>
        <div class='row'>
            <div class='container'>
                <div class='col-xs-3'>
                    <div class='input-group' name='datum_tage' id='datum_tage'>
                        <span class='input-group-addon kursor'>Datum</span>
                        <input type='text' class='form-control datum_tage' name='datum_tage' id='datum_tage' value='<?= substr($datum_tage, 0, 10); ?>' style='min-width: 100px;'>
                        <span class='input-group-addon kursor' style='min-width: 10px;'><span class='glyphicon glyphicon-calendar' ></span></span>
                    </div>
                    <div class='input-group' name='halle_group' id='halle_group'>
                        <span class='input-group-addon'>Halle</span>
                        <select class='selectpicker halle' data-width='100%' id='halle' name='halle'>
                            <option value=3 <?php if ($halle_id == 3) {
    echo 'selected';
} ?>>Halle 3</option>
                            <option value=4 <?php if ($halle_id == 4) {
    echo 'selected';
} ?>>Halle 4</option>
                            <option value=5 <?php if ($halle_id == 5) {
    echo 'selected';
} ?>>Halle 4A</option>
                        </select>
                    </div>
                </div>
                <div class='col-xs-3'>
                    <div class='col-xs-3'>
                        <div><button type="submit" name='submit_form_tagesansicht' id='submit_form_tagesansicht' class="btn btn-success">Daten holen</button></div>
                    </div>
                </div>
                <div class='col-xs-6' style='text-align: right;'>
                    <a href="#" style='color: inherit;'><i id='submit_form_exportCSV' name='submit_form_exportCSV' class="fa fa-file-excel-o fa-2x" aria-hidden="true"></i></a>
                    &nbsp;
                    <span id='print' name='print' class="glyphicon glyphicon-print kursor print" aria-hidden="true" title='Dr�cken'></span>
                </div>

            </div>
        </div>
    </form>
</div>

<form method='POST' role='form' id='form_exportCSV' name='form_exportCSV' action='reports.schichtprotokoll.exportCSV'>
    <input type='hidden' name='report' id='report' value='schichtprotokoll'>
    <input type='hidden' name='datum_tage' id='datum_tage' value='<?= substr($datum_tage, 0, 10); ?>'>
    <input type='hidden' name='halle' id='halle' value='<?= $halle_id; ?>'>
</form>

<div class='row nonprintable'><div class='container'><HR></div></div>
<?php
if ($halle_id == 3)
    $halle_txt = '3';
if ($halle_id == 4)
    $halle_txt = '4';
if ($halle_id == 5)
    $halle_txt = '4A';
?>

<div class='row'>
    <div class='container printable' style="display: none;">
        <div class='table-responsive'>
            <table class='table table_header' style='width: 100% !important;'>
                <tr>
                    <td style='width: 15%;'><h4 style='color: #337ab7;'>Halle <?= $halle_txt ?></h4></td>
                    <td style='width: 70%;'><h1>Schichtprotokoll</h1></td>
                    <td style='width: 15%;'><h4 style='color: #337ab7;'><?= substr($datum_tage, 0, 10); ?></h4></td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class='row nonprintable'>
    <div class='container'>
        <div class='col-xs-6' style='padding-left: 0px;'>
            <h4 style='color: #337ab7;'>Datum: <?= substr($datum_tage, 0, 10); ?></h4>
        </div>
        <div class='col-xs-6' style='padding-right: 0px; text-align: right;'>
            <h4 style='color: #337ab7;'>Halle <?= $halle_txt ?></h4>
        </div>
    </div>
</div>

<div class='row'>
    <div class='container printable' style='text-align: left;'>
        <span><strong>Technisch</strong></span>
        <span>L0: <span id='0T' name='0T'></span></span>
        <span>L1: <span id='1T' name='1T'></span></span>
        <span>L2: <span id='2T' name='2T'></span></span>
        <span><strong> || Organisatorisch</strong></span>
        <span>L0: <span id='0O' name='0O'></span></span>
        <span>L1: <span id='1O' name='1O'></span></span>
        <span>L2: <span id='2O' name='2O'></span></span>
        <span><strong> || Qualit�t</strong></span>
        <span>L0: <span id='0Q' name='0Q'></span></span>
        <span>L1: <span id='1Q' name='1Q'></span></span>
        <span>L2: <span id='2Q' name='2Q'></span></span>
    </div>
</div>


<style>

    .bggray{background: gray;}
    .bgdarkgray{background: #424A53;}
    .bgdarkblue{background: #215B87;}
    .bgwhite{background: #fff;}
    .fontwhite{color:#fff;}
    .fontOrange{color: #FFA500; border:solid 1px #5b5b5a;}
    .fontOrange > td {border:solid 1px #5b5b5a;}
    .fontRed > td {border:solid 1px #5b5b5a;}
    .fontRed{color: #F00; border:solid 1px #5b5b5a;}
    .px25widthcell{width:20px !important;padding: 0px !important}
    .px25heightcell{width:20px;}
    .tableAdvanced{
        font-family: Arial;
        font-size: 12px;
       
        white-space: nowrap;
    }
    .tableAdvanced > tr > td{padding: 0px !important;}
    .tableAdvanced > tr{padding: 0px !important;}
    .tableAdvanced > tr{padding: 0px !important;}
    

    .outer {
        height: 76px;
        width: 26px;
        position: relative;
        display: inline-block;

        float:left;
    }
    .outer2 {
        height: 76px;
        width: 80px;
        position: relative;
        display: inline-block;

        float:left;
       
    }
    .outer3 {
        position: relative;
        display: inline-block;

        float:left;
       
    }
span{border:0px;}
    .inner {
        font-size: 13px;
        font-color: #878787;
        position: relative;
        top: 50%;
        left: 50%;

        white-space: nowrap;
        float: left;
    }
    .inner2 {
        font-size: 13px;
        font-color: #878787;
        font-weight: bolder;
        position: relative;
        top: 0%;
        left: 13%;

        white-space: nowrap;
        float: left;
    }
    .innertotal {
        font-size: 13px;
        font-color: #878787;
        position: relative;
        top: 0%;
        left: 0%;

        white-space: nowrap;
        float: left;
    }
    .inner3 {
        font-size: 13px;
        font-color: #878787;
        position: relative;
        top: 0%;
        left: 0%;
        margin-left :-15px;
        margin-right :-15px;
        white-space: nowrap;
        float: left;
    }            
    
    .inner4 {
        font-size: 13px;
        font-color: #878787;
        position: relative;
        top: 0%;
        left: 0%;
        margin-left :-20px;
        margin-right :-20px;
        white-space: nowrap;
        float: left;
    }   
    .innerNacharbeit {
        font-size: 13px;
        font-color: #878787;
        position: relative;
        margin-left :-17px;
        margin-right :-17px;
        top: 0%;
        left: 0%;
        white-space: nowrap;
        float: left;
    }  
    .innerRohSkid {
        font-size: 13px;
        font-color: #878787;
        position: relative;
        margin-left :-14px;
        margin-right :-14px;
        top: 0%;
        left: 0%;
        white-space: nowrap;
        float: left;
    }    
    .innerH2 {
        font-size: 13px;
        font-color: #878787;
        position: relative;
        margin-left :4px;
        margin-right :4px;
        top: 0%;
        left: 0%;
        white-space: nowrap;
        float: left;
    }    
    .innerFuller {
        font-size: 13px;
        font-color: #878787;
        position: relative;
        margin-left :-4px;
        margin-right :-4px;
        top: 0%;
        left: 0%;
        white-space: nowrap;
        float: left;
    }    
    .innerSonzt {
        font-size: 13px;
        font-color: #878787;
        position: relative;
        margin-left :-5px;
        margin-right :-5px;
        top: 0%;
        left: 0%;
        white-space: nowrap;
        float: left;
    }          
    .innerLinie {
        font-size: 13px;
        font-color: #878787;
        position: relative;
        top: 0%;
        left: 0%;
        white-space: nowrap;
        float: left;
    }       
    .innerOrga {
        font-size: 13px;
        font-color: #878787;
        position: relative;
        top: 0%;
        left: 0%;
        white-space: nowrap;
        float: left;
    }   
    .innerUhrzeit {
        font-size: 13px;
        font-color: #878787;
        position: relative;
        margin-left :-3px;
        margin-right :-3px;
        top: 0%;
        left: 0%;
        white-space: nowrap;
        float: left;
    }

    .rotate {
        -moz-transform: translateX(0%) translateY(0%) rotate(-90deg);
        -webkit-transform: translateX(0%) translateY(0%) rotate(-90deg);
        transform:  translateX(0%) translateY(0%) rotate(-90deg);
    }
    .fullborder{}
    .noborder{border: 0px !important;}
</style>

<table class="tableAdvanced" >
    <thead  style="background: #FFF;">
        <tr class="bggray">
            <td colspan="16" class="fontwhite" style="font-size:22px; text-align: center;">Halle 3</td>
            <td colspan="7" class="fontwhite" style="font-size:22px; text-align: center;">Schichtprotokoll</td>
            <td class="fontwhite" style="font-size:22px; text-align: center; width:150px;"><?php echo $datum_tage;?></td>
        </tr>
        <tr style="background: #215B87; border: solid #000 1px;">
            <td rowspan="2" class="px25widthcell" ></td>
            <td rowspan="2" class="rotate fontwhite px25widthcell">VBH</td>
            <td rowspan="2" class="rotate fontwhite px25widthcell">KTL</td>
            <td rowspan="2" class="rotate fontwhite px25widthcell">GAD</td>
            <td rowspan="2" class="rotate fontwhite px25widthcell">UBS</td>
            <td rowspan="2" class="fontwhite px25widthcell"><span class="inner3 rotate">Schweller</span></td>
            <td rowspan="2" class="rotate fontwhite px25widthcell">SDM</td>
            <td rowspan="2" class="rotate fontwhite px25widthcell">FAD</td>
            <td rowspan="1" colspan="3" style="text-align: center; border: solid 1px #000;" class=" fontwhite">Speicher</td>
            <td rowspan="1" colspan="2" style="text-align: center" class=" fontwhite">Trockner</td>
            <td rowspan="2" class="fontwhite px25widthcell"><span class="inner4 rotate ">PVC Versor.</span></td>
            <td rowspan="1" colspan="3" style="text-align: center" class=" fontwhite">R�ckf�hrung</td>
            <td rowspan="2" class=" fontwhite px25widthcell"><span class="innerNacharbeit rotate ">Nacharbeit</span></td>
            <td rowspan="2" class=" fontwhite px25widthcell"><span class="innerSonzt rotate ">Sonst.</span></td>
            <td rowspan="2" class=" fontwhite px25widthcell" style="background:#BFBFBF;color:#000;"><span class="innerOrga rotate ">Orga</span></td>
            <td rowspan="2" class=" fontwhite px25widthcell"><span class="innerUhrzeit rotate ">Uhrzeit</span></td>
            <td rowspan="2" class=" fontwhite px25widthcell"><span class="innerLinie rotate ">Linie</span></span></td>
            <td rowspan="2" class=" fontwhite px25widthcell" style="text-align: center; min-width:350px;">St�rungsbereich</td>
            <td rowspan="2" class=" fontwhite px25widthcell" style="text-align: center;min-width:350;">Bemerkung</td>
        </tr>
        <tr  style="background: #215B87; border: solid #000 1px;">   
            <td class=" fontwhite px25widthcell" style="height: 70px;"><span class="innerH2 rotate ">H2</span></td>
            <td class="rotate fontwhite px25widthcell">KTL</td>
            <td class="fontwhite px25widthcell"><span class="innerFuller rotate ">Füller</span></td>
            <td class="rotate fontwhite px25widthcell">KTL</td>
            <td class="rotate fontwhite px25widthcell">UBS</td>
            <td class=" fontwhite px25widthcell"><span class="innerRohSkid rotate ">Roh.Skid</span></td>
            <td class="rotate fontwhite px25widthcell">EHB</td>
            <td class=" fontwhite px25widthcell"><span class="innerRohSkid rotate ">Lackskid</span></td>
        </tr>
    </thead>
    <tbody style="border: solid #000 1px !important;">
        <!-- 1. smjena  -->
            <?php foreach ($report->Sch as $schicht) {
                foreach ($schicht->errors as $key => $err) {
                    ?>

                <tr style="text-align: center;  border: solid #000 1px; " class="bgwhite 
                    <?php if($err->value >= 10 && $err->value < 100){echo "fontOrange";}elseif($err->value >= 100){echo "fontRed";}?>
                    ">
        <?php if ($key == 0) { ?>
                    <td rowspan="2" class="bgdarkblue fontwhite nobborder" >DS2</td>
        <?php }
        if ($key == 2) {
            ?>
                        <td rowspan="<?php echo $schicht->GetLabelRowspan() ?>" class=" bgdarkblue fontwhite px25heightcell notborder"style="border-top: 0px !important;"><span class="inner2 rotate"><?php
						switch ($err->schicht){
							case 1:
							echo "FS";
							break;
							case 2:
							echo "SS";
							break;
							case 3:
							echo "NS";
							break;
							default:
							break;
						}
						?></span></td>
        <?php } ?>
                    <td><?php echo $err->VBH; ?></td>
                    <td><?php echo $err->KTL; ?></td>
                    <td><?php echo $err->GAD; ?></td>
                    <td><?php echo $err->UBS; ?></td>
                    <td><?php echo $err->Schweller; ?></td>
                    <td><?php echo $err->SDM; ?></td>
                    <td><?php echo $err->FAD; ?></td>
                    <td><?php echo $err->SpeicherH2; ?></td>
                    <td><?php echo $err->SpeicherKTL; ?></td>
                    <td><?php echo $err->SpeicherFuller; ?></td>
                    <td><?php echo $err->TrocknerKTL; ?></td>
                    <td><?php echo $err->TrocknerUBS; ?></td>
                    <td><?php echo $err->PVCVersor; ?></td>
                    <td><?php echo $err->RuckführungRohSkid; ?></td>
                    <td><?php echo $err->RuckführungEHBLackskid; ?></td>
                    <td><?php echo $err->RuckführungLackskid; ?></td>
                    <td><?php echo $err->Nacharbeit; ?></td>
                    <td><?php echo $err->Sonst; ?></td>
                    <td  style="background:#BFBFBF;color:#000;"><?php echo $err->Orga; ?></td>
                    <td><?php echo $err->Uhrzeit; ?></td>
                    <td><?php echo $err->Linie; ?></td>
                    <td><?php echo $err->Storungsbereich; ?></td>
                    <td><?php echo $err->Bemerkung; ?></td>
                </tr>
    <?php } ?>
            <!--SumRow-->
            <tr style="text-align: center;  border: solid #000 1px; " class="bggray fontwhite">
                <td class="bgdarkblue fontwhite">Sum</td>
                <td><?php echo $schicht->sumRow->VBH; ?></td>
                <td><?php echo $schicht->sumRow->KTL; ?></td>
                <td><?php echo $schicht->sumRow->GAD; ?></td>
                <td><?php echo $schicht->sumRow->UBS; ?></td>
                <td><?php echo $schicht->sumRow->Schweller; ?></td>
                <td><?php echo $schicht->sumRow->SDM; ?></td>
                <td><?php echo $schicht->sumRow->FAD; ?></td>
                <td><?php echo $schicht->sumRow->SpeicherH2; ?></td>
                <td><?php echo $schicht->sumRow->SpeicherKTL; ?></td>
                <td><?php echo $schicht->sumRow->SpeicherFuller; ?></td>
                <td><?php echo $schicht->sumRow->TrocknerKTL; ?></td>
                <td><?php echo $schicht->sumRow->TrocknerUBS; ?></td>
                <td><?php echo $schicht->sumRow->PVCVersor; ?></td>
                <td><?php echo $schicht->sumRow->RuckführungRohSkid; ?></td>
                <td><?php echo $schicht->sumRow->RuckführungEHBLackskid; ?></td>
                <td><?php echo $schicht->sumRow->RuckführungLackskid; ?></td>
                <td><?php echo $schicht->sumRow->Nacharbeit; ?></td>
                <td><?php echo $schicht->sumRow->Sonst; ?></td>
                <td><?php echo $schicht->sumRow->Orga; ?></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
<?php } ?>
        <tr style="text-align: center;  border: solid #000 1px; height: 40px; " class="bgdarkgray fontwhite">
            <td class="fontwhite"><span class="innertotal rotate fontwhite">Total</span></td>
            <td><?php echo $report->Sum->VBH; ?></td>
            <td><?php echo $report->Sum->KTL; ?></td>
            <td><?php echo $report->Sum->GAD; ?></td>
            <td><?php echo $report->Sum->UBS; ?></td>
            <td><?php echo $report->Sum->Schweller; ?></td>
            <td><?php echo $report->Sum->SDM; ?></td>
            <td><?php echo $report->Sum->FAD; ?></td>
            <td><?php echo $report->Sum->SpeicherH2; ?></td>
            <td><?php echo $report->Sum->SpeicherKTL; ?></td>
            <td><?php echo $report->Sum->SpeicherFuller; ?></td>
            <td><?php echo $report->Sum->TrocknerKTL; ?></td>
            <td><?php echo $report->Sum->TrocknerUBS; ?></td>
            <td><?php echo $report->Sum->PVCVersor; ?></td>
            <td><?php echo $report->Sum->RuckführungRohSkid; ?></td>
            <td><?php echo $report->Sum->RuckführungEHBLackskid; ?></td>
            <td><?php echo $report->Sum->RuckführungLackskid; ?></td>
            <td><?php echo $report->Sum->Nacharbeit; ?></td>
            <td><?php echo $report->Sum->Sonst; ?></td>
            <td><?php echo $report->Sum->Orga; ?></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </tbody>

</table>

<BR>
<BR>
<?php //endfor;  ?>

<?php require ('app/views/partials/footer_vendor_scripts.php'); ?>

<!-- initalize selectpicker -->
<script type='text/javascript'>
    $('.selectpicker').selectpicker({
        size: 4
    });
</script>
<!-- /.initalize selectpicker -->

<!-- configure dateTime Picker -->
<script type='text/javascript'>
    var today = new Date();
    var format_date = 'DD.MM.YYYY';
    var viewMode = 'days';

    $('#datum_tage').datetimepicker({
        viewMode: viewMode,
        locale: moment.locale('de'),
        format: format_date,
        useCurrent: false,
        extraFormats: false,
        sideBySide: true
    });
</script>
<!-- /.configure dateTime Picker -->

<!-- activate dataTable -->
<script type='text/javascript'>
    $(document).ready(function () {
        $.fn.dataTable.moment('DD.MM.YYYY');
        $('.myDataTable').DataTable({
            searching: false,
            paging: false,
            info: false,
            ordering: false,
            pageLength: 50,
            lengthMenu: [[50, 150, 250, -1], [50, 150, 250, "Alle"]],
            language: {
                emptyTable: 'Keine Daten in der Tabelle vorhanden',
                info: '_START_ bis _END_ von _TOTAL_ Eintr�gen',
                infoEmpty: '0 bis 0 von 0 Eintr�gen',
                infoFiltered: '(gefiltert von _MAX_ Eintr�gen)',
                infoPostFix: '',
                thousands: '.',
                lengthMenu: '_MENU_ Eintr�ge anzeigen',
                loadingRecords: 'Wird geladen...',
                processing: 'Bitte warten...',
                search: 'Suchen',
                zeroRecords: 'Keine Eintr�ge vorhanden.',
                paginate: {
                    first: 'Erste',
                    previous: 'Zur�ck',
                    next: 'N�chste',
                    last: 'Letzte'
                },
                aria: {
                    sortAscending: ': aktivieren, um Spalte aufsteigend zu sortieren',
                    sortDescending: ': aktivieren, um Spalte absteigend zu sortieren'
                },
                select: {
                    rows: {
                        _: '%d Zeilen ausgew�hlt',
                        0: 'Zum Ausw�hlen auf eine Zeile klicken',
                        1: '1 Zeile ausgew�hlt'
                    }
                }
            }
        });
    });
</script>
<!-- /.activate dataTable -->
<!-- open print dialog on print icon click -->
<script type="text/javascript">
    $('#print').click(function () {
        window.print();
    });
</script>
<!-- /.open print dialog on print icon click -->

<!-- submit exportCSV form -->
<script type="text/javascript">
    $('#submit_form_exportCSV').click(function () {
        $('#form_exportCSV').submit();
    });
</script>
<!-- /.submit exportCSV form -->

<script type="text/javascript">
    var sum = <?= json_encode($sum); ?>;
    var dauerSum = 0;
    for (i = 0; i <= 2; i++) {
        dauerSum = $('.dauerSum' + i).data('value');
        if (dauerSum == 0) {
            $('.myDataTable_' + i).hide();
        }
    }
    ;

    function gethoursMinutes(i) {
        var hours = Math.floor(i / 60);
        var minutes = i % 60;

        if (minutes < 10) {
            minutes = '0' + minutes.toString();
        }
        ;
        hoursMinutes = hours.toString() + ':' + minutes;

        return hoursMinutes;
    }
    ;

    for (i = 0; i <= 2; i++) {
        $('#' + i + 'T').text(gethoursMinutes(sum[i + 'T']));
        $('#' + i + 'O').text(gethoursMinutes(sum[i + 'O']));
        $('#' + i + 'Q').text(gethoursMinutes(sum[i + 'Q']));
    }
    ;
</script>

<?php require ('app/views/partials/footer.php'); ?>