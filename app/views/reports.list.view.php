<?php require ('app/views/partials/head.php'); ?>

<div class='row nonprintable'>
    <div class='container'>
        <h3>Schichtprotokoll</h3>
    </div>
    <form method='POST' role='form' id='form_tagesansicht' name='form_tagesansicht' action='reports.listreport'>
        <div class='row'>
            <div class='container'>
                <div class='col-xs-3'>
                    <div class='input-group' name='datum_tage' id='datum_tage'>
                        <span class='input-group-addon kursor'>Datum</span>
                        <input type='text' class='form-control datum_tage' name='datum_tage' id='datum_tage' value='<?= substr($datum_tage, 0, 10); ?>' style='min-width: 100px;'>
                        <span class='input-group-addon kursor' style='min-width: 10px;'><span class='glyphicon glyphicon-calendar' ></span></span>
                    </div>
                    <div class='input-group' name='halle_group' id='halle_group'>
                        <span class='input-group-addon'>Halle</span>
                        <select class='selectpicker halle' data-width='100%' id='halle' name='halle'>
                            <option value=3 <?php
                            if ($halle_id == 3) {
                                echo 'selected';
                            }
                            ?>>Halle 3</option>
                            <option value=4 <?php
                            if ($halle_id == 4) {
                                echo 'selected';
                            }
                            ?>>Halle 4</option>
                            <option value=5 <?php
                            if ($halle_id == 5) {
                                echo 'selected';
                            }
                            ?>>Halle 4A</option>
                        </select>
                    </div>
                </div>
                <div class='col-xs-3'>
                    <div class='col-xs-3'>
                        <div><button type="submit" name='submit_form_tagesansicht' id='submit_form_tagesansicht' class="btn btn-success">Daten holen</button></div>
                    </div>
                </div>
                <div class='col-xs-6' style='text-align: right;'>
                    <a href="#" style='color: inherit;'><i id='submit_form_exportCSV' name='submit_form_exportCSV' class="fa fa-file-excel-o fa-2x" aria-hidden="true"></i></a>
                    &nbsp;
                    <span id='print' name='print' class="glyphicon glyphicon-print kursor print" aria-hidden="true" title='Dr�cken'></span>
                </div>

            </div>
        </div>
    </form>
</div>

<form method='POST' role='form' id='form_exportCSV' name='form_exportCSV' action='reports.schichtprotokoll.exportCSV'>
    <input type='hidden' name='report' id='report' value='schichtprotokoll'>
    <input type='hidden' name='datum_tage' id='datum_tage' value='<?= substr($datum_tage, 0, 10); ?>'>
    <input type='hidden' name='halle' id='halle' value='<?= $halle_id; ?>'>
</form>

<div class='row nonprintable'><div class='container'><HR></div></div>
<?php
if ($halle_id == 3)
    $halle_txt = '3';
if ($halle_id == 4)
    $halle_txt = '4';
if ($halle_id == 5)
    $halle_txt = '4A';
?>

<div class='row'>
    <div class='container nonprintable' style="display: none;">
        <div class='table-responsive'>
            <table class='table table_header' style='width: 100% !important;'>
                <tr>
                    <td style='width: 15%;'><h4 style='color: #337ab7;'>Halle <?= $halle_txt ?></h4></td>
                    <td style='width: 70%;'><h1>Schichtprotokoll</h1></td>
                    <td style='width: 15%;'><h4 style='color: #337ab7;'><?= substr($datum_tage, 0, 10); ?></h4></td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class='row nonprintable'>
    <div class='container'>
        <div class='col-xs-6' style='padding-left: 0px;'>
            <h4 style='color: #337ab7;'>Datum: <?= substr($datum_tage, 0, 10); ?></h4>
        </div>
        <div class='col-xs-6' style='padding-right: 0px; text-align: right;'>
            <h4 style='color: #337ab7;'>Halle <?= $halle_txt ?></h4>
        </div>
    </div>
</div>
<style>

    .bggray{background: gray;}
    .bgdarkgray{background: #424A53;}
    .bgdarkblue{background: #215B87;}
    .bgwhite{background: #fff;}
    .fontwhite{color:#fff;}
    .fontOrange{color: #FFA500}
    .fontRed{color: #F00}
    .fontBlack{color: #000}
    .px25widthcell{width:20px !important;padding: 0px !important; border: solid #000 1px;}
    .px25heightcell{width:20px;}
    .tableAdvanced{
        font-family: Arial;
        font-size: 12px;
        background: #000 !important;
        white-space: nowrap;
    }
    .graya{background:#BFBFBF;color:#000;}
    table.tableAdvanced > td{padding: 0px !important;}
    table.tableAdvanced > tr{padding: 0px !important;}
    tbody  td { border: solid #b5b5b4 1px }
    .tal(
        text-align: left !important;
    )
    .outer {
        height: 76px;
        width: 26px;
        position: relative;
        display: inline-block;

        float:left;
        border: solid 1px;
    }
    .outer2 {
        height: 76px;
        width: 80px;
        position: relative;
        display: inline-block;

        float:left;
        border: solid 1px;
    }
    .outer3 {
        position: relative;
        display: inline-block;

        float:left;
        border: solid 1px;
    }

    .inner {
        font-size: 13px;
        font-color: #878787;
        position: relative;
        top: 50%;
        left: 50%;

        white-space: nowrap;
        float: left;
    }
    .inner2 {
        font-size: 13px;
        font-color: #878787;
        font-weight: bolder;
        position: relative;
        top: 0%;
        left: 13%;

        white-space: nowrap;
        float: left;
    }
    .innertotal {
        font-size: 13px;
        font-color: #878787;
        position: relative;
        top: 0%;
        left: 0%;

        white-space: nowrap;
        float: left;
    }
    .inner3 {
        font-size: 13px;
        font-color: #878787;
        position: relative;
        top: 0%;
        left: 0%;
        margin-left :-15px;
        margin-right :-15px;
        white-space: nowrap;
        float: left;
    }            

    .inner4 {
        font-size: 13px;
        font-color: #878787;
        position: relative;
        top: 0%;
        left: 0%;
        margin-left :-23px;
        margin-right :-23px;
        white-space: nowrap;
        float: left;
    }   

    .rotate {
        -moz-transform: translateX(0%) translateY(0%) rotate(-90deg);
        -webkit-transform: translateX(0%) translateY(0%) rotate(-90deg);
        transform:  translateX(0%) translateY(0%) rotate(-90deg);
    }
    .nobborder{border-bottom: 0px !important;}
    .notborder{border-top: 0px !important;}
</style>

<table class="tableAdvanced" >
    <thead  style="background: #FFF;">
        <tr class="bggray">
            <td colspan="4" class="fontwhite" style="font-size:22px; text-align: center;">Halle 3</td>
            <td colspan="2" class="fontwhite" style="font-size:22px; text-align: center;">Schichtprotokoll</td>
            <td class="fontwhite" style="font-size:22px; text-align: center; width:150px;"><?php echo $datum_tage; ?></td>
        </tr>
        <tr style="background: #215B87; ">
            <td ></td>
            <td class="fontwhite" >Bereich</td>
            <td class="fontwhite" >Dauer</td>
            <td class="fontwhite" >Uhrzeit</td>
            <td class="fontwhite" >Linie</td>
            <td class="fontwhite" >Unterbereich</td>
            <td class="fontwhite" style="min-width:300;max-width:300;">Bemerkung</td>

        </tr>
    </thead>
    <tbody style="">

        <?php
		
		//print_r($report);
        foreach ($report->Sch as $schicht) {
            $cnt = 0;
            foreach ($schicht->errors as $kex => $err) {
                $cnt += count($err);
            }
            $first = true;
            $schichtCounter = 0;
            $rowCounter = 0;
			
            foreach ($schicht->errors as $kex => $val) {
               // foreach ($err as $key => $val) {
                    ?>

                    <tr style="text-align: center;  " class="bgwhite 
                    <?php
                    if($val->name != "Orga"){
                        if ($val->dauer >= 10 && $val->dauer < 100) {
                            echo "fontOrange";
                        } elseif ($val->dauer >= 100) {
                            echo "fontRed";
                        }
                    } else {
                        echo " graya";
                    }
                    ?>
                        ">
                        <?php 
                        if ($kex == 0 && $first) { ?>
                            <td rowspan="2" class="bgdarkblue fontwhite noborder" ><?php echo "DS", $schicht->team;
                                    ?></td>
                            <?php
                        }
                        if ($rowCounter == 2 ) {
                            ?>
                            <td rowspan="<?php echo ($cnt > 2) ? $cnt - 2 : 1 ?>" class=" bgdarkblue fontwhite px25heightcell notborder"style="border-top: 0px !important;">
                                <span class="inner2 rotate"><?php
                                    switch ($val->schicht) {
                                        case 1:
                                            echo "FS";
                                            break;
                                        case 2:
                                            echo "SS";
                                            break;
                                        case 3:
                                            echo "NS";
                                            break;
                                        default:
                                            break;
                                    }
                                    ?>
                                </span>
                            </td>
                        <?php } ?>
                        <?php 
                        ?><td class=" fontBlack "><?php if($kex == 0){echo $val->name; }?></td>
                        <td><?php echo $val->dauer; ?></td>
                        <td><?php echo $val->Uhrzeit; ?></td>
                        <td><?php echo $val->Linie; ?></td>
                        <td class="tal"><?php echo $val->Storungsbereich; ?></td>
                        <td class="tal" style = "white-space: normal;"><?php echo $val->Bemerkung; ?></td>
                    </tr>
                    <?php 
                    $first = false;
                    $rowCounter++;
                //}
            
                $schichtCounter++; 
            }
			$schicht->fixMissingRows2($schicht);  ?>
            <tr><td colspan="7" class="bgdarkblue"> </td></tr>
        <?php } 
		
			
		?>

    </tbody>

</table>

<BR>
<BR>
<?php //endfor;    ?>

<?php require ('app/views/partials/footer_vendor_scripts.php'); ?>

<!-- initalize selectpicker -->
<script type='text/javascript'>
    $('.selectpicker').selectpicker({
        size: 4
    });
</script>
<!-- /.initalize selectpicker -->

<!-- configure dateTime Picker -->
<script type='text/javascript'>
    var today = new Date();
    var format_date = 'DD.MM.YYYY';
    var viewMode = 'days';

    $('#datum_tage').datetimepicker({
        viewMode: viewMode,
        locale: moment.locale('de'),
        format: format_date,
        useCurrent: false,
        extraFormats: false,
        sideBySide: true
    });
</script>
<!-- /.configure dateTime Picker -->

<!-- activate dataTable -->
<script type='text/javascript'>
    $(document).ready(function () {
        $.fn.dataTable.moment('DD.MM.YYYY');
        $('.myDataTable').DataTable({
            searching: false,
            paging: false,
            info: false,
            ordering: false,
            pageLength: 50,
            lengthMenu: [[50, 150, 250, -1], [50, 150, 250, "Alle"]],
            language: {
                emptyTable: 'Keine Daten in der Tabelle vorhanden',
                info: '_START_ bis _END_ von _TOTAL_ Eintr�gen',
                infoEmpty: '0 bis 0 von 0 Eintr�gen',
                infoFiltered: '(gefiltert von _MAX_ Eintr�gen)',
                infoPostFix: '',
                thousands: '.',
                lengthMenu: '_MENU_ Eintr�ge anzeigen',
                loadingRecords: 'Wird geladen...',
                processing: 'Bitte warten...',
                search: 'Suchen',
                zeroRecords: 'Keine Eintr�ge vorhanden.',
                paginate: {
                    first: 'Erste',
                    previous: 'Zur�ck',
                    next: 'N�chste',
                    last: 'Letzte'
                },
                aria: {
                    sortAscending: ': aktivieren, um Spalte aufsteigend zu sortieren',
                    sortDescending: ': aktivieren, um Spalte absteigend zu sortieren'
                },
                select: {
                    rows: {
                        _: '%d Zeilen ausgew�hlt',
                        0: 'Zum Ausw�hlen auf eine Zeile klicken',
                        1: '1 Zeile ausgew�hlt'
                    }
                }
            }
        });
    });
</script>
<!-- /.activate dataTable -->
<!-- open print dialog on print icon click -->
<script type="text/javascript">
    $('#print').click(function () {
        window.print();
    });
</script>
<!-- /.open print dialog on print icon click -->

<!-- submit exportCSV form -->
<script type="text/javascript">
    $('#submit_form_exportCSV').click(function () {
        $('#form_exportCSV').submit();
    });
</script>
<!-- /.submit exportCSV form -->

<script type="text/javascript">
    var sum = <?= json_encode($sum); ?>;
    var dauerSum = 0;
    for (i = 0; i <= 2; i++) {
        dauerSum = $('.dauerSum' + i).data('value');
        if (dauerSum == 0) {
            $('.myDataTable_' + i).hide();
        }
    }
    ;

    function gethoursMinutes(i) {
        var hours = Math.floor(i / 60);
        var minutes = i % 60;

        if (minutes < 10) {
            minutes = '0' + minutes.toString();
        }
        ;
        hoursMinutes = hours.toString() + ':' + minutes;

        return hoursMinutes;
    }
    ;

    for (i = 0; i <= 2; i++) {
        $('#' + i + 'T').text(gethoursMinutes(sum[i + 'T']));
        $('#' + i + 'O').text(gethoursMinutes(sum[i + 'O']));
        $('#' + i + 'Q').text(gethoursMinutes(sum[i + 'Q']));
    }
    ;
</script>

<?php require ('app/views/partials/footer.php'); ?>