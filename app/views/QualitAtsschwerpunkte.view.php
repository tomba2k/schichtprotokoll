<?php require ('app/views/partials/head.php'); ?>

<form method="POST" role="form" id="form_saveQualitatsschwerpunkte" name="form_saveQualitatsschwerpunkte" action="qualitatsschwerpunkte.store">
    <div class='container'>
        <div class='row'>
            <h3>Qualit�tsschwerpunkte</h3>
            <div class='form-group col-xs-2'>
                <div class="input-group date" name="datum_tage" id="datum_tage_">
                    <span class="input-group-addon">Datum</span>
                    <input type="text" class="form-control datum_tage" name="datum_tage" id="datum_tage" value="<?= substr($datum_tage, 0,10);?>"  style="min-width: 100px;">
                    <span class="input-group-addon" style="min-width: 10px;"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
            </div>

        </div>

        <div class='row'>
            <div class='form-group col-xs-6'>
                <div class='input-group halle' id='halle_group'>
                    <span class='input-group-addon'>Halle</span>
                    <select class='selectpicker' data-width='100%' id='halle' name='halle'>
                        <option value='3'>3</option>
                        <option value='4'>4</option>
                        <option value='5'>4A</option>
                    </select>
                </div>
                
            </div>
        </div>

        <div class='row'>
            <div class='form-group col-xs-6'>
                <div class='input-group bemerkung' id='bemerkung_group'>
                    <span class='input-group-addon'>Bemerkung</span>
                    <input type='text' class='form-control' placeholder='Bemerkung' id='bemerkung' name='bemerkung' aria-describedby='username_group'>
                </div>
            </div>
        </div>


        <div class='row'>
            <div class="form-group col-xs-3">
                <button type="submit" class="btn btn-info" id="submit_storeQualitatsschwerpunkte" name="submit_storeQualitatsschwerpunkte">Speichern</button>
            </div>
        </div>
    </div>
</form>

<div class='container'><HR></div>

<div class='container'>
    <div class='table-responsive'>
        <table id='myDataTable' class='table table-striped table-bordered table-hover schichten myDataTable' style='cellspacing: 0; width: 70%;'>
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Datum</th>
                    <th>Halle</th>
                    <th>Bemerkung</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($qualitatsschwerpunkte as $item): ?>
                    <tr>
                        <?php foreach ($item as $key => $value): ?>
                          
                                <td class='<?= $key; ?>' id='<?= $key; ?>'><?= $value; ?></td>

                                    

                        <?php endforeach; ?>
                    <td>
						<button type='button' name='btn_editQualitatsschwerpunkte' id='btn_editQualitatsschwerpunkte' class='btn btn-xs btn-success btn_editQualitatsschwerpunkte' value='btn-editQualitatsschwerpunkte'>Edit</button>
						<button type="button" name="btn-deleteQualitatsschwerpunkte" id="btn-deleteQualitatsschwerpunkte" class="btn btn-danger btn-xs delete btn-deleteQualitatsschwerpunkte"  value='btn-deleteQualitatsschwerpunkte'>Delete</button></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<!-- User change modal -->
<div class='modal fade' id='modal_editUser' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
    <div class='modal-dialog'>
        <div class='modal-content'>
            <div class='modal-header'>
                <h4>Change data</h4>
            </div>
            <form method="POST" role="form" id="form_updateQualitatsschwerpunkte" name="form_updateQualitatsschwerpunkte" action="qualitatsschwerpunkte.edit">
                <div class='modal-body'>
                    <div class='container'>
                        <input type='hidden' id='modalid' name='modalid' value='' />
                        <table>
                            <tr>
                                <td>
									<div class="input-group date" name="datum_tage" id="datum_tage_">
										<span class="input-group-addon">Datum</span>
										<input type="text" class="form-control datum_tage" name="mdatum_tage" id="mdatum_tage" value="<?=substr($datum_tage, 0,10);?>" style="min-width: 100px;">
										<span class="input-group-addon" style="min-width: 10px;"><span class="glyphicon glyphicon-calendar"></span></span>
									</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
									<div class='input-group halle' id='halle_group'>
										<span class='input-group-addon'>Halle</span>
										<select class='selectpicker' data-width='100%' id='mhalle' name='mhalle'>
											<option value='3'>3</option>
											<option value='4'>4</option>
											<option value='5'>4A</option>
										</select>
									</div>
                                </td>
                            </tr>
                            <tr>
                                <td>
									<div class='input-group username' id='username_group'>
										<span class='input-group-addon'>Bemerkung</span>
										<input type='text' class='form-control' placeholder='Bemerkung' id='mbemerkung' size=28 name='mbemerkung' aria-describedby='username_group'>
									</div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class='modal-footer'>
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Abbruch</button>
                    <button type='submit' class='btn btn-info' id='submit_saveQualitatsschwerpunkte' name='submit_saveQualitatsschwerpunkte'>Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /User change modal -->

<!-- activate dataTable -->
<script type='text/javascript'>
    $(document).ready(function () {

    });
</script>
<!-- /activate dataTable -->
<!-- define global functions -->
<script type="text/javascript">
    function isBetween(n, a, b) {
            return (n - a) * (n - b) <= 0
        };

    function fillVonBisHours (start, end) {
        if (Number(start)>Number(end)){
                
                
                for(i=Number(start); i<=23; i++){
                    if(i<10){i='0'+i};
                    $('#startzeit_h, #endzeit_h').append('<option value="'+i+'">' + i + '</option>');

                };
                for(i=0; i<=Number(end); i++){
                    if(i<10){i='0'+i};
                    $('#startzeit_h, #endzeit_h').append('<option value="'+i+'">' + i + '</option>');
                };
            }
            else {
                for(i=Number(start); i<=Number(end); i++){
                    if(i<10){i='0'+i};
                    $('#startzeit_h, #endzeit_h').append('<option value="'+i+'">' + i + '</option>');
                };
            };
    };
	
	
</script>
<!-- /.define global functions -->

<?php require ('app/views/partials/footer_vendor_scripts.php'); ?>
<!-- initalize selectpicker -->
<script type='text/javascript'>
    $('.selectpicker').selectpicker({
        size: 4
    });
</script>
<!-- initalize dataTable -->

<!-- delete selected error -->
<script type="text/javascript">
    $('.btn-deleteQualitatsschwerpunkte').click(function () {
        var id = $(this).closest('tr').children('#ID').text();
		 if(confirm('Delete Qualit�tsschwerpunkte?')){
		$.post( "qualitatsschwerpunkte.delete", { delete: id } );
		location.reload();
		   };
    });
</script>
<script type='text/javascript'>
    $(document).ready(function() {
        $.fn.dataTable.moment('DD.MM.YYYY');
        $('.myDataTable').DataTable({
            searching: false,
            paging: false,
            info: false,
            language: {
                emptyTable:     'Keine Daten in der Tabelle vorhanden',
                info:           '_START_ bis _END_ von _TOTAL_ Eintrï¿œgen',
                infoEmpty:      '0 bis 0 von 0 Eintrï¿œgen',
                infoFiltered:   '(gefiltert von _MAX_ Eintrï¿œgen)',
                infoPostFix:    '',
                thousands:      '.',
                lengthMenu:     '_MENU_ Eintrï¿œge anzeigen',
                loadingRecords: 'Wird geladen...',
                processing:     'Bitte warten...',
                search:         'Suchen',
                zeroRecords:    'Keine Eintrï¿œge vorhanden.',
                paginate: {
                    first:      'Erste',
                    previous:   'Zurï¿œck',
                    next:       'Nï¿œchste',
                    last:       'Letzte'
                },
                aria: {
                    sortAscending:  ': aktivieren, um Spalte aufsteigend zu sortieren',
                    sortDescending: ': aktivieren, um Spalte absteigend zu sortieren'
                },
                select: {
                    rows: {
                        _: '%d Zeilen ausgewï¿œhlt',
                        0: 'Zum Auswï¿œhlen auf eine Zeile klicken',
                        1: '1 Zeile ausgewï¿œhlt'
                    }
                }
            }
        });
    });
</script>
<!-- activate dataTable -->
<script type='text/javascript'>
    // autorefresh varables
    var autoplay = null;
    //var int = null;
    //var onoff = null;



    // start time, end time, dauer varabales
    var hours = <?= json_encode($hours);?>;

    var current_schicht = <?=$schicht;?>;
    // selection block variables
    var show_hide_speed = 0;
    var autotext_text ='';
    var auto_text_container ='';

	      // start time, end time, dauer varabales

    var current_date = new Date();
    var current_date_val = '<?=substr($datum_tage, 0,10);?>';
    var current_hour = current_date.getHours();
    if (current_hour<10){current_hour = '0'+current_hour;};

    var current_minute = current_date.getMinutes();
    if (current_minute<10){current_minute = '0'+current_minute;};
	var filtered = hours.filter(function (v,i) {
                        return v.SCHICHT==current_schicht;
                    });
    var start = Number(filtered[0].STR_TIME.substr(0,2));
    if (isBetween(Number(start),0,9)){start = '0'+start;};
    var end = Number(filtered[0].STR_TIME_END.substr(0,2));
    if (isBetween(Number(end),0,9)){end = '0'+end;};


           
</script>
<!-- /.define global variables -->






<!-- configure dateTime Picker -->
<script type='text/javascript'>
    function initializeDateTimePicker(){

        var format_date = 'DD.MM.YYYY';
        var viewMode = 'days';
        
        $('#datum_tage').datetimepicker({
            viewMode: viewMode,
           // locale: moment.locale('de'),
            format: format_date,
            
            useCurrent: false,
            extraFormats: false,
            sideBySide: true
        });        
        $('#mdatum_tage').datetimepicker({
            viewMode: viewMode,
            //locale: moment.locale('de'),
            format: format_date,
            
            useCurrent: false,
            extraFormats: false,
            sideBySide: true
        });

    }
    initializeDateTimePicker();
</script>
<!-- /.configure dateTime Picker -->
<!-- set values for modal window -->
<script type='text/javascript'>
    $('.btn_editQualitatsschwerpunkte').click(function () {
        $('#modal_editUser').modal('show');
        var id = $(this).closest('tr').children('#ID').text();
        var datum = $(this).closest('tr').children('#DATUM').text();
        var halle = $(this).closest('tr').children('#HALLE').text();
        var bemerkung = $(this).closest('tr').children('#BEMERKUNG').text();

        $('#modalid').val(id).text(id);
        $('#mdatum_tage').val(datum);
        $('#mhalle').selectpicker('val', halle);
        $('#mbemerkung').val(bemerkung);
    });
</script>
<!-- /set values for modal window -->

<?php require ('app/views/partials/footer.php'); ?>

