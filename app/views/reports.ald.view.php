<?php require ('app/views/partials/head.php'); ?>

<?php require ('app/views/partials/footer_vendor_scripts.php'); ?>
<div class='row nonprintable'>
    <div class='container'>
        <h3>ALD Report</h3>
    </div>
    <form method='POST' role='form' id='form_tagesansicht' name='form_tagesansicht' action='reports.ALDreport'>
        <div class='row'>
            <div class='container'>
                <div class='col-xs-3'>
                    <div class='input-group' name='datum_tage' id='datum_tage_'>
                        <span class='input-group-addon kursor'>Datum</span>
                        <input type='text' class='form-control datum_tage' name='datum_tage' id='datum_tage' value='<?= substr($datum_tage, 0, 10); ?>' style='min-width: 100px;'>
                        <span class='input-group-addon kursor' style='min-width: 10px;'><span class='glyphicon glyphicon-calendar' ></span></span>
                    </div>
                    <div class='input-group' name='halle_group' id='halle_group'>
                        <span class='input-group-addon'>Halle</span>
                        <select class='selectpicker halle' data-width='100%' id='halle' name='halle'>
                            <option value=3 <?php if ($halle_id == 3) {
    echo 'selected';
} ?>>Halle 3</option>
                            <option value=4 <?php if ($halle_id == 4) {
    echo 'selected';
} ?>>Halle 4</option>
                            <option value=5 <?php if ($halle_id == 5) {
    echo 'selected';
} ?>>Halle 4A</option>
                        </select>
                    </div>
                </div>
                <div class='col-xs-3'>
                    <div class='col-xs-3'>
                        <div><button type="submit" name='submit_form_tagesansicht' id='submit_form_tagesansicht' class="btn btn-success">Daten holen</button></div>
                    </div>
                </div>
                <div class='col-xs-6' style='text-align: right;'>
                    <a href="#" style='color: inherit;'><i id='submit_form_exportCSV' name='submit_form_exportCSV' class="fa fa-file-excel-o fa-2x" aria-hidden="true"></i></a>
                    &nbsp;
                    <span id='print' name='print' class="glyphicon glyphicon-print kursor print" aria-hidden="true" title='Dr�cken'></span>
                </div>

            </div>
        </div>
    </form>
</div>

<form method='POST' role='form' id='form_exportCSV' name='form_exportCSV' action='reports.schichtprotokoll.exportCSV'>
    <input type='hidden' name='report' id='report' value='schichtprotokoll'>
    <input type='hidden' name='datum_tage' id='datum_tagex' value='<?= substr($datum_tage, 0, 10); ?>'>
    <input type='hidden' name='halle' id='hallex' value='<?= $halle_id; ?>'>
</form>

<div class='row nonprintable'><div class='container'><HR></div></div>
<?php
if ($halle_id == 3)
    $halle_txt = '3';
if ($halle_id == 4)
    $halle_txt = '4';
if ($halle_id == 5)
    $halle_txt = '4A';
?>

<div class='row'>
    <div class='container nonprintable' style="display: none;">
        <div class='table-responsive'>
            <table class='table table_header' style='width: 100% !important;'>
                <tr>
                    <td style='width: 15%;'><h4 style='color: #337ab7;'>Halle <?= $halle_txt ?></h4></td>
                    <td style='width: 70%;'><h1>Schichtprotokoll</h1></td>
                    <td style='width: 15%;'><h4 style='color: #337ab7;'><?= substr($datum_tage, 0, 10); ?></h4></td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class='row nonprintable'>
    <div class='container'>
        <div class='col-xs-6' style='padding-left: 0px;'>
            <h4 style='color: #337ab7;'>Datum: <?= substr($datum_tage, 0, 10); ?></h4>
        </div>
        <div class='col-xs-6' style='padding-right: 0px; text-align: right;'>
            <h4 style='color: #337ab7;'>Halle <?= $halle_txt ?></h4>
        </div>
    </div>
</div>




<style>

    .bggray{background: gray;}
    .bgdarkgray{background: #424A53; text-align: center;}
    .bgdarkblue{background: #215B87;}
    .bgwhite{background: #fff;}
    .fontwhite{color:#fff;}
    .fontOrange{color: #FFA500; ; font-weight: bold;}
    .fontRed{color: #F00; border:solid 0px #5b5b5a;; font-weight: bold;}
	@media print {
	   .fontRed{color: #F00; border:solid 0px #5b5b5a; background: #aaaaaa  !important;}
	   .fontOrange{color: #Faa; border:solid 0px #5b5b5a; background: #eeeeee  !important;}
	   .tableAdvanced{
        font-family: Arial;
        font-size: 12px;
       
        white-space: nowrap;
		border: solid 2px;
		table { border: solid 1px #000;}
    }
    
  <!-- .px25widthcell, .bgdarkblue{background: #dddddd !important; -->
	
		.bgLightGray{background: #dddddd !important;border: 0px !important;}
	}
}
	
	
    .px25widthcell{width:20px !important;padding: 0px !important}
    .px25heightcell{width:20px;}
    .tableAdvanced{
        font-family: Arial;
        font-size: 12px;
       
        white-space: nowrap;
    }
    .tableAdvanced > tr > td{padding: 0px !important;}
    .tableAdvanced > tr{padding: 0px !important;}

    
    .gray{
        background-color: gray;
        color: white;
        font-weight: bold;
        text-align: center;
    }
    .darkgray{
        background-color: #000;
        color: wheat;
        font-weight: bold;
        text-align: center;
    }

    .graya{background:#DFDFDF; border: solid #000 1px !important;}
    .grayb{background:#EEEEEE;border: solid #000 1px;}
    .rotate {
        -moz-transform: translateX(0%) translateY(0%) rotate(-90deg);
        -webkit-transform: translateX(0%) translateY(0%) rotate(-90deg);
        transform:  translateX(0%) translateY(0%) rotate(-90deg);
    }
    .fullborder{}
    .noborder{border: 0px !important;}
</style>

<?php 
	if($rep != null){
                echo $rep->generateHtmlTable();
            
        }
?>
<BR>
<BR>
<?php //endfor;  ?>


<!-- initalize selectpicker -->
<script type='text/javascript'>
    $('.selectpicker').selectpicker({
        size: 4
    });
</script>
<!-- /.initalize selectpicker -->

<!-- configure dateTime Picker -->
<script type='text/javascript'>
    var today = new Date();
    var format_date = 'DD.MM.YYYY';
    var viewMode = 'days';

    $('#datum_tage').datetimepicker({
        viewMode: viewMode,
        locale: moment.locale('de'),
        format: format_date,
        useCurrent: false,
        extraFormats: false,
        sideBySide: true
    });
</script>
<!-- /.configure dateTime Picker -->

<!-- activate dataTable -->
<script type='text/javascript'>
    $(document).ready(function () {
        $.fn.dataTable.moment('DD.MM.YYYY');
        $('.myDataTable').DataTable({
            searching: false,
            paging: false,
            info: false,
            ordering: false,
            pageLength: 50,
            lengthMenu: [[50, 150, 250, -1], [50, 150, 250, "Alle"]],
            language: {
                emptyTable: 'Keine Daten in der Tabelle vorhanden',
                info: '_START_ bis _END_ von _TOTAL_ Eintr�gen',
                infoEmpty: '0 bis 0 von 0 Eintr�gen',
                infoFiltered: '(gefiltert von _MAX_ Eintr�gen)',
                infoPostFix: '',
                thousands: '.',
                lengthMenu: '_MENU_ Eintr�ge anzeigen',
                loadingRecords: 'Wird geladen...',
                processing: 'Bitte warten...',
                search: 'Suchen',
                zeroRecords: 'Keine Eintr�ge vorhanden.',
                paginate: {
                    first: 'Erste',
                    previous: 'Zur�ck',
                    next: 'N�chste',
                    last: 'Letzte'
                },
                aria: {
                    sortAscending: ': aktivieren, um Spalte aufsteigend zu sortieren',
                    sortDescending: ': aktivieren, um Spalte absteigend zu sortieren'
                },
                select: {
                    rows: {
                        _: '%d Zeilen ausgew�hlt',
                        0: 'Zum Ausw�hlen auf eine Zeile klicken',
                        1: '1 Zeile ausgew�hlt'
                    }
                }
            }
        });
    });
</script>
<!-- /.activate dataTable -->
<!-- open print dialog on print icon click -->
<script type="text/javascript">
    $('#print').click(function () {
        window.print();
    });
</script>
<!-- /.open print dialog on print icon click -->

<!-- submit exportCSV form -->
<script type="text/javascript">
    $('#submit_form_exportCSV').click(function () {
        $('#form_exportCSV').submit();
    });
</script>
<!-- /.submit exportCSV form -->


<?php require ('app/views/partials/footer.php'); ?>