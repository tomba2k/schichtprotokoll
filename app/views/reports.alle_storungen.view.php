<?php require ('app/views/partials/head.php'); ?>

<!--Errors table -->
<div class='container'>
    <h3>St�rungsliste</h3>
</div>
<BR>
<div class='row'>
    <div class='container'>
        <div class='col-xs-3' style="max-width: 200px;">
            <div class='input-group' name='datum_von' id='datum_von'>
                <span class='input-group-addon kursor' style="min-width: 50px;">Von</span>
                    <input type='text' class='form-control datum_von my_datum' name='datum_von' id='datum_von' style='min-width: 100px;' value=''>
                <span class='input-group-addon kursor' style='min-width: 10px;'><span class='glyphicon glyphicon-calendar' ></span></span>
            </div>
        </div>
        <div class='col-xs-3' style="max-width: 200px;">
            <div class='input-group' name='datum_bis' id='datum_bis'>
                <span class='input-group-addon kursor' style="min-width: 50px;">Bis</span>
                    <input type='text' class='form-control datum_bis my_datum' name='datum_bis' id='datum_bis' style='min-width: 100px;' value=''>
                <span class='input-group-addon kursor' style='min-width: 10px;'><span class='glyphicon glyphicon-calendar' ></span></span>
            </div>
        </div>
        <div class='col-xs-4'>
            <form method='GET' role='form' id='myform' name='myform' class='form-inline' action='reports.alle_storungen'>
            <div class='form-group'>
                <div class='col-xs-3' style='padding-left: 0px;'>
                    <input type='checkbox' checked name='autorefresh' id='autorefresh' data-toggle="toggle" data-on="On" data-off="Off">
                </div>
                <div class='col-xs-5 div_autorefreshtime'>
                    <div class='input-group' name='autorefreshtime_group' id='autorefreshtime_group'>
                        <span class='input-group-addon fade_group'>Autorefresh</span>
                        <input type='number' class='form-control' name='autorefreshtime' id='autorefreshtime' style='min-width: 60px;' value='' >
                        <span class='input-group-addon fade_group' style='min-width: 0px;'>Min</span>
                    </div>
                </div>
            </div>
            </form>
        </div>
        <div class='col-xs-2 pull-right' style='text-align: right;'>
                <a href="#" style='color: inherit;'><i id='submit_form_exportCSV' name='submit_form_exportCSV' class="fa fa-file-excel-o fa-2x" aria-hidden="true"></i></a>
        </div>
    </div>
</div>
<BR>

<form method='POST' role='form' id='form_exportCSV' name='form_exportCSV' action='reports.schichtprotokoll.exportCSV'>
    <input type='hidden' name='report' id='report' value='alle_storungen'>
    <input type='hidden' class = 'datum_tage_von' name='datum_tage_von' id='datum_tage_von' value=''>
    <input type='hidden' class = 'datum_tage_bis' name='datum_tage_bis' id='datum_tage_bis' value=''>
</form>

<div class='container'>
<div class='table-responsive'>
<table id='myDataTable' class='table table-striped table-bordered table-hover schichten' style='cellspacing: 0; width: 100%;'>
    <thead>
        <tr>
            <th>Datum</th>
            <th>Schichtbeginn</th>
            <th>Anfangszeit</th>
            <th>Dauer (Min)</th>
            <th>Art</th>
            <th>St�rung</th>
            <th>Halle</th>
            <th>Linie</th>
            <th>Ebene 1</th>
            <th>Ebene 2</th>
            <th>Ebene 3</th>
            <th>Ebene 4</th>
            <th style='width: 30%;'>Bemerkung manuell</th>
            <th>Modell</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($alle_storungen as $alle_storunge): ?>
        <tr>
            <td><?= $alle_storunge->DATUM;?></td>
            <td><?= $alle_storunge->SCHICHT_BEGIN;?></td>
            <td><?= $alle_storunge->VON.':00';?></td>
            <td><?= $alle_storunge->DAUER;?></td>
            <td><?= $alle_storunge->ART;?></td>
            <td><?= $alle_storunge->STORUNG_TEXT;?></td>
            <td><?= $alle_storunge->HALLE;?></td>
            <td><?= $alle_storunge->LINIE;?></td>
            <td><?= $alle_storunge->U1_TEXT;?></td>
            <td><?= $alle_storunge->U2_TEXT;?></td>
            <td><?= $alle_storunge->U3_TEXT;?></td>
            <td><?= $alle_storunge->U4_TEXT;?></td>
            <td><?= $alle_storunge->KOMENTAR;?></td>
            <td><?= $alle_storunge->TYP;?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
</div>
</div>
<!--/.Errors table -->


<?php require ('app/views/partials/footer_vendor_scripts.php'); ?>



<script type="text/javascript">
    
</script>

<!-- activate dataTable -->
<script type='text/javascript'>
        $.fn.dataTable.moment('DD.MM.YYYY');
        var table = $('#myDataTable').DataTable({
            searching: true,
            paging: true,
            info: true,
            order: [[ 0, 'desc' ]],
            pageLength: 50,
            lengthMenu: [ [50, 150, 250, -1], [50, 150, 250, "Alle"] ],
            language: {
            	emptyTable:   	'Keine Daten in der Tabelle vorhanden',
				info:         	'_START_ bis _END_ von _TOTAL_ Eintr�gen',
				infoEmpty:    	'0 bis 0 von 0 Eintr�gen',
				infoFiltered: 	'(gefiltert von _MAX_ Eintr�gen)',
				infoPostFix:  	'',
				thousands:  	'.',
				lengthMenu:   	'_MENU_ Eintr�ge anzeigen',
				loadingRecords:	'Wird geladen...',
				processing:   	'Bitte warten...',
				search:       	'Suchen',
				zeroRecords:  	'Keine Eintr�ge vorhanden.',
				paginate: {
					first:    	'Erste',
					previous: 	'Zur�ck',
					next:     	'N�chste',
					last:     	'Letzte'
				},
				aria: {
					sortAscending:  ': aktivieren, um Spalte aufsteigend zu sortieren',
					sortDescending: ': aktivieren, um Spalte absteigend zu sortieren'
				},
				select: {
    				rows: {
						_: '%d Zeilen ausgew�hlt',
						0: 'Zum Ausw�hlen auf eine Zeile klicken',
						1: '1 Zeile ausgew�hlt'
	    			}
				}
            }
        });

</script>
<!-- /.activate dataTable -->

<!-- configure dateTime Picker -->
<script type='text/javascript'>
    var today = new Date();
    var format_date = 'DD.MM.YYYY';
    var viewMode = 'days';
    
    $('#datum_von').datetimepicker({
        viewMode: viewMode,
        locale: moment.locale('de'),
        format: format_date,
        
        useCurrent: false,
        extraFormats: false,
        sideBySide: true,
    });

    $('#datum_bis').datetimepicker({
        viewMode: viewMode,
        locale: moment.locale('de'),
        format: format_date,
        
        useCurrent: false,
        extraFormats: false,
        sideBySide: true,
    });
</script>
<!-- /.configure dateTime Picker -->

<!-- von/bis date search extension for dataTable -->
<script type="text/javascript">
$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        var min  = $('#datum_von').data("DateTimePicker").date();
        var max  = $('#datum_bis').data("DateTimePicker").date();
        var createdAt = moment(data[0], format_date);

        if (min == null && max == null){return true;}
        else if (min == null && moment(createdAt).isSameOrBefore(max)){return true;}
        else if (max == null && moment(createdAt).isSameOrAfter(min)){return true;}
        else if (moment(createdAt).isSameOrAfter(min) && moment(createdAt).isSameOrBefore(max)){return true;} 
        else {return false;}
    }
);

$("#datum_von, #datum_bis").on('focusout',function(){
    table.draw();
});
</script>
<!-- /.von/bis date search extension for dataTable -->

<!-- autorefresh controller -->
<script>
    var autoplay = null;
    var int = null;
    var onoff = null;
    $( document ).ready(function() {
        
        onoff = GetURLParameter('autorefresh');
        int = GetURLParameter('autorefreshtime');
        
        if(onoff == 'on'){
            $('#autorefreshtime').val(int);
            //alert($('#autorefresh').val());
            autoplay = setInterval(function() {$('#myform').submit();}, int*1000*60);
        }
    });

    $('#autorefreshtime').keyup(function(){
        int = $(this).val()*1000*60;
        autoplay = setInterval(function() {$('#myform').submit();}, int);
    });

    $('#autorefresh').change(function() {
        if($(this).prop('checked')){
            $("#autorefreshtime").prop("readonly", false);
            $('.fade_group').css('color', '#333');
            $('#autorefreshtime').css('color', '#333');
            int = $('#autorefreshtime').val();
            $('.div_autorefreshtime').show(300);
            autoplay = setInterval(function() {$('#myform').submit();}, int*1000*60);
        } 
        else{
            $('#autorefreshtime').prop('readonly', true);
            $('#autorefreshtime').css('color', '#e6e6e6');
            $('.fade_group').css('color', '#e6e6e6');
            //$('.div_autorefreshtime').hide(300);
            clearInterval(autoplay);
        }
      
    });

    // fetch GEt URL paramaters function
    function GetURLParameter(sParam){
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++){
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam){
                return sParameterName[1];
            }
        }
    }
</script>
<!-- /.autorefresh controller -->

<!-- submit exportCSV form -->
<script type="text/javascript">
    $('#submit_form_exportCSV').click(function(){
        $('.datum_tage_von').val($('.datum_von').val());
        $('.datum_tage_bis').val($('.datum_bis').val());
        $('#form_exportCSV').submit();
    });
</script>
<!-- /.submit exportCSV form -->


<?php require ('app/views/partials/footer.php'); ?>