<?php

return [
	'database' => [
		'username' => 'STORUNG',
		'password' => 'storung',
		'tns' => '(DESCRIPTION =(ADDRESS_LIST =(ADDRESS = (PROTOCOL = TCP)(HOST =localhost)(PORT = 1521)))(CONECT_DATA =(SERVICE_NAME =XE)))',
		'charset' => 'WE8ISO8859P15',
		'options' => [
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
			PDO::ATTR_EMULATE_PREPARES => false,
			PDO::ATTR_STRINGIFY_FETCHES => false
		]
	]
];